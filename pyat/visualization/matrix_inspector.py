import sys

import matplotlib.pyplot as plt
import numpy as np
from PyQt5 import QtWidgets

import pyat.core.utils.tiff_driver as tiff
from visualization.OnNotebook import Notebook
from visualization.OnSoftware import Display_Windows


# pylint: disable=C0415


def check_ipython():
    if "get_ipython" in sys.modules:
        return get_ipython()
    else:
        try:
            # pylint: disable=C0415
            from IPython import get_ipython

            return get_ipython()
        except ModuleNotFoundError:
            return None


def display_matrix(values: np.ndarray, title: str, show=False, bins=100):
    """
    Display a numpy matrix and statistics about its content
    """
    if check_ipython() is not None:
        ntbk = Notebook()
        if values.ndim > 1:
            ntbk.display_2d(values, title, show, bins=bins)
        else:
            ntbk.display_1d(values, title, show)
    else:
        app = QtWidgets.QApplication(sys.argv)
        soft = Display_Windows(app, values, title, 255)
        soft.run()


def display_geotiff_data(file_name: str, title: str, show=True):
    """
    Display a geotiff and statistics about its content
    do not take into account for projection and offset, only display data content
    """
    tiff_values = np.ma.filled(tiff.read_tiff(file_name), np.nan)
    # I have had that to delete wrong values from sonarscope
    display_matrix(tiff_values, title, show)


def show():
    plt.show()
