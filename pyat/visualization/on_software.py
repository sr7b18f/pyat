import sys

import matplotlib.animation as animation
import numpy as np
from PyQt5 import QtWidgets, uic
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.widgets import MultiCursor


class Display_Windows(QtWidgets.QMainWindow):
    def __init__(self, app, values, data_title, bins):
        super(Display_Windows, self).__init__()
        self.app = app
        self.hide = False
        self.bins = bins
        self.data = values
        self.cursor_activated = False
        if self.data.ndim > 1:
            uic.loadUi("2d_window.ui", self)
            self.title.setText(data_title)
            self.run_dynamic_plot_button.setVisible(False)
            self.static_plot, self.main_plot = self.image_window(self.data)
        else:
            uic.loadUi("1d_window.ui", self)
            self.title.setText(data_title)
            self.static_plot, self.main_plot = self.plot_window(self.data)

    def image_window(self, values):
        """
        Display a window with data, histogram and stats
        Used to display data with dim > 1
        :param window: Actual window
        :param values: Matrix to display
        :param title: Title of the graph
        """
        self.profile_button.clicked.connect(self.create_profile_plot)
        self.grid_button.clicked.connect(self.show_grid)
        self.run_dynamic_plot_button.clicked.connect(self.run_animation)
        self.cursor_button.clicked.connect(self.cursor_on)

        mnt_displayed, plot = self.draw_plot(values)
        self.toolbar = NavigationToolbar(mnt_displayed, self)

        self.toolbar_layout.addWidget(self.toolbar)
        self.grid_main_layout.addWidget(mnt_displayed, 0, 0)
        self.add_hist(values)
        return mnt_displayed, plot

    def plot_window(self, values):
        """
        Display a window with data and stats
        Used to display a plot for dim = 1
        :param window: Actual window
        :param values: matrix 1D
        :param title: Title of graph
        """
        # Display plot
        mnt_displayed, plot = self.display_plot(values)
        self.main_plot_layout.addWidget(mnt_displayed)
        self.add_stats(values)
        return mnt_displayed, plot

    def draw_plot(self, values):
        """
        Display graph for 2dimensions data
        """
        static_plot = FigureCanvas(Figure())
        main_plot = static_plot.figure.add_subplot(111)
        main_plot.clear()
        main_plot.imshow(values, aspect="auto")
        main_plot.set_xlabel("Ping")
        main_plot.set_ylabel("Beam")
        return static_plot, main_plot

    def display_plot(self, values):
        """
        Display plot for 1dimension data
        """
        static_plot = FigureCanvas(Figure())
        main_plot = static_plot.figure.add_subplot(111)
        main_plot.clear()
        main_plot.plot(values)
        main_plot.set_xlabel("Data")
        return static_plot, main_plot

    def create_profile_plot(self):
        """
        Create two plots to display differents profiles
        Add animation to dynamically show profile
        """
        if not self.hide:
            # Create
            self.anim = animation.FuncAnimation(
                self.static_plot.figure,
                self.animate,
                blit=False,
                repeat=False,
                interval=0.1,
            )
            self.plot_right_profile = FigureCanvas(Figure(figsize=(0.25, 4)))
            self.ax = self.plot_right_profile.figure.add_subplot(1, 1, 1, sharey=self.main_plot)
            self.grid_main_layout.addWidget(self.plot_right_profile, 0, 1)
            # Create
            self.plot_left_profile = FigureCanvas(Figure(figsize=(4, 0.25)))
            self.ay = self.plot_left_profile.figure.add_subplot(1, 1, 1, sharex=self.main_plot)
            self.grid_main_layout.addWidget(self.plot_left_profile, 1, 0)
            self.profile_button.setText("Hide profile")
            self.hide = True
            self.anim.event_source.start()
        else:
            self.profile_button.setText("Display profile")
            self.anim.event_source.stop()
            self.anim = None
            self.plot_right_profile.deleteLater()
            self.plot_left_profile.deleteLater()
            self.hide = False

    def cursor_on(self):
        if not self.cursor_activated:
            self.multi.set_active(True)
            self.static_plot.draw()
            self.cursor_button.setText(" Cursor Off")
            self.multi.visible = True
            self.cursor_activated = True
        else:
            self.multi.visible = False
            self.multi.set_active(False)
            self.static_plot.draw()
            self.static_plot.update()
            self.cursor_button.setText("Cursor On")
            self.cursor_activated = False

    def show_grid(self):
        """
        Display or Hide grid
        Called when button is pushed
        """
        self.main_plot.grid()
        self.static_plot.draw()

    def add_stats(self, values):
        """
        Assign stats to each label
        :param values: Array
        """
        self.head_label.setText("Statistic :")
        self.mean_label.setText("Mean : " + str(np.nanmean(values)))
        self.median_label.setText("Median : " + str(np.nanmedian(values)))
        self.std_label.setText("Std : " + str(np.nanstd(values)))
        self.max_label.setText("Max : " + str(np.nanmax(values)))
        self.min_label.setText("Min : " + str(np.nanmin(values)))

    def run_animation(self):
        """
        When using the toolbar, animation is paused
        So we need to restart animation
        """
        self.anim.event_source.start()
        self.run_dynamic_plot_button.setVisible(False)

    def get_pos(self):
        """
        Get position of the cursor and extract X & Y
        return : position of X and Y
        """
        pos_string = self.toolbar.pos
        if pos_string == "zoom rect":
            self.anim.event_source.stop()
            self.run_dynamic_plot_button.setVisible(True)
            return 0, 0
        if pos_string is not None and self.toolbar._active is None:
            self.anim.event_source.start()
            x = pos_string[2 : pos_string.find(" ")]
            string = pos_string[pos_string.find("y=") : :]
            y = string[2 : string.find(" ")]
            return x, y
        else:
            return 0, 0

    def animate(self, frame):
        """
        Called every frame (interval = 0.1s)
        :param frame: necessary but unused
        """
        x, y = self.get_pos()
        if x and y is not 0:
            self.static_plot.update()
            # TODO : Find a way to know if animation is running
            # Start animation
            self.anim.event_source.start()
            self.ax.clear()
            x_axis = np.array(self.data[:, int(float(x))])
            y_axis = np.arange(len(self.data[:, int(float(y))]))
            self.ax.plot(x_axis, y_axis)
            self.plot_right_profile.draw()

            self.ay.clear()
            self.ay.set_xlim([0, self.data.shape[1]])
            self.ay.plot(np.array(self.data[int(float(y))]))
            self.plot_left_profile.draw()

    def add_hist(self, values):
        # Display histogramme of data
        hist = FigureCanvas(Figure())
        hist_plot = hist.figure.add_subplot(111)
        hist_plot.clear()
        hist_plot.hist(values.flatten(), bins=self.bins, histtype="stepfilled")
        hist_plot.set_xlabel("Histogram")
        hist.draw()
        self.hist_layout.addWidget(hist)
        self.add_stats(values)

    def run(self):
        self.multi = MultiCursor(
            self.static_plot,
            self.static_plot.figure.axes,
            color="black",
            lw=0.5,
            horizOn=True,
            vertOn=True,
        )
        self.multi.set_active(False)
        self.show()
        sys.exit(self.app.exec_())


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Display_Windows(0, 0, 0)
    sys.exit(app.exec_())
