import pandas as pd
import numpy as np


def read(file: str):
    """Parse a hfs file and return file data
    hfs is a ascii file containing date time and value
    """
    ds = pd.read_csv(file, delim_whitespace=True, names=["date", "time", "value"])
    ds["datetime"] = ds["date"] + " " + ds["time"]
    pd.to_datetime(ds["datetime"], format="%Y-%m-%d %H:%M:%S")

    return ds["datetime"].to_numpy(dtype="datetime64[us]"), ds["value"].to_numpy()


if __name__ == "__main__":
    time, values = read("C:/data/datasets/Tide/prediction_mayotte/prediction_1230S4539E.hfs")
    print(time)
    print(values)
