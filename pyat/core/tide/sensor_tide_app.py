#! /usr/bin/env python3
# coding: utf-8
from abc import abstractmethod

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy
import scipy.signal

import pyat.core.utils.pyat_logger as log
from pyat.core.sensor.navigation_file import read_navigation
from pyat.core.sounder import sounder_driver_factory
from pyat.core.surface_reference.bathyelli import BathyElli
from pyat.core.surface_reference.sea_surface import SeaSurfaceProjector
from pyat.core.tide.fes2014.fes_tide_predict import FESTideComputer
from pyat.core.utils.exceptions.exception_list import BadParameter
from pyat.core.utils.monitor import ProgressMonitor
from pyat.core.xsf.xsf_driver import XsfDriver


class SensorTide:
    def __init__(self, **params):
        self.logger = log.logging.getLogger(SensorTide.__name__)
        self.model_dir = None
        self.input_files = []
        self.output_file = None
        self.predictive_mode = False
        self.display = False
        self.positioning_type_filter = False
        self.reference_surface = "LAT"
        self.reference_altitude = 0.0
        self.parse_parameter(**params)

    def parse_parameter(self, **params):
        if "input_files" in params:
            self.input_files = params["input_files"]

        if "output_file" in params:
            self.output_file = params["output_file"]
        else:
            raise BadParameter("output_file is mandatory")

        if "model_dir" in params:
            self.model_dir = params["model_dir"]

        if "reference_surface" in params:
            self.reference_surface = params["reference_surface"]

        if "reference_altitude" in params:
            self.reference_altitude = float(params["reference_altitude"])

        if "predictive_mode" in params:
            self.predictive_mode = params["predictive_mode"]

        if "positioning_type_filter" in params:
            self.positioning_type_filter = params["positioning_type_filter"]

        if "display" in params:
            self.display = params["display"]

    def process(self):
        self.logger.info(f"Read input files")

        (longitudesMasked, latitudesMasked, altitudesMasked) = self.read_sensor_data()
        if altitudesMasked.empty:
            self.logger.info(f"No data to process")
            return

        self.logger.info(f"Change reference")

        if self.reference_surface == "ZH":
            tides_ref = self.update_surface_reference_zh(longitudes=longitudesMasked.values,
                                                         latitudes=latitudesMasked.values,
                                                         tides=altitudesMasked.values)
        elif self.reference_surface == "LAT":
            tides_ref = self.update_surface_reference_lat(longitudes=longitudesMasked.values,
                                                          latitudes=latitudesMasked.values,
                                                          tides=altitudesMasked.values)
        else:
            tides_ref = altitudesMasked.values
            if self.reference_altitude:
                tides_ref -= self.reference_altitude

        if self.display:
            display_ref_alt_series = pd.Series(data=tides_ref, index=altitudesMasked.axes[0])

        if self.predictive_mode:
            self.logger.info(f"Predictive mode is active : substract tide prediction before lowpass filtering")
            # substract fesp rediction
            _, _, _, tide_FES_pred_sub, _, _, _, _ = FESTideComputer(self.model_dir + "/fes2014").compute_tides(
                longitudes=longitudesMasked.values,
                latitudes=latitudesMasked.values,
                dates=altitudesMasked.axes[0],
                monitor=ProgressMonitor())
            tides_ref = tides_ref - tide_FES_pred_sub

        self.logger.info(f"Apply lowpass filtering")
        tides_ref_series = pd.Series(data=tides_ref, index=altitudesMasked.axes[0])
        (filtered_long, filtered_lat, filtered_alt) = _filter_and_interpolate_nav(longitudesMasked,
                                                                                  latitudesMasked,
                                                                                  tides_ref_series)

        filtered_time = filtered_alt.axes[0]
        if self.predictive_mode:
            self.logger.info(f"Predictive mode is active : reapply tide prediction after lowpass filtering")
            # add fes prediction
            _, _, _, tide_FES_pred_add, _, _, _, _ = FESTideComputer(self.model_dir + "/fes2014").compute_tides(
                longitudes=filtered_long.values,
                latitudes=filtered_lat.values,
                dates=filtered_time,
                monitor=ProgressMonitor())
            filtered_alt = filtered_alt + tide_FES_pred_add

        # format data for export as csv
        date_series = pd.Series(data=filtered_time, name="date", index=filtered_time)
        lon_series = pd.Series(data=filtered_long, name="longitude", index=filtered_time)
        lat_series = pd.Series(data=filtered_lat, name="latitude", index=filtered_time)
        tide_series = pd.Series(data=filtered_alt, name="tide", index=filtered_time)
        df = pd.concat([date_series, lon_series, lat_series, tide_series], axis=1)
        sampling = '10min'
        date_resample = pd.date_range(start=filtered_time[0] - pd.Timedelta(sampling),
                                      end=filtered_time[-1] + pd.Timedelta(sampling), freq=sampling)
        date_resample = date_resample.round(sampling)
        df_resample = df.reindex(index=date_resample, method='nearest')
        df_resample.to_csv(self.output_file, sep=";", index=False, date_format='%Y-%m-%dT%H:%M:%SZ')

        if self.display:
            # matplotlib
            plt.plot(display_ref_alt_series, '.', color="red", markersize=1)
            plt.plot(filtered_alt, color="black")
            plt.xlabel("date")
            plt.ylabel("tide")
            plt.show()

    def update_surface_reference_lat(self, longitudes: np.ndarray, latitudes: np.ndarray, tides: np.ndarray):
        self.logger.info("Changing reference surface to LAT")

        reference = SeaSurfaceProjector(model_dir=self.model_dir)
        ref_values = reference.project_ellipsoid_to_lat(longitudes=longitudes, latitudes=latitudes, values=tides).flat[
                     :]
        return ref_values

    def update_surface_reference_zh(self, longitudes: np.ndarray, latitudes: np.ndarray, tides: np.ndarray):
        self.logger.info("Changing reference surface to ZH")

        reference = BathyElli(source_directory=self.model_dir)
        ref_values = reference.get_ellipsoid_to_zerohydro(longitudes=longitudes, latitudes=latitudes).flat[:]
        if np.any(ref_values):
            return tides - ref_values
        else:
            self.logger.warning(f"Data outside of BathyElli zone")
            return tides

    @abstractmethod
    def read_sensor_data(self) -> (pd.Series, pd.Series, pd.Series):
        """
        return raw longitudes, latitudes, altitudes as Series with masked data
        """


class XsfNmeaTide(SensorTide):
    def __init__(self, **params):
        super().__init__(**params)
        self.logger = log.logging.getLogger(XsfNmeaTide.__name__)
        self.global_df = None

    def __call__(self):
        self.process()

    def read_sensor_data(self) -> (pd.Series, pd.Series, pd.Series):
        """
        implementation of SensorTide abstract method
        """
        for input_file in self.input_files:
            self.read_xsf_nmea(input_file)

        # sort
        self.global_df.sort_index(inplace=True)
        # remove duplicates
        self.global_df = self.global_df.drop_duplicates("date")

        quality = self.global_df["quality"]

        # uncomment to see unfiltered raw data with colored quality on plot
        # if self.display:
        #     self.global_df.plot.scatter(x="date", y="altitude", c=quality, cmap="viridis", marker=".")

        if self.positioning_type_filter:
            gps_quality_mask = (quality > 2)
            altitudes_masked = self.global_df["altitude"][gps_quality_mask]
            longitudes_masked = self.global_df["longitude"][gps_quality_mask]
            latitudes_masked = self.global_df["latitude"][gps_quality_mask]
        else:
            altitudes_masked = self.global_df["altitude"][:]
            longitudes_masked = self.global_df["longitude"][:]
            latitudes_masked = self.global_df["latitude"][:]

        return longitudes_masked, latitudes_masked, altitudes_masked

    def read_xsf_nmea(self, input_file: str):
        """Read a xsf file,
        :return a time variable, and a list of tuple containing the name of a column and the associated data
        """
        with sounder_driver_factory.open_sounder(input_file) as i_sounder_driver:
            df = self._evaluate_nmea(i_sounder_driver)
            self.global_df = df.append(other=self.global_df, sort=False)

    def _evaluate_nmea(self, i_sounder_driver: XsfDriver):
        """
        Use the spatial resolution in meter as distance from one point of the navigation to evaluate the resolution in degree
        """
        times = i_sounder_driver.read_position_times()
        lons = i_sounder_driver.read_position_longitudes()
        lats = i_sounder_driver.read_position_latitudes()
        # water_level = i_sounder_driver.read_water_level()
        (_, _, sensor_position_offset_z) = i_sounder_driver.read_position_offset()

        nmeas = i_sounder_driver.read_position_nmea()

        # Field Number:
        #   1) Universal Time Coordinated (UTC)
        #   2) Latitude
        #   3) N or S (North or South)
        #   4) Longitude
        #   5) E or W (East or West)
        #   6) GPS Quality Indicator,
        #      0 - fix not available,
        #      1 - GPS fix,
        #      2 - Differential GPS fix
        #      (values above 2 are 2.3 features)
        #      3 = PPS fix
        #      4 = Real Time Kinematic
        #      5 = Float RTK
        #      6 = estimated (dead reckoning)
        #      7 = Manual input mode
        #      8 = Simulation mode
        #   7) Number of satellites in view, 00 - 12
        #   8) Horizontal Dilution of precision (meters)
        #   9) Antenna Altitude above/below mean-sea-level (geoid) (in meters)
        #  10) Units of antenna altitude, meters
        #  11) Geoidal separation, the difference between the WGS-84 earth
        #      ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level
        #      below ellipsoid
        #  12) Units of geoidal separation, meters
        #  13) Age of differential GPS data, time in seconds since last SC104
        #      type 1 or 9 update, null field when DGPS is not used
        #  14) Differential reference station ID, 0000-1023
        #  15) Checksum
        gps_quality_index = 6
        antenna_altitude_index = 9
        geoidal_separation_index = 11

        self.logger.info(f"Evaluation of the nmea")

        qualities = []
        altitudes = []
        separations = []
        for nmea in nmeas:
            fields = nmea.split(",")
            qualities.append(float(fields[gps_quality_index]))
            altitudes.append(float(fields[antenna_altitude_index]) + sensor_position_offset_z) # -water_level
            separations.append(float(fields[geoidal_separation_index]))

        df = pd.DataFrame(data={"date": times,
                                "longitude": lons, "latitude": lats,
                                "quality": qualities,
                                "altitude": altitudes, "geoidal separation": separations}, index=times)

        return df


class NavTide(SensorTide):
    def __init__(self, **params):
        super().__init__(**params)
        self.logger = log.logging.getLogger(NavTide.__name__)

    def __call__(self):
        self.process()

    def read_sensor_data(self) -> (pd.Series, pd.Series, pd.Series):
        """
        implementation of SensorTide abstract method
        """
        global_df = None

        # read navigation
        for input_file in self.input_files:
            self.logger.info(f"Read {input_file}")
            nav_data = read_navigation(input_file)
            if nav_data.types is not None:
                df = pd.DataFrame(data={"date": nav_data.times,
                                        "longitude": nav_data.longitudes, "latitude": nav_data.latitudes,
                                        "altitude": nav_data.altitudes, "type": nav_data.types}, index=nav_data.times)
            else:
                df = pd.DataFrame(data={"date": nav_data.times,
                                        "longitude": nav_data.longitudes, "latitude": nav_data.latitudes,
                                        "altitude": nav_data.altitudes}, index=nav_data.times)
            global_df = df.append(other=global_df, sort=False)

        # sort
        global_df.sort_index(inplace=True)
        # remove duplicates
        global_df = global_df.drop_duplicates("date")

        if "type" in global_df:
            quality = global_df["type"]
        else:
            quality = None
        has_quality_data = quality is not None and quality.any()

        # uncomment to see unfiltered raw data with colored quality on plot
        # if self.display:
        #     if has_quality_data:
        #         global_df.plot.scatter(x="date", y="altitude", c=quality, cmap="viridis", marker=".")
        #     else:
        #         global_df.plot.scatter(x="date", y="altitude", color="red", marker=".")

        if self.positioning_type_filter and has_quality_data:
            quality_mask = (quality > 2)  # quality, positioning type better than differential GPS
            altitudes_masked = global_df["altitude"][quality_mask]
            longitudes_masked = global_df["longitude"][quality_mask]
            latitudes_masked = global_df["latitude"][quality_mask]
        else:
            altitudes_masked = global_df["altitude"][:]
            longitudes_masked = global_df["longitude"][:]
            latitudes_masked = global_df["latitude"][:]

        return longitudes_masked, latitudes_masked, altitudes_masked


def _filter_and_interpolate_nav(longitudes: pd.Series, latitudes: pd.Series, altitudes: pd.Series) -> (
    pd.Series, pd.Series, pd.Series):
    filtered_altitudes = _resample_interpolate_and_filter_data(data=altitudes)
    filtered_longitudes = _resample_interpolate_and_filter_data(data=longitudes)
    filtered_latitudes = _resample_interpolate_and_filter_data(data=latitudes)
    return filtered_longitudes, filtered_latitudes, filtered_altitudes


def _resample_interpolate_and_filter_data(data: pd.Series) -> pd.Series:
    # first apply rolling window mean and interpolate
    resampled_data = _resample_and_interpolate(data)
    # then apply lowpass filter
    filtered_data = _apply_low_pass_cheby2_filter(resampled_data)
    return filtered_data


def _resample_and_interpolate(data: pd.Series) -> pd.Series:
    # first apply a rolling window mean
    sampling = "1s"
    window_size = 30
    time = pd.date_range(start=data.index[0] - pd.Timedelta(sampling), end=data.index[-1] + pd.Timedelta(sampling),
                         freq=sampling)
    resampled_data_raw = data.reindex(index=time, method='nearest', limit=1)
    resampled_data_mean = resampled_data_raw.rolling(window=window_size, center=True).mean()

    if len(resampled_data_raw) > window_size:
        resampled_data_mean[:window_size] = resampled_data_raw[:window_size].rolling(window=window_size, center=True,
                                                                                     min_periods=1).mean()
        resampled_data_mean[-window_size:] = resampled_data_raw[-window_size:].rolling(window=window_size, center=True,
                                                                                       min_periods=1).mean()
    # then linear interpolation of missing data
    return resampled_data_mean.interpolate(limit_direction='both')


def _apply_low_pass_cheby2_filter(data: pd.Series) -> pd.Series:
    order = 2
    attenuation_cut = 40
    frequency_cut = 1 / 1000  # (-40dB point)
    frequency_sampling = 1  # 1Hz

    (b, a) = scipy.signal.cheby2(order, attenuation_cut, frequency_cut, 'lowpass', fs=frequency_sampling, analog=False)
    filtered_data = scipy.signal.filtfilt(b=b, a=a, x=data, method='gust')
    return pd.Series(data=filtered_data, index=data.axes[0])


def _apply_low_pass_butter_filter(data: pd.Series) -> pd.Series:
    order = 4
    frequency_cut = 1 / 2000  # Hz (-3dB point)
    frequency_sampling = 1  # 1Hz

    # Prepare filter
    (b, a) = scipy.signal.butter(order, frequency_cut, 'lowpass', fs=frequency_sampling, analog=False)
    # Apply filter
    filtered_data = scipy.signal.filtfilt(b=b, a=a, x=data)
    return pd.Series(data=filtered_data, index=data.axes[0])
