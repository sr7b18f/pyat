import locale

import datetime
import os
import tempfile
from shutil import rmtree

import numpy as np
import pyfes
from dateutil import rrule

from pyat.core.tide.common import array_commons as util


# pylint: disable=W0621
from pyat.core.utils.monitor import ProgressMonitor


class FESTideComputer:
    def __copy_and_replace_pattern(
        self, input_path: str, output_path: str, input_pattern: str, output_pattern: str
    ) -> str:
        """
        Copy input file and replace string occurences
        Args:
            input_path : input file path
            output_path : output file path
            input_pattern : source pattern to be replaced
            output_pattern : destination pattern

        Returns:
            the output file name
        """
        with open(input_path, "r", encoding=locale.getpreferredencoding()) as file_in, open(
            output_path, "w", encoding=locale.getpreferredencoding()
        ) as file_out:
            lines = file_in.readlines()
            file_out.writelines([x.replace(input_pattern, output_pattern) for x in lines])
        return output_path

    def __init__(self, model_dir_path: str):
        """Initialize parameters for FES Model"""
        module_dir = os.path.realpath(os.path.dirname(__file__))
        self.model_dir = model_dir_path

        # I do not know why but relative paths in .ini file fails on windows.
        # Copy .ini files in a temp directory replace relative path with static path where datamodel is deployed

        self.tmpdir = tempfile.mkdtemp(suffix="_tide_model")
        self.ocean_tide_parameters = self.__copy_and_replace_pattern(
            input_path=os.path.join(module_dir, "file_pattern", "ocean_tide.ini"),
            output_path=os.path.join(self.tmpdir, "ocean_tide.ini"),
            input_pattern="./ocean_tide",
            output_pattern=os.path.join(self.model_dir, "ocean_tide"),
        )

        self.load_tide_parameters = self.__copy_and_replace_pattern(
            input_path=os.path.join(module_dir, "file_pattern", "load_tide.ini"),
            output_path=os.path.join(self.tmpdir, "load_tide.ini"),
            input_pattern="./load_tide",
            output_pattern=os.path.join(self.model_dir, "load_tide"),
        )

        self.ocean_tide_extrapolated_parameters = self.__copy_and_replace_pattern(
            input_path=os.path.join(module_dir, "file_pattern", "ocean_tide_extrapolated.ini"),
            output_path=os.path.join(self.tmpdir, "ocean_tide_extrapolated"),
            input_pattern="./ocean_tide_extrapolated",
            output_pattern=os.path.join(self.model_dir, "ocean_tide_extrapolated"),
        )

    def __del__(self):
        rmtree(self.tmpdir)

    def compute_tides(self, longitudes: np.ndarray, latitudes: np.ndarray, dates: np.ndarray, monitor: ProgressMonitor):
        """
        Compute tide for latitudes, longitudes and dates with FES2014 model,


        FES2014 was produced by NOVELTIS, LEGOS, CLS Space Oceanography Division and CNES. It is distributed by AVISO, with support from CNES (http://www.aviso.altimetry.fr/)


        Bibliography

        Lyard F., L. Carrere, M. Cancet, A. Guillot, N. Picot: FES2014, a new finite elements tidal model for global ocean, in preparation, to be submitted to Ocean Dynamics in 2016.

        Carrere L., F. Lyard, M. Cancet, A. Guillot, N. Picot: FES 2014, a new tidal model - Validation results and perspectives for improvements, presentation to ESA Living Planet Conference, Prague 2016.

        Args:
            longitudes: the longitudes array, shape of latitudes and longitudes must always be equals
            latitudes: the latitudes array, shape of latitudes and longitudes must always be equals
            dates: the date of interest array either datetime object or 'datetime64[us]' numpy array. Lenght of dates or (latitude or longitude) must be equals or of size 1


        Returns:
            (lats,lons,dates,tide_msl,tide_base,lp,load,load_lp) tuple containing
            lats: A latitude vector array
            longs: A longitude vector array
            dates: A date vector array, either datetime object or datetime64[us] depending of input parameter tide
            tide_msl : Tide relative to MSL, tide_msl = tide_base + lp + load + load_lp
            tide_base : Computed height of the diurnal and semi-diurnal constituents of the tidal spectrum
            lp : Computed height of the long period wave constituents of the tidal spectrum
            load : Computed height for the load components
            load_lp : Computed height of the long period wave constituents for the load components

        """

        monitor.begin_task(name="Computing tide on FES model", n=2)
        longitudes, latitudes, dates = util.reshape_args(lats=latitudes, lons=longitudes, dates=dates)
        # we need to use datetime64 type, fes model seems to interpret python datetime objects as local time instead of UTC
        dates = dates.astype("datetime64[us]")
        # Create handler
        short_tide = pyfes.Handler("ocean", "io", self.ocean_tide_extrapolated_parameters)
        radial_tide = pyfes.Handler("radial", "io", self.load_tide_parameters)
        tide, lp, _ = short_tide.calculate(longitudes, latitudes, dates)
        monitor.worked(1)
        # set units to meter
        tide *= 0.01
        lp *= 0.01
        load, load_lp, _ = radial_tide.calculate(longitudes, latitudes, dates)
        load *= 0.01
        load_lp *= 0.01
        monitor.worked(1)
        return latitudes, longitudes, dates, tide + lp + load, tide, lp, load, load_lp


if __name__ == "__main__":
    predictor = FESTideComputer(model_dir_path="C:/data/datasets/Tide/fes2014")
    # Creating the time series
    date = datetime.datetime(2003, 1, 1)
    #dates_ = np.array([date + datetime.timedelta(seconds=item * 3600 / 10) for item in range(24 * 10 * 12)])
    dates_ = np.array([date + datetime.timedelta(minutes=item*10) for item in range(24*6)])

    year = 2003
    # compute FES local Lowest Astronomical Tide over 20 years, with a 10 min resolution
    date_start = datetime.datetime(year=year, month=1, day=1, hour=0, minute=0, second=0, tzinfo=datetime.timezone.utc)
    date_end = datetime.datetime(
        year=2021, month=1, day=1, hour=0, minute=0, second=0, tzinfo=datetime.timezone.utc
    )
    time_elapsed = date_end - date_start
    duration_minute = int(time_elapsed.total_seconds() / 60)
    dates_for_lowest_astro_tide = list(
        rrule.rrule(freq=rrule.MINUTELY, interval=10, dtstart=date_start, count=int(duration_minute / 10) + 1)
    )
    dates_for_lowest_astro_tide = np.array(dates_for_lowest_astro_tide)
    dates_count = len(dates_for_lowest_astro_tide)
    dates_=dates_for_lowest_astro_tide

    latitudes = np.full(dates_.shape, 59.195)
    longitudes = np.full(dates_.shape, -7.688)

    from timeit import default_timer as timer

    ts = timer()
    latitudes, longitudes, dates_, tides, tide_ori, long_period, load_, load_lp_, *_ = predictor.compute_tides(
        latitudes=latitudes, longitudes=longitudes, dates=dates_, monitor=ProgressMonitor()
    )
    print(f"compute_tides on year , {(timer() - ts):.2f} sec.")
    import pandas as pd

    df = pd.DataFrame(columns=["Tides", "long_period","Pure Tide", "Geo Tide"])
    df["Tides"]=tide_ori
    df["long_period"]=long_period
    df["Pure Tide"]=tide_ori + long_period
    df["Geo Tide"]=tides

    print(df.head())
