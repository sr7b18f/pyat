import os.path
from typing import Optional, List

import netCDF4 as nc
import numpy as np

from pyat.core.techsas.pytechsas.sensor.techsas_file import read_techsas_file


class NavigationFileProxy:
    def __init__(self):
        self.filename = None
        self.times = None
        self.longitudes = None
        self.latitudes = None
        self.altitudes = None
        self.types = None


def interpolate_navigation(
    time_navigation: np.ndarray,
    latitudes: np.ndarray,
    longitudes: np.ndarray,
    time_sensor: np.ndarray,
    altitudes: Optional[np.ndarray] = None,
) -> (np.ndarray, np.ndarray):
    """Interpolate navigation data based on the given time sensor
    @:param time_navigation the navigation time vector
    @:param latitudes : the navigation latitude vector
    @:param longitudes: the navigation longitude vector
    @:param altitudes: the navigation altitude vector
    @:return (longitude, latitudes) a tuple containing the interpolated longitudes and latitude
    """
    # interpolate data
    if time_navigation[0] > time_sensor[0]:
        raise Exception(
            f"Not supported case : Starting date ({time_navigation[0]}) from navigation file is higher than starting date of time sensor ({time_sensor[0]})"
        )

    if time_navigation[-1] < time_sensor[-1]:
        raise Exception(
            f"Not supported case : Last date  ({time_navigation[-1]}) from navigation file is less than last date of time sensor ({time_sensor[-1]})"
        )

    # We need to change time to float in order to be able to interpolate data
    # We substract the lowest date as a reference date
    reference_date = time_navigation[0]
    time_navigation_float = (time_navigation - reference_date) / np.timedelta64(1, "s")
    time_sensor_float = (time_sensor - reference_date) / np.timedelta64(1, "s")

    # Need to check for interpolation around 180/-180 for longitudes
    interpolated_longitudes = np.interp(time_sensor_float, time_navigation_float, longitudes)
    interpolated_latitudes = np.interp(time_sensor_float, time_navigation_float, latitudes)

    if altitudes is None:
        interpolated_altitudes = None
    else:
        interpolated_altitudes = np.interp(time_sensor_float, time_navigation_float, altitudes)

    return interpolated_longitudes, interpolated_latitudes, interpolated_altitudes


def read_nvi(nvi_file: str) -> NavigationFileProxy:
    """Read a nvi file and return time, latitude, longitude and altitude"""
    with nc.Dataset(nvi_file) as dataset:
        time = dataset.variables["mbDate"]
        time_frac = dataset.variables["mbTime"]
        time.set_auto_scale(False)  # disable autoscale to be able to set a starting date, force to 1970
        time_values = time[:]
        time_frac_values = time_frac[:]
        # time_values = nc.num2date(time[1:100], calendar="julian", units="days since 1970-01-01 00:00:00",)

        npd = time_values.astype("timedelta64[D]")
        npd2 = time_frac_values.astype("timedelta64[ms]")

        output = NavigationFileProxy()
        output.filename = nvi_file
        output.times = np.datetime64("1970-01-01 00:00:00") + npd + npd2
        output.latitudes = dataset.variables["mbOrdinate"][:]
        output.longitudes = dataset.variables["mbAbscissa"][:]
        output.altitudes = dataset.variables["mbAltitude"][:]
        types = dataset.variables["mbPType"]
        types.set_auto_scale(False)
        types.set_auto_chartostring(False)
        output.types = np.frombuffer(types[:], dtype=np.uint8)

        return output


def read_techsas_nav(nav_file: str) -> NavigationFileProxy:
    """Read a nav file and return time, latitude, longitude and altitude"""
    (time_values, dictionary, frame_period) = read_techsas_file(nav_file)
    output = NavigationFileProxy()
    output.filename = nav_file
    output.times = time_values
    output.latitudes = dictionary["lat"][1]
    output.longitudes = dictionary["long"][1]
    output.altitudes = dictionary["alt"][1]
    if "mode" in dictionary:
        output.types = dictionary["mode"][1]

    return output


def read_navigation(navigation_file: str) -> NavigationFileProxy:
    """Read a navigation file and return time, latitude, longitude and altitude"""
    (base, ext) = os.path.splitext(navigation_file)
    if ext == ".nvi":
        return read_nvi(navigation_file)
    else:
        return read_techsas_nav(navigation_file)


def aggregate_navigation_files(navigation_files: List[str]) -> NavigationFileProxy:
    """Read multiple navigation files and return time, latitude, longitude and altitude"""
    # read navigation files
    aggr_times_nav = []
    aggr_longitudes_nav = []
    aggr_latitudes_nav = []
    aggr_altitudes_nav = []

    # find and read navigation file
    for nav_file in navigation_files:
        nav_data = read_navigation(nav_file)
        aggr_times_nav.extend(nav_data.times)
        aggr_longitudes_nav.extend(nav_data.longitudes)
        aggr_latitudes_nav.extend(nav_data.latitudes)
        aggr_altitudes_nav.extend(nav_data.altitudes)

    # sort nav points by time
    indexer = np.asarray(aggr_times_nav).argsort()
    output = NavigationFileProxy()
    output.times = np.asarray(aggr_times_nav)[indexer]
    output.longitudes = np.asarray(aggr_longitudes_nav)[indexer]
    output.latitudes = np.asarray(aggr_latitudes_nav)[indexer]
    output.altitudes = np.asarray(aggr_altitudes_nav)[indexer]

    return output
