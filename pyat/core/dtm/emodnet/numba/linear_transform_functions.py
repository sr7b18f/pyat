#! /usr/bin/env python3
# coding: utf-8

import numpy as np
from numba import prange, njit


# Linear transform process
@njit(
    ["float32[:,:], float32[:, :], uint8[:,:], int32, int32", "int32[:,:], float32[:, :], uint8[:,:], int32, int32"],
    parallel=True,
    cache=True,
)
def linear_transform(o_arr, i_arr, mask, a, b):
    """Optimized function numba used for the linear transformation. If the cell is in the
    geo_mask and is not masked, then the result cell is set to

    output =  input * a + b

    Arguments:
        o_arr {np.array} -- Array of the output layer.
        i_arr {np.array} -- Array of the input layer.
        mask {np.array} -- Array of the geographical zone processed.
        a {float} --
        b {float} --

    Returns:
        [np.array] -- Output array.
    """
    for i in prange(o_arr.shape[0]):
        for j in prange(o_arr.shape[1]):
            if mask[i, j] and not np.isnan(i_arr[i, j]):
                o_arr[i, j] = i_arr[i, j] * a + b
            else:
                o_arr[i, j] = i_arr[i, j]

    return o_arr


@njit(
    ["float32[:,:], float32[:, :], uint8[:,:], int32", "int32[:,:], float32[:, :], uint8[:,:], int32"],
    parallel=True,
    cache=True,
)
def linear_transform_stdev(o_arr, i_arr, mask, a):
    """Optimized function numba used for the linear transformation. If the cell is in the
    geo_mask and is not masked, then the result cell is set to

    output =  input * abs(a)

    Arguments:
        o_arr {np.array} -- Array of the output layer.
        i_arr {np.array} -- Array of the input layer.
        mask {np.array} -- Array of the geographical zone processed.
        a {float} --

    Returns:
        [np.array] -- Output array.
    """
    for i in prange(o_arr.shape[0]):
        for j in prange(o_arr.shape[1]):
            if mask[i, j] and not np.isnan(i_arr[i, j]):
                o_arr[i, j] = i_arr[i, j] * abs(a)
            else:
                o_arr[i, j] = i_arr[i, j]

    return o_arr
