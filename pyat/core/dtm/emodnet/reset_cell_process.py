#! /usr/bin/env python3
# coding: utf-8

import netCDF4 as nc
import numpy as np

import pyat.core.dtm.dtm_driver as dtm_driver
import pyat.core.dtm.emodnet.cdi_layer_util as cdi_util
import pyat.core.dtm.emodnet.dtm_standard_constants as DtmConstants
import pyat.core.dtm.emodnet.emodnet_constants as const
import pyat.core.dtm.emodnet.numba.reset_cell_functions as nb
import pyat.core.dtm.utils.process_utils as process_util
import pyat.core.utils.argument_utils as arg_util
import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor
from pyat.core.dtm.mask import compute_geo_mask_from_dtm


class ResetCellProcess:
    """Reset Cell process class. Can reset cells in function of filters parameters."""

    def __init__(
        self,
        i_paths: list,
        o_paths: list = None,
        suffix: str = "-zeroed",
        overwrite: bool = False,
        cdi: str = const.NO_CDI,
        filters: list = None,
        mask: str = None,
        reverse_filtering: bool = False,
        monitor=DefaultMonitor,
    ):
        """By default, the name of the output file is i_path + "-zeroed". No filter of zone,
        no filter by cdi, no filter by layer.

        Arguments:
            i_paths {list} -- Input file list (.nc).
            o_paths {list} -- Optional output file list (.nc). (default: {None})
            suffix {str} -- Suffix of generated output path. Used when o_paths is empty. (default: {-zeroed})
            overwrite {bool} -- true to overwrite output file if exists. (default: {False})
            cdi {str} -- Name of the cdi. (default: {"No CDI filter"})
            filters {list} -- List of filters. (default: {None})
            mask {list} -- Mask file list. (default: {None})
            reverse_filtering {bool} -- true to reset all data, except the selected
            monitor {list} -- Progress monitor. (default is a silent monitor: {DefaultMonitor})

        Raises:
            TypeError: Not good format for lat / lon.
            ValueError: Raise an exception if the layer isn't in the list layers_filter.
            ValueError: Raise an exception if the operation filter isn't in the list name_oper.
        """
        self.i_paths = i_paths
        self.o_paths = o_paths
        self.suffix = suffix
        self.overwrite = overwrite
        self.cdi = cdi
        self.mask_files = arg_util.parse_list_of_files("mask", mask)
        self.reverse_filtering = reverse_filtering
        self.monitor = monitor

        self.filters = []
        if filters:
            for f in filters:
                oneFilter = {}
                if f["layer"] in DtmConstants.LAYERS:
                    oneFilter["layer"] = f["layer"]
                else:
                    raise ValueError(f'The name of the layer {f["layer"]} isn\'t processed.')

                if f["oper"] in const.OPERATION:
                    oneFilter["oper"] = f["oper"]
                else:
                    raise ValueError(f'The operation {f["oper"]} isn\'t processed.')

                oneFilter["a"] = float(f["a"])
                if "b" in f:
                    oneFilter["b"] = float(f["b"])

                self.filters.append(oneFilter)

        self.logger = log.logging.getLogger(self.__class__.__name__)

    def __create_mask(self, i_driver: dtm_driver.DtmDriver, geo_mask_array: np.array) -> np.ndarray:
        """Create global mask with cdi_mask, geo_mask and filters_mask.
        If no filters, mask is an array full of 1.

        Arguments:
            i_driver -- input DTM.

        Raises:
            ValueError: Cdi name is not in the layer cdi_reference.

        Returns:
            [np.array] -- Mask array.
        """
        # SetUp
        shape = i_driver[DtmConstants.ELEVATION_NAME].shape

        # Process
        # FILTER CDI
        filters_cdi = []
        if self.cdi != const.NO_CDI:
            index_array = []
            if DtmConstants.CDI in i_driver:
                index_array = np.where(i_driver[DtmConstants.CDI][:] == self.cdi)[0]
                filters_cdi = np.full(shape, False, dtype=bool)
            if len(index_array) == 0:
                self.logger.warning(f"The cdi {self.cdi} isn't in the input file {i_driver.get_file_path()}.")
                # CDI in not in the given file, thus no CDI filter is applied, in such a case the mask is filled with zeros
            else:
                for cdi_index in index_array:
                    filters_cdi |= i_driver[DtmConstants.CDI_INDEX][:] == int(cdi_index)
        if len(filters_cdi) == 0:
            filters_cdi = np.full(shape, True, dtype=bool)

        # filter mask, set to True when a given value shall be erased
        if self.filters:
            filters_mask = np.full(shape, True, dtype=bool)
            for f in self.filters:
                name = f["layer"]
                oper = f["oper"]
                a = f["a"]
                if "b" in f:
                    b = f["b"]
                data = i_driver[name][:]

                if oper == const.EQUAL:
                    filters_mask = filters_mask & (data == a)
                elif oper == const.NOT_EQUAL:
                    filters_mask = filters_mask & (data != a)
                elif oper == const.LESS_THAN:
                    filters_mask = filters_mask & (data <= a)
                elif oper == const.MORE_THAN:
                    filters_mask = filters_mask & (data >= a)
                elif oper == const.BETWEEN:
                    filters_mask = filters_mask & ((data >= a) & (data <= b))
        else:
            filters_mask = np.full(shape, True, dtype=bool)
        # mask geographic with filters
        mask = geo_mask_array & filters_cdi & filters_mask
        if np.ma.is_masked(mask):
            mask = np.ma.filled(mask, fill_value=False)

        mask = np.array((~mask if self.reverse_filtering else mask), dtype=np.uint8)
        return mask

    def __process_data(
        self, i_driver: dtm_driver.DtmDriver, o_driver: dtm_driver.DtmDriver, monitor: ProgressMonitor
    ) -> None:
        # if a CDI is given, we will apply reset cell only where CDI id match one of the given CDI
        # note : only one CDI is accepted
        """Create the variable and process it.

        Arguments:
            ind {int} -- Number of the processed file.
        """

        # Initialize output file
        process_util.initialize_output_file(i_driver, o_driver, process_name=self.__class__.__name__)

        o_file = o_driver.dataset

        # Used for the log
        count = 0
        n = len(i_driver.get_layers())
        monitor.set_work_remaining(n + 1)

        geo_mask = compute_geo_mask_from_dtm(i_driver.get_file_path(), self.mask_files)
        # geo_mask is set to one
        geo_mask = geo_mask > 0  # convert to boolean array
        mask = self.__create_mask(i_driver, geo_mask)

        for name, variable in i_driver.get_layers().items():
            if name in DtmConstants.LAYERS:
                count += 1
                log.info_progress_layer(self.logger, "layer", name, count, n)

                # Create variable in the o_files[ind].
                o_file.createVariable(name, variable.datatype, variable.dimensions)
                self.__process_layer(i_driver, name, o_file, mask)

            elif name == DtmConstants.CDI:
                # Copy cdi layer, it will be cleaned later
                count += 1
                log.info_progress_layer(self.logger, "layer", name, count, n)
                o_driver.create_cdi_reference_variable(cdi_util.trim_string_array(variable[:]))

            monitor.worked(1)

        # now once everything is processed, clean cdi
        cdi_util.clean_cdi(o_file)
        monitor.worked(1)

    def __process_layer(self, i_driver: dtm_driver.DtmDriver, name: str, o_file: nc.Dataset, mask) -> None:
        """Process layer.

        Arguments:
            name {str} -- Name of the layer.
            ind {int} -- Indice of the input file.
        """
        # copy variable attributes all at once via dictionary
        o_file[name].setncatts(i_driver[name].__dict__)

        # Initialisation
        o_data = o_file[name][:].data
        i_data = i_driver[name][:].data
        m_val = i_driver[name]._FillValue

        # Reset selected cells
        o_file[name][:] = nb.reset_layer(o_data, i_data, m_val, mask)

    def __call__(self) -> None:
        process_util.process_each_input_file_to_output_file(
            self.__class__.__name__,
            self.i_paths,
            self.__process_data,
            self.logger,
            self.o_paths,
            self.suffix,
            self.overwrite,
            self.monitor,
        )
