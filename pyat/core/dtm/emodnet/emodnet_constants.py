#! /usr/bin/env python3
# coding: utf-8

# Operation filter for reset cells
EQUAL: str = "equal"
NOT_EQUAL: str = "not_equal"
LESS_THAN: str = "less_than"
MORE_THAN: str = "more_than"
BETWEEN: str = "between"
OPERATION = ["equal", "not_equal", "less_than", "more_than", "between"]

NO_CDI = "No CDI filter"

# MERGE
# Type of geo_bounds for merge
REFERENCE = "ref"
UNION = "all"
CUSTOM = "custom"

# Type of merge
FILL = "fill"
SIMPLE = "simple"
