This is a fork of https://github.com/coronis-computing/heightmap_interpolation
done for integration purpose in Globe and pyat project

Differences are :
- import renaming to be relatives to the core package
- remove useless imports
- import matplotlib only when needed
- ...
