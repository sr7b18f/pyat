#! /usr/bin/env python3
# coding: utf-8

import numpy as np

import pyat.core.dtm.dtm_driver as dtm_driver
import pyat.core.dtm.emodnet.cdi_layer_util as cdi_util
import pyat.core.dtm.emodnet.dtm_standard_constants as DtmConstants
import pyat.core.dtm.emodnet.numba.sanity_check_functions as nb
import pyat.core.dtm.utils.process_utils as process_util
import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor


class SanityCheckProcess:
    """Sanity Check process class. This process will perform a sanity check on the given files

    interpolation flag : Check for interpolation flag layer. For each cell, set it to zero
    (not interpolated) if depth value exists and if its interpolation flag is set to invalid value.
    Leave it unchanged if it is already set to a valid value.

    Compress CDI metadata or Force cdi: If one and only one CDI is declared in the nc file, and if this
    CDI is not used. i.e. the CDI_LAYER is fully filled with invalid values, then set this CDI to valid
    depth values in the file.
    Compress CDI metadata. Remove declared CDI entries that are not effectively
    used in the file.
    """

    def __init__(
        self,
        i_paths: list,
        o_paths: list = None,
        suffix="-cleaned",
        overwrite: bool = False,
        interp: bool = False,
        cdi: bool = False,
        monitor=DefaultMonitor,
    ):
        """

        Arguments:
            i_paths {list} -- Input file list (.nc).
            o_paths {list} -- Optional output file list (.nc). (default: {None})
            suffix {str} -- Suffix of generated output path. Used when o_paths is empty. (default: {-cleaned})
            overwrite {bool} -- true to overwrite output file if exists. (default: {False})
            interp {bool} -- Name of the cdi. (default: {False})
            cdi {bool} -- List of filters. (default: {False})
            monitor {list} -- Progress monitor. (default is a silent monitor: {DefaultMonitor})

        """
        self.i_paths = i_paths
        self.o_paths = o_paths
        self.suffix = suffix
        self.overwrite = overwrite
        self.interp = interp
        self.cdi = cdi
        self.monitor = monitor

        if not (self.cdi or self.interp):
            raise ValueError(
                "Useless process without parameters.\nStop the program.\nPlease enter at least one of the following parameters:\n"
                "- Sanity check by interpolation with the option -in, --interp.\n"
                "- Sanity check by cdi with the option -c, --cdi."
            )

        self.logger = log.logging.getLogger(self.__class__.__name__)

    def __process_data(
        self, i_driver: dtm_driver.DtmDriver, o_driver: dtm_driver.DtmDriver, monitor: ProgressMonitor
    ) -> None:
        """Create variable and process it. Copy or clean layers.

        Arguments:
            ind {int} -- Number of the processed file.
        """
        # Initialize output file
        process_util.initialize_output_file(i_driver, o_driver, process_name=self.__class__.__name__)

        # Used for the log
        count = 0
        n = len(i_driver.get_layers())
        monitor.set_work_remaining(n)

        for name in i_driver.get_layers().keys():
            if name in DtmConstants.LAYERS:
                if name != DtmConstants.CDI_INDEX:
                    count += 1
                    # Create variable in the o_drivers[ind].
                    o_driver.add_layer(name)
                    # Copy variable attributes all at once via dictionary
                    log.info_progress_layer(self.logger, "layer", name, count, n)
                    self.__process_layer(i_driver, name, o_driver)

            elif name in [DtmConstants.CDI]:
                # Create cdi_index before the cdi_ref
                if not DtmConstants.CDI_INDEX in o_driver:
                    count += 1
                    name_tmp = DtmConstants.CDI_INDEX
                    o_driver.add_layer(name_tmp)
                    log.info_progress_layer(self.logger, "layer", name, count, n)
                    self.__process_layer(i_driver, name_tmp, o_driver)

                # Copy cdi_ref
                count += 1
                log.info_progress_layer(self.logger, "layer", name, count, n)
                self.__process_cdi_ref(i_driver, o_driver)

            monitor.worked(1)

    def __process_layer(self, i_driver: dtm_driver.DtmDriver, name: str, o_driver: dtm_driver.DtmDriver) -> None:
        """Copy layer or update the interpolation_flag layer.

        Arguments:
            name {str} -- Name of the layer.
            ind {int} -- Indice of the input file.
        """

        # Initialisation
        o_data = o_driver[name][:].data
        i_data = i_driver[name][:].data
        m_val = i_driver[name]._FillValue
        i_elev = i_driver[DtmConstants.ELEVATION_NAME][:].data

        if name == DtmConstants.INTERPOLATION_FLAG and self.interp:
            self.logger.debug("Update interpolation flag.")
            o_driver[name][:] = nb.update_interp(o_data, i_data, i_elev, m_val)
        else:
            o_driver[name][:] = i_driver[name][:]

    def __process_cdi_ref(self, i_driver: dtm_driver.DtmDriver, o_driver: dtm_driver.DtmDriver) -> None:
        """Set the long_name attributes and copy data for the cdi_ref layer.
        Compress the cdi_reference variable and report it into the cdi_index.

        Arguments:
            name {str} -- Name of the layer.
            ind {int} -- Indice of the input file.
        """
        # Initialisation
        o_data = np.empty([0], dtype=dtm_driver.get_type(DtmConstants.CDI))
        i_elev = i_driver[DtmConstants.ELEVATION_NAME][:]
        o_cdi_index = o_driver[DtmConstants.CDI_INDEX][:].data
        m_val = i_driver[DtmConstants.CDI_INDEX]._FillValue
        i_data = i_driver[DtmConstants.CDI][:]
        i_data_only = i_data[i_data != ""]
        i_cdi_index = i_driver[DtmConstants.CDI_INDEX][:].data

        if self.cdi:
            self.logger.debug("Compress cdis.")
            for i_count, cdi in enumerate(i_data_only):
                if not cdi in o_data:
                    o_data = np.append(o_data, cdi)
                index = int(np.where(o_data == cdi)[0])

                if len(i_data_only) == 1 and (i_cdi_index == m_val).all():
                    # Do like Set Cdi process
                    i_count = m_val

                o_cdi_index = nb.update_cdi_index(o_cdi_index, i_cdi_index, i_elev, index, i_count)
        else:
            o_data = i_data
            o_cdi_index = i_cdi_index

        o_driver.create_cdi_reference_variable(cdi_util.trim_string_array(o_data))
        o_driver[DtmConstants.CDI_INDEX][:] = o_cdi_index

    def __call__(self) -> None:
        process_util.process_each_input_file_to_output_file(
            self.__class__.__name__,
            self.i_paths,
            self.__process_data,
            self.logger,
            self.o_paths,
            self.suffix,
            self.overwrite,
            self.monitor,
        )
