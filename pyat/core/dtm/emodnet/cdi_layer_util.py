from typing import List
import netCDF4 as nc
import numpy as np
from scipy import ndimage

import pyat.core.dtm.dtm_driver as driver
import pyat.core.dtm.emodnet.dtm_standard_constants as DtmConstants
import pyat.core.dtm.emodnet.numba.set_cdi_functions as nb


def clean_double(old_cdi_names: List[str]):
    """
    remove doubles in a list of cdi, order the list and put empty strings at the end

    return uniques, inverse where
        uniques is a list of unique names
        inverse an array allowing to map old index to new ones
    """
    if len(old_cdi_names) == 0:
        return old_cdi_names, []
    uniques, inverse = np.unique(old_cdi_names, return_inverse=True)
    # result are sorted, which means that if the list of string contains an empty string
    # it will be located at the beginning. We want to move it at the end
    if uniques[0] == "":
        uniques = np.roll(uniques, -1)  # move empty string to the end
        inverse = inverse - 1  # shift all index by one
        inverse[inverse == -1] = len(uniques) - 1  # reset index to empty string to the end

    return uniques, inverse


def trim_string_array(string_array: List[str]) -> List[str]:
    """
    remove trailing empty strings in the given array but keep the ones in the middle
    return a shorter list of empty strings without the trailing empty strings
    """
    # find the last non empty name
    last_value_index = None
    for index, name in enumerate(string_array):
        if isinstance(name, str) and name.strip() != "":
            last_value_index = index
    if last_value_index is None:
        return []
    else:
        return string_array[0 : last_value_index + 1]


def reset_all_cdi_id(dtm_file: nc.Dataset):
    _reset_all_cdi_id(dtm_file)


def _reset_all_cdi_id(dtm_file: nc.Dataset):
    """reset all cdi id in variable DtmConstants.CDI, ie set all values to an empty string"""
    old_cdi_names = dtm_file[DtmConstants.CDI][:]
    # trim to avoid to reset last values to "" where they already have those values
    old_cdi_names = trim_string_array(old_cdi_names)
    for index, name in enumerate(old_cdi_names):
        dtm_file[DtmConstants.CDI][index] = ""


def _clean_multiple_cdi_entries(dtm_file: nc.Dataset) -> None:
    """Parse a netcdf dtm dataset, remove duplicate entries in DtmConstants.CDI and update accordingly cdi_index layer"""
    old_cdi_names = dtm_file[DtmConstants.CDI][:]
    new_ids, index_map = clean_double(old_cdi_names)

    # reset all values of cdi
    _reset_all_cdi_id(dtm_file)

    # copy new values
    for index, name in enumerate(new_ids):
        # VLEN can be only accessed one at a time
        dtm_file[DtmConstants.CDI][index] = name
    missing = driver.get_missing_value(DtmConstants.CDI_INDEX)
    updated_values = nb.remap_cdi_index(dtm_file[DtmConstants.CDI_INDEX][:], index_map, missing)
    dtm_file[DtmConstants.CDI_INDEX][:] = updated_values


def clean_cdi(dataset: nc.Dataset) -> None:
    """
    Remove unused CDI id; shift index values and remove entry in DtmConstants.CDI
    Warning does not remove doubles CDI entry
    """
    if not DtmConstants.CDI_INDEX in dataset.variables:
        return

    # check if we have double entries:
    index_names = dataset[DtmConstants.CDI][:]

    # no cdi , exit
    if len(index_names) == 0:
        return

    # remove trailing empty strings
    index_names = trim_string_array(index_names)
    # do we have doubles
    uniques = np.unique(index_names)
    if any(index_names == "") or len(uniques) != len(index_names):
        # we at least got an empty CDI or a double, clean all and update indexes
        _clean_multiple_cdi_entries(dataset)

    index_values = dataset[DtmConstants.CDI_INDEX]
    index_values_used = np.unique(dataset[DtmConstants.CDI_INDEX])
    if np.ma.is_masked(index_values_used):
        index_values_used = index_values_used[~index_values_used.mask].data
    # now we remove values that are not referenced, each new id will

    # clean up CDI names
    old_cdi_names = dataset[DtmConstants.CDI][:]
    _reset_all_cdi_id(dataset)

    for o_index, i_index in enumerate(index_values_used):
        # VLEN can be only accessed one at a time
        dataset[DtmConstants.CDI][o_index] = old_cdi_names[i_index]

    # clean up CDI index, ie apply new indexes
    # create a map from old index to new index
    missing = driver.get_missing_value(DtmConstants.CDI_INDEX)
    if len(index_values_used) > 0:
        max_index = np.max(index_values_used)
        index_map = np.empty(max_index + 1, dtype=np.int32)
        for index, value in enumerate(index_values_used):
            index_map[value] = index
        updated_values = nb.remap_cdi_index(index_values[:], index_map, missing)
        dataset[DtmConstants.CDI_INDEX][:] = updated_values
    else:
        dataset[DtmConstants.CDI_INDEX][:] = missing


def update_cdi(o_dtm_driver, mask):
    """
    update cdi_index of the dtm file
    """
    if DtmConstants.CDI_INDEX in o_dtm_driver:
        cdi_index = o_dtm_driver[DtmConstants.CDI_INDEX][:]
        labeled_mask, num_features = ndimage.label(mask)
        # browse each label but 0 (0 represents elevations in original file)
        for num_feature in range(1, num_features + 1):
            # keep only the hole of label num_feature
            feature_mask = np.where(labeled_mask == num_feature, True, False)
            # Dilation of the hole to reach some cells with CDI
            dilation_mask = np.logical_not(ndimage.binary_dilation(feature_mask))

            # Keep only CDI indexes over the hole
            cdi_on_feature = np.ma.masked_array(cdi_index, dilation_mask)
            # Statistics of the CDI index
            cdi, nb = np.unique(cdi_on_feature, return_counts=True)
            # Ignore cells without CDI
            nb[cdi.mask] = -1
            # Set the most encountered CDI index
            cdi_index[feature_mask] = cdi[np.argmax(nb)]

        o_dtm_driver[DtmConstants.CDI_INDEX][:] = cdi_index
