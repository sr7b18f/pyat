#! /usr/bin/env python3
# coding: utf-8

import datetime

import netCDF4 as nc
from osgeo import ogr, osr

import pyat.core.common.geo_file as gf
import pyat.core.dtm.dtm_driver as dtm_driver
import pyat.core.dtm.emodnet.cdi_layer_util as cdi_util
import pyat.core.dtm.emodnet.dtm_legacy_constants as OldIfr
import pyat.core.dtm.emodnet.dtm_standard_constants as NewFormatConst
import pyat.core.dtm.utils.process_utils as process_util
import pyat.core.utils.netcdf_utils as nc_util
import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import DefaultMonitor


class DTMMigrate:
    """
    Utility classe used to migrate netcdf3 dtm file to netcdf4 dtm format
    The nc format is inspired from the GEBCO nc release format and
    the NOAA grid format template https://www.nodc.noaa.gov/data/formats/nc/v2.0/grid.cdl
    """

    def __init__(
        self,
        i_paths: list,
        o_paths: list,
        overwrite: bool = False,
        monitor=DefaultMonitor,
    ):
        """Constructor.

        Arguments:
            i_paths {list} -- Input file list (.dtm).
            o_paths {list} -- Optional output file list (.nc). (default: {None})
            overwrite {bool} -- true to overwrite output file if exists. (default: {False})
            monitor {list} -- Progress monitor. (default is a silent monitor: {DefaultMonitor})
        """
        self.i_paths = i_paths
        self.o_paths = o_paths
        self.overwrite = overwrite
        self.monitor = monitor

        self.logger = log.logging.getLogger(self.__class__.__name__)

    def _migrateToDataset(
        self,
        inDataset: nc.Dataset,
        o_dtm_driver: dtm_driver.DtmDriver,
        monitor,
    ) -> None:
        outDtmFile = o_dtm_driver.dtm_file

        # Extent
        longitudes = inDataset.variables[OldIfr.VARIABLE_COLUMN][:]
        latitudes = inDataset.variables[OldIfr.VARIABLE_LINE][:]

        # the bounding box should take into account that the row/col coordinates are referred to the center
        # of the cells, so the real bounding box should be increased by half a cell size on each direction
        outDtmFile.west = longitudes[0] - outDtmFile.spatial_resolution_x / 2
        outDtmFile.east = longitudes[-1] + outDtmFile.spatial_resolution_x / 2
        outDtmFile.south = latitudes[0] - outDtmFile.spatial_resolution_y / 2
        outDtmFile.north = latitudes[-1] + outDtmFile.spatial_resolution_y / 2
        self.logger.info(
            f"Extent : west={outDtmFile.west} east={outDtmFile.east} north={outDtmFile.south} south={outDtmFile.south} "
        )

        o_dtm_driver.dataset.history = "Converted with Python MigrateDtm script from " + inDataset.filepath()

        # create a mapping between variable name and the function in charge of its mapping
        dic = {
            OldIfr.VARIABLE_DEPTH: NewFormatConst.ELEVATION_NAME,
            OldIfr.VARIABLE_DEPTH_SMOOTH: NewFormatConst.ELEVATION_SMOOTHED_NAME,
            OldIfr.VARIABLE_MAX_SOUNDING: NewFormatConst.ELEVATION_MAX,
            OldIfr.VARIABLE_MIN_SOUNDING: NewFormatConst.ELEVATION_MIN,
            OldIfr.VARIABLE_VSOUNDINGS: NewFormatConst.VALUE_COUNT,
            OldIfr.VARIABLE_STDEV: NewFormatConst.STDEV,
            OldIfr.VARIABLE_INTERPOLATION_FLAG: NewFormatConst.INTERPOLATION_FLAG,
            OldIfr.VARIABLE_CDI: NewFormatConst.CDI_INDEX,
            OldIfr.VARIABLE_REFLECTIVITY: NewFormatConst.BACKSCATTER,
            OldIfr.VARIABLE_MIN_ACROSS_DISTANCE: NewFormatConst.MIN_ACROSS_DISTANCE,
            OldIfr.VARIABLE_MAX_ACROSS_DISTANCE: NewFormatConst.MAX_ACROSS_DISTANCE,
            OldIfr.VARIABLE_ACCROSS_ANGLE: NewFormatConst.MAX_ACCROSS_ANGLE,
        }

        # Migrate variables
        n = len(inDataset.variables) + 1
        monitor.set_work_remaining(n)
        for v in inDataset.variables:
            if v in dic:
                data = inDataset.variables[v][:]
                o_dtm_driver.add_layer(dic[v], data)

            monitor.worked(1)

        # Write layers CDI. need to create a mapping
        if OldIfr.VARIABLE_CDI_INDEX in inDataset.variables:
            v = inDataset.variables[OldIfr.VARIABLE_CDI_INDEX]
            monitor.worked(1)
            ids = cdi_util.trim_string_array(nc.chartostring(v[:]))
            # we do not remove empty entries, just in case of bugs and empty values remaining
            # ids=ids[np.logical_not(ids == "")]
            o_dtm_driver.create_cdi_reference_variable(cdis=ids)

        cdi_util.clean_cdi(inDataset)

    def _upgradeWkt(self, esri_pe_string: str) -> str:
        """
            Utility function to reinterpret projection WKT description from old MNT to a GDAL compatible description
        Args:
            esri_pe_string: projection description in WKT from old MNT format
        Returns:
            reformatted esri_pe_string
        """
        if "Lambert" in esri_pe_string:
            if "Scale_Factor" in esri_pe_string:
                esri_pe_string = esri_pe_string.replace("Lambert_Conformal_Conic",
                                                        "Lambert_Conformal_Conic_1SP")
            else:
                esri_pe_string = esri_pe_string.replace("Lambert_Conformal_Conic",
                                                        "Lambert_Conformal_Conic_2SP")
        elif "\"Mercator\"" in esri_pe_string:
            if "Scale_Factor" in esri_pe_string:
                esri_pe_string = esri_pe_string.replace("Mercator", "Mercator_1SP")
            else:
                esri_pe_string = esri_pe_string.replace("Mercator", "Mercator_2SP")
        elif "Equidistant_Cylindrical" in esri_pe_string:
            esri_pe_string = esri_pe_string.replace("Equidistant_Cylindrical", "Equirectangular")
        return esri_pe_string

    def __call__(self) -> None:
        """
            main entry point for this class, will migrate an EMODnet nc format to a new nc format
        :param file_name_in:
        :param file_name_out:
        :param monitor : monitor the progress of the activity (0.0 = started ... 1.0 = finished)
        :return:
            Raised exceptions :
                - FileNotFoundError when file_name_in does not exist
                - PermissionError when file_name_in is not readable or file_name_out is not writable
                - IOError when any IO error occurs
        """
        start = datetime.datetime.now()
        self.monitor.set_work_remaining(len(self.i_paths))
        files_in_error = []
        for ind, path in enumerate(self.i_paths):
            self.logger.info(f"Start migration of file {path} to file {self.o_paths[ind]}.")
            start_tmp = datetime.datetime.now()
            sub_monitor = self.monitor.split(1)

            try:
                with nc.Dataset(path) as inDataset:  # Open the old format DTM
                    spatial_ref = gf.SR_WGS_84
                    # Check projection of the input DTM
                    try:  # check input projection is not lat/long
                        if inDataset.mbProj4String:
                            spatial_ref = osr.SpatialReference()
                            if spatial_ref.ImportFromProj4(inDataset.mbProj4String) != ogr.OGRERR_NONE:
                                self.logger.error(
                                    f"Migration not available for this projected files. (Input file projection : {inDataset.mbProj4String})"
                                )
                                continue
                    except AttributeError: # mbProj4String not available in dataset
                        if inDataset.variables[OldIfr.VARIABLE_DEPTH].esri_pe_string:
                            esri_pe_string = self._upgradeWkt(inDataset.variables[OldIfr.VARIABLE_DEPTH].esri_pe_string)
                            self._upgradeWkt(esri_pe_string)
                            spatial_ref = osr.SpatialReference()
                            if spatial_ref.ImportFromWkt(esri_pe_string) != ogr.OGRERR_NONE:
                                self.logger.error(
                                    f"Migration not available for this projected files. (Input file projection : {inDataset.mbProj4String})"
                                )
                                continue

                    if spatial_ref.IsProjected():
                        if not nc_util.is_spatial_reference_supported(spatial_ref):
                            self.logger.error(f'Projection {spatial_ref.GetAttrValue("PROJECTION")} not supported.')
                            continue
                        self.logger.info(f'Projection : {spatial_ref.GetAttrValue("PROJECTION")}')

                    o_dtm_driver = dtm_driver.DtmDriver(self.o_paths[ind])

                    spatial_resolution_x = (
                        inDataset.variables[OldIfr.VARIABLE_COLUMN][1] - inDataset.variables[OldIfr.VARIABLE_COLUMN][0]
                    )
                    spatial_resolution_y = (
                        inDataset.variables[OldIfr.VARIABLE_LINE][1] - inDataset.variables[OldIfr.VARIABLE_LINE][0]
                    )
                    with o_dtm_driver.create_file(
                        inDataset.dimensions[OldIfr.DIM_COLUMNS].size,
                        inDataset.variables[OldIfr.VARIABLE_COLUMN][0] - 0.5 * spatial_resolution_x,
                        spatial_resolution_x,
                        inDataset.dimensions[OldIfr.DIM_LINE].size,
                        inDataset.variables[OldIfr.VARIABLE_LINE][0] - 0.5 * spatial_resolution_y,
                        spatial_resolution_y,
                        spatial_ref,
                        self.overwrite,
                    ) as outDataset:
                        self._migrateToDataset(inDataset, o_dtm_driver, sub_monitor)

                end_tmp = datetime.datetime.now()
                self.logger.info(
                    f"File {self.o_paths[ind]} migrated with success (time elapsed {end_tmp - start_tmp} )."
                )
            except ValueError as e:
                self.logger.error(str(e))
                files_in_error.append(path)
            except Exception:
                self.logger.error(f"Error while processing file {path}", exc_info=True)
                files_in_error.append(path)

            finally:
                sub_monitor.done()

        process_util.log_result(self.logger, start, files_in_error)
