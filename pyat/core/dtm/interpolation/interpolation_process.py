#! /usr/bin/env python3
# coding: utf-8

import os
import shutil
import tempfile
from typing import Callable, List, Optional

import pyat.core.dtm.dtm_driver as dtm_driver
import pyat.core.dtm.emodnet.cdi_layer_util as cdi_util
import pyat.core.dtm.emodnet.dtm_standard_constants as DtmConstants
import pyat.core.dtm.mask as mask_util
import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor


def interpolate_dtms(
    i_paths: List,
    o_paths: List,
    interpolation_algo: Callable[[str, str], None],
    overwrite: bool = False,
    areas: Optional[str] = None,
    monitor: ProgressMonitor = DefaultMonitor,
):
    """
    Launch the an interpolation of DTMs.
    for each input DTM file, call the interpolation_algo.
    """
    logger = log.logging.getLogger("Interpolation process")

    monitor.set_work_remaining(len(i_paths) * 2)

    with tempfile.TemporaryDirectory() as tmp_dir:
        for i_path, o_path in zip(i_paths, o_paths):
            try:
                logger.info(f"Processing file {i_path}")
                if os.path.exists(o_path) and not overwrite:
                    logger.error(
                        f"File already exists and overwrite not allowed (allow overwrite with option : '-o --overwrite)"
                    )
                    continue

                logger.info("Processing elevation interpolation")
                tmp_o_path = os.path.join(tmp_dir, os.path.basename(o_path))
                interpolation_algo(i_path, tmp_o_path)
                monitor.worked(1)

                if os.path.exists(tmp_o_path):
                    logger.info(f"Writing resulting file {os.path.basename(o_path)}")
                    shutil.copy(i_path, o_path)
                    with dtm_driver.open_dtm(tmp_o_path) as i_dtm, dtm_driver.open_dtm(o_path, mode="r+") as o_dtm:
                        # first create if not existing the interpolation layer
                        o_dtm.create_interpolation_layer()

                        # update
                        interpolated_elevations = i_dtm[DtmConstants.ELEVATION_NAME][:]

                        # masking value out of zone
                        geo_mask = mask_util.compute_geo_mask_from_dtm(tmp_o_path, [areas] if areas else [])
                        reset_cell_mask = geo_mask != 1
                        interpolated_elevations[reset_cell_mask] = dtm_driver.get_missing_value(
                            DtmConstants.ELEVATION_NAME
                        )
                        mask_of_new_elevations = o_dtm.update_elevation(interpolated_elevations)

                        # CDI
                        cdi_util.update_cdi(o_dtm, mask_of_new_elevations)
                else:
                    logger.error(f"Interpolation of {i_path} failed.")

                monitor.worked(1)
            except Exception:
                logger.error(f"Error while processing file {i_path}", exc_info=True)

    monitor.done()
