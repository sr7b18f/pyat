# pylint:disable=no-member

import shutil
from typing import List, Optional

import rioxarray  # Don't delete !
import xarray

import pyat.core.dtm.emodnet.dtm_standard_constants as DtmConstants
import pyat.core.utils.argument_utils as arg_util
from pyat.core.dtm import dtm_driver
from pyat.core.dtm.interpolation.interpolation_process import interpolate_dtms
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor


class InterpolateProcess:
    """
    Interpolate process
    """

    def __init__(
        self,
        i_paths: List[str],
        o_paths: List[str],
        overwrite: bool = False,
        masks: Optional[str] = None,
        monitor: ProgressMonitor = DefaultMonitor,
    ):
        """
        Constructor.
        """
        self.__call__implementation = interpolate_dtms(
            i_paths,
            o_paths,
            self._interpolates_one_dtm,
            overwrite,
            masks,
            monitor,
        )

    def __call__(self) -> None:
        """
        Simply call the interpolates method defined in the constructor
        """
        self.__call__implementation

    def _interpolates_one_dtm(self, i_path: str, o_path: str) -> None:
        """
        use of xarray and rioxarray to compute the missing elevations
        """
        shutil.copyfile(src=i_path, dst=o_path)
        with dtm_driver.open_dtm(o_path, mode="r+") as o_dtm_driver:
            crs_description = o_dtm_driver.dtm_file.spatial_reference.ExportToProj4()
            xdata_array = xarray.DataArray(o_dtm_driver[DtmConstants.ELEVATION_NAME][:], dims=["y", "x"])
            xdata_array.rio.write_crs(crs_description, inplace=True)
            xdata_array.rio.write_nodata(dtm_driver.get_missing_value(DtmConstants.ELEVATION_NAME), inplace=True)
            interpolated_elevations = xdata_array.rio.interpolate_na(method="linear").to_numpy()
            o_dtm_driver[DtmConstants.ELEVATION_NAME][:] = interpolated_elevations
