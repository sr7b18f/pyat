import errno
import logging
import os

import netCDF4 as nc
from osgeo import osr


def duplicate_variable(i_var: nc.Variable, o_dataset: nc.Dataset) -> None:
    """Create the same variable (same attributes and  data) in the output Dataset than the specified variable

    Arguments:
        i_var -- input variable.
        o_dataset -- output dataset.
    """
    o_var = o_dataset.createVariable(i_var.name, i_var.datatype, i_var.dimensions)
    o_var.setncatts(i_var.__dict__)
    o_var[:] = i_var[:]


def duplicate_dimensions(i_dataset: nc.Dataset, o_dataset: nc.Dataset) -> None:
    """Copy all dimensions based on the input file.

    Arguments:
        i_dataset -- input dataset.
        o_dataset -- output dataset.
    """
    for name, dimension in i_dataset.dimensions.items():
        o_dataset.createDimension(name, len(dimension) if not dimension.isunlimited() else None)


def open_read(path: str, nc_format="NETCDF4") -> nc.Dataset:
    """Open the nc file with path in a read mode.

    Arguments:
        path {str} -- Path of the file.
        nc_format -- underlying file format .

    Raises:
        SystemError: File is not in the expected format
        FileNotFoundError: Doesn't find the file.
    """
    logging.getLogger(__name__).info(f"Open file {path} in read mode.")
    return open_ncfile(path, "r", nc_format)


def create_file(path: str, nc_format="NETCDF4", overwrite=True) -> nc.Dataset:
    """Create the nc file with path.

    Arguments:
        path {str} -- Path of the file.
        nc_format -- underlying file format.
        overwrite -- True to overwrite the file if exists

    Raises:
        FileExistsError: file exists and overwrite not allowed
        SystemError: File is not in the expected format
    """
    logging.getLogger(__name__).info(f"Create file {path}.")
    if not os.path.exists(path) or overwrite:
        return open_ncfile(path, "w", nc_format)
    raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST), path)


def open_modify(path: str, nc_format="NETCDF4") -> nc.Dataset:
    """Open the nc file with path in a modify mode.

    Arguments:
        path {str} -- Path of the file.
        nc_format -- underlying file format .

    Raises:
        SystemError: File is not in the expected format
        FileNotFoundError: Doesn't find the file.
    """
    logging.getLogger(__name__).info(f"Open file {path} in modify mode.")
    return open_ncfile(path, "r+", nc_format)


def open_ncfile(path: str, nc_mode="r", nc_format="NETCDF4") -> nc.Dataset:
    """Open the nc file with path in a specified mode.

    Arguments:
        path {str} -- Path of the file.
        mode -- access mode ("r" means read-only; "w" new file is created; "r+" mean append
        nc_format -- underlying file format .

    Raises:
        SystemError: File is not in the expected format
        FileNotFoundError: Doesn't find the file.
    """
    try:
        result = nc.Dataset(path, mode=nc_mode)
        if result.file_format != nc_format:
            result.close()
            raise SystemError(f"The format of the file {path} must be {nc_format} (not {result.file_format}).")
        return result
    except FileNotFoundError as e:
        raise FileNotFoundError(f"Dtm file {path} not found.") from e


def set_history_attr(o_dataset: nc.Dataset, process_name: str, i_paths=None, append=True) -> None:
    """Appened the history global attribute. If no history attribute, create and fill it with
        - name of the process
        - file(s)
    If history attribute exists, just add the process after it.

    Arguments:
        o_dataset -- output dataset.
        process_name -- name of the python process.
        i_paths -- input files if any.
        append -- if false, hitory is replaced
    """

    if o_dataset.history and append:
        o_dataset.history = o_dataset.history + f", process with Python {process_name}"
    else:
        o_dataset.history = f"Process with Python {process_name}"

    if i_paths:
        if isinstance(i_paths, list):
            o_dataset.history = o_dataset.history + f' from {", ".join(i_paths)}.'
        else:
            o_dataset.history = o_dataset.history + f" from {i_paths}."


# __        ___  _______  ______  ____   ___      _ _  _     _                       _       _   _               _           ____ _____   _   __
# \ \      / / |/ /_   _|/ /  _ \|  _ \ / _ \    | | || |   | |_ _ __ __ _ _ __  ___| | __ _| |_(_) ___  _ __   | |_ ___    / ___|  ___| / | / /_
#  \ \ /\ / /| ' /  | | / /| |_) | |_) | | | |_  | | || |_  | __| '__/ _` | '_ \/ __| |/ _` | __| |/ _ \| '_ \  | __/ _ \  | |   | |_    | || '_ \
#   \ V  V / | . \  | |/ / |  __/|  _ <| |_| | |_| |__   _| | |_| | | (_| | | | \__ \ | (_| | |_| | (_) | | | | | || (_) | | |___|  _|   | || (_) |
#    \_/\_/  |_|\_\ |_/_/  |_|   |_| \_\\___/ \___(_) |_|    \__|_|  \__,_|_| |_|___/_|\__,_|\__|_|\___/|_| |_|  \__\___/   \____|_|     |_(_)___/
#
# See https://cfconventions.org/wkt-proj-4.html


def __translate_spatial_reference_epsg_9804(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Mercator EPSG 9804 SpatialReference to netcdf compliant attributes.
    Mercator Variant A is defined with the equator as the single standard parallel, with scale factor on the equator also defined.
    GDAL id = osr.SRS_PT_MERCATOR_1SP
    Proj4 = +proj=merc
    """
    return {
        "grid_mapping_name": "mercator",
        "longitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "scale_factor_at_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_SCALE_FACTOR),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_epsg_9805(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Mercator EPSG 9805 SpatialReference to netcdf compliant attributes
    Mercator Variant B is defined through the latitude of two parallels equidistant either side of the equator upon which the grid scale is true
    GDAL id = osr.SRS_PT_LAMBERT_CONFORMAL_CONIC_2SP
    Proj4 = +proj=merc
    """
    return {
        "grid_mapping_name": "mercator",
        "longitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "standard_parallel": spatial_ref.GetProjParm(osr.SRS_PP_STANDARD_PARALLEL_1),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_aea(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Albers Conical Equal Area SpatialReference to netcdf compliant attributes
    GDAL id = osr.SRS_PT_ALBERS_CONIC_EQUAL_AREA
    Proj4 = +proj=aea
    """
    return {
        "grid_mapping_name": "albers_conical_equal_area",
        "standard_parallel": [
            spatial_ref.GetProjParm(osr.SRS_PP_STANDARD_PARALLEL_1),
            spatial_ref.GetProjParm(osr.SRS_PP_STANDARD_PARALLEL_2),
        ],
        "longitude_of_central_meridian": spatial_ref.GetProjParm(osr.SRS_PP_LONGITUDE_OF_CENTER),
        "latitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_CENTER),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_aeqd(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Azimuthal Equidistant SpatialReference to netcdf compliant attributes
    GDAL id = osr.SRS_PT_AZIMUTHAL_EQUIDISTANT
    Proj4 = +proj=aeqd
    """
    return {
        "grid_mapping_name": "azimuthal_equidistant",
        "longitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LONGITUDE_OF_CENTER),
        "latitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_CENTER),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_laea(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Lambert Azimuthal Equal Area SpatialReference to netcdf compliant attributes
    GDAL id = osr.SRS_PT_LAMBERT_AZIMUTHAL_EQUAL_AREA
    Proj4 = +proj=laea
    """
    return {
        "grid_mapping_name": "lambert_azimuthal_equal_area",
        "longitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LONGITUDE_OF_CENTER),
        "latitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_CENTER),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_lcc_1sp(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Lambert conformal SpatialReference to netcdf compliant attributes
    GDAL id = osr.SRS_PT_LAMBERT_CONFORMAL_CONIC_1SP
    Proj4 = +proj=lcc
    """
    return {
        "grid_mapping_name": "lambert_conformal_conic",
        "standard_parallel": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_ORIGIN),
        "longitude_of_central_meridian": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "latitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_ORIGIN),
        # Scale factor at natural origin not in CF (always 1)
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_lcc_2sp(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Lambert conformal SpatialReference to netcdf compliant attributes
    GDAL id = osr.SRS_PT_LAMBERT_CONFORMAL_CONIC_2SP
    Proj4 = +proj=lcc
    """
    return {
        "grid_mapping_name": "lambert_conformal_conic",
        "standard_parallel": [
            spatial_ref.GetProjParm(osr.SRS_PP_STANDARD_PARALLEL_1),
            spatial_ref.GetProjParm(osr.SRS_PP_STANDARD_PARALLEL_2),
        ],
        "longitude_of_central_meridian": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "latitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_ORIGIN),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_cea(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Lambert cylindrical equal area to netcdf compliant attributes
    GDAL id = osr.SRS_PT_CYLINDRICAL_EQUAL_AREA
    Proj4 = +proj=cea
    """
    return {
        "grid_mapping_name": "lambert_cylindrical_equal_area",
        "standard_parallel": spatial_ref.GetProjParm(osr.SRS_PP_STANDARD_PARALLEL_1),
        "longitude_of_central_meridian": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_ortho(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Orthographic projection to netcdf compliant attributes
    GDAL id = osr.SRS_PT_ORTHOGRAPHIC
    Proj4 = +proj=ortho
    """
    return {
        "grid_mapping_name": "orthographic",
        "longitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "latitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_ORIGIN),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_stere(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Stereographic projection to netcdf compliant attributes
    GDAL id = osr.SRS_PT_STEREOGRAPHIC
    Proj4 = +proj=stere
    """
    return {
        "grid_mapping_name": "stereographic",
        "longitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "latitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_ORIGIN),
        "scale_factor_at_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_SCALE_FACTOR),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


def __translate_spatial_reference_polar_stere(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Polar stereographic projection to netcdf compliant attributes
    GDAL id = osr.SRS_PT_POLAR_STEREOGRAPHIC
    Proj4 = +proj=stere
    """
    latitude_of_origin = spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_ORIGIN)
    result = {
        "grid_mapping_name": "polar_stereographic",
        "latitude_of_projection_origin": 90.0 if latitude_of_origin >= 0.0 else -90.0,
        "straight_vertical_longitude_from_pole": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }

    if latitude_of_origin in [-90.0, 90.0]:
        # Polar stereographic PS-A
        result["scale_factor_at_projection_origin"] = spatial_ref.GetProjParm(osr.SRS_PP_SCALE_FACTOR)
    else:
        # Polar stereographic PS-B
        result["standard_parallel"] = latitude_of_origin

    return result


def __translate_spatial_reference_tmerc(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a Transverse Mercator projection to netcdf compliant attributes
    GDAL id = osr.SRS_PT_TRANSVERSE_MERCATOR
    Proj4 = +proj=tmerc
    """
    return {
        "grid_mapping_name": "transverse_mercator",
        "scale_factor_at_central_meridian": spatial_ref.GetProjParm(osr.SRS_PP_SCALE_FACTOR),
        "longitude_of_central_meridian": spatial_ref.GetProjParm(osr.SRS_PP_CENTRAL_MERIDIAN),
        "latitude_of_projection_origin": spatial_ref.GetProjParm(osr.SRS_PP_LATITUDE_OF_ORIGIN),
        "false_easting": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_EASTING),
        "false_northing": spatial_ref.GetProjParm(osr.SRS_PP_FALSE_NORTHING),
    }


SPATIAL_REFERENCE_TRANSLATORS = {
    osr.SRS_PT_MERCATOR_1SP: __translate_spatial_reference_epsg_9804,
    osr.SRS_PT_MERCATOR_2SP: __translate_spatial_reference_epsg_9805,
    osr.SRS_PT_ALBERS_CONIC_EQUAL_AREA: __translate_spatial_reference_aea,
    osr.SRS_PT_AZIMUTHAL_EQUIDISTANT: __translate_spatial_reference_aeqd,
    osr.SRS_PT_LAMBERT_AZIMUTHAL_EQUAL_AREA: __translate_spatial_reference_laea,
    osr.SRS_PT_LAMBERT_CONFORMAL_CONIC_1SP: __translate_spatial_reference_lcc_1sp,
    osr.SRS_PT_LAMBERT_CONFORMAL_CONIC_2SP: __translate_spatial_reference_lcc_2sp,
    osr.SRS_PT_CYLINDRICAL_EQUAL_AREA: __translate_spatial_reference_cea,
    osr.SRS_PT_ORTHOGRAPHIC: __translate_spatial_reference_ortho,
    osr.SRS_PT_STEREOGRAPHIC: __translate_spatial_reference_stere,
    osr.SRS_PT_POLAR_STEREOGRAPHIC: __translate_spatial_reference_polar_stere,
    osr.SRS_PT_TRANSVERSE_MERCATOR: __translate_spatial_reference_tmerc,
}


def is_projection_supported(projection: str) -> bool:
    """
    Return true if the specified projection is translatable
    """
    return projection in SPATIAL_REFERENCE_TRANSLATORS


def is_spatial_reference_supported(spatial_ref: osr.SpatialReference) -> bool:
    """
    Return true if the specified projection is translatable
    """
    return spatial_ref.GetAttrValue("PROJECTION") in SPATIAL_REFERENCE_TRANSLATORS


def translate_spatial_reference(spatial_ref: osr.SpatialReference) -> dict:
    """
    Convert a SpatialReference to netcdf compliant attributes
    """
    projection = spatial_ref.GetAttrValue("PROJECTION")
    if is_projection_supported(projection):
        return SPATIAL_REFERENCE_TRANSLATORS[projection](spatial_ref)
    # Projection not supported
    raise ValueError(f"Specified projection is not compliant with the Netcdf convention : {projection}")
