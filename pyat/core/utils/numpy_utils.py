import datetime
import math
import tempfile
from typing import Optional, Tuple, Union

import numba
import numpy as np
from osgeo import osr
from pyproj import Transformer, crs
import scipy.interpolate

import pyat.core.utils.argument_utils as arg_util


def interp1d_nan(values: np.ndarray):
    """Function to replace nan values by interpolated values in a 1D array"""
    indexes = np.arange(values.shape[0])
    valid, = np.where(np.isfinite(values))
    f = scipy.interpolate.interp1d(valid,
                                   values[valid],
                                   bounds_error=False,
                                   fill_value="extrapolate"
                                   )
    return f(indexes)


@numba.guvectorize(["void(float64[:,:], float64[:,:])"], "(r, c),(m, c2)", target="parallel", nopython=True, cache=True)
def minMaxOnFloat(inArray: np.ndarray, result: np.ndarray):
    """
    Return the minimum and the maximum values of an array along the column axis
    :param inArray : the input array to process
    :param result : the resulting array containing the minimum and the maximum for each column
    """
    for colIndex in range(result.shape[1]):
        for rowIndex in range(inArray.shape[0]):
            colValue = float(inArray[rowIndex, colIndex])
            if not np.isnan(colValue):
                # Can't use min() or np.fmin() because they raise a RuntimeWarning (invalid value encountered) ?
                result[0, colIndex] = (
                    colValue if np.isnan(result[0, colIndex]) else np.minimum(result[0, colIndex], colValue)
                )
                result[1, colIndex] = (
                    colValue if np.isnan(result[1, colIndex]) else np.maximum(result[1, colIndex], colValue)
                )


@numba.jit(nopython=True, parallel=True, cache=True)
def aggregate(
    inValues: np.ndarray, xIndex: int, yIndex: int, valueIndex: int, outArray: np.ndarray, factor: float = 1.0
):
    """
    Return the minimum and the maximum values of an array along the column axis
    :param inValues : values to aggregate.
    :param xIndex : column index in inValues, containing the column index of the destination cell
    :param yIndex : column index in inValues, containing the row index of the destination cell
    :param valueIndex : column index in inValues, containing the float values to apply to the destination cell
    :param outArray : the receiving array where values are aggregated
    :param factor : factor applied to values
    """
    for rowIndex in numba.prange(inValues.shape[0]):
        if not np.isnan(inValues[rowIndex, 0]) and not np.isnan(inValues[rowIndex, 1]):
            x = int(inValues[rowIndex, xIndex])
            y = int(inValues[rowIndex, yIndex])
            if not np.isnan(x) and not np.isnan(y) and not np.isnan(inValues[rowIndex, valueIndex]):
                outArray[y, x] = inValues[rowIndex, valueIndex] * factor


@numba.njit(cache=True, fastmath=False)
def compute_statistics(
    in_array: np.ndarray,
    x_array: np.ndarray,
    y_array: np.ndarray,
    out_last_array: Optional[np.ndarray] = None,
    out_min_array: Optional[np.ndarray] = None,
    out_mean_array: Optional[np.ndarray] = None,
    out_max_array: Optional[np.ndarray] = None,
    out_count_array: Optional[np.ndarray] = None,
    out_sum_array: Optional[np.ndarray] = None,
    out_filtered_array: Optional[np.ndarray] = None,
) -> None:
    """
    Project the values contained in in_array to the out_*_array
    Indexes (column and row) used to determine the cell in out_*_array are found in x_array and y_array

    :param in_array : float values to aggregate.
    :param x_array : column index in in_array, containing the column index of the destination cell
    :param y_array : row index in in_array, containing the row index of the destination cell
    :param filtering_array : validity array indicating witch values in in_array have to be processed
    :param out_last_array : the receiving array containing all last projected values per cell
    :param out_min_array : the receiving array containing all min values per cell
    :param out_mean_array : the receiving array containing all mean values per cell
    :param out_max_array : the receiving array containing all max values per cell
    :param out_count_array : the receiving array containing the number of values per cell
    :param out_filtered_array : the receiving array containing the number of values equals to nan
    :param out_sum_array : the receiving array containing all sum values per cell
    """
    # Check out array
    shape = (0, 0)
    if out_last_array is not None:
        shape = out_last_array.shape
    if out_min_array is not None:
        shape = out_min_array.shape
    if out_mean_array is not None:
        shape = out_mean_array.shape
    if out_max_array is not None:
        shape = out_max_array.shape
    if out_count_array is not None:
        shape = out_count_array.shape
    if out_sum_array is not None:
        shape = out_sum_array.shape
    if out_filtered_array is not None:
        shape = out_filtered_array.shape
    if shape == (0, 0):
        raise ValueError("At least one out array must be specified")
    if out_mean_array is not None and out_count_array is None:
        raise ValueError("out_count_array is mandatory to compute the mean values")

    for value, col, row in zip(in_array.flat, x_array.flat, y_array.flat):
        # Sanity check
        if 0 <= row < shape[0] and 0 <= col < shape[1]:
            if not np.isnan(value):
                # Last value
                if out_last_array is not None:
                    out_last_array[row, col] = value
                # Value mean
                if out_mean_array is not None and out_count_array is not None:
                    if np.isnan(out_mean_array[row, col]):
                        out_mean_array[row, col] = value
                    else:
                        out_mean_array[row, col] = ((out_mean_array[row, col] * out_count_array[row, col]) + value) / (
                            out_count_array[row, col] + 1
                        )
                # Value count
                if out_count_array is not None:
                    if out_count_array[row, col] <= 0:
                        out_count_array[row, col] = 1
                    else:
                        out_count_array[row, col] = out_count_array[row, col] + 1
                # Value min
                if out_min_array is not None:
                    if np.isnan(out_min_array[row, col]):
                        out_min_array[row, col] = value
                    else:
                        out_min_array[row, col] = min(out_min_array[row, col], value)
                # Value max
                if out_max_array is not None:
                    if np.isnan(out_max_array[row, col]):
                        out_max_array[row, col] = value
                    else:
                        out_max_array[row, col] = max(out_max_array[row, col], value)
                # Value sum
                if out_sum_array is not None:
                    if np.isnan(out_sum_array[row, col]):
                        out_sum_array[row, col] = value
                    else:
                        out_sum_array[row, col] = out_sum_array[row, col] + value

            elif out_filtered_array is not None:
                if out_filtered_array[row, col] <= 0:
                    out_filtered_array[row, col] = 1
                else:
                    out_filtered_array[row, col] = out_filtered_array[row, col] + 1


@numba.njit(cache=True, fastmath=False)
def compute_standard_deviation_first_pass(
    in_values: np.ndarray,
    x_array: np.ndarray,
    y_array: np.ndarray,
    out_tmp_array: np.ndarray,
):
    """
    Project the values of the in_values to the out_stddev_array and prepare the computing of the standard deviation for each cell
    Indexes (column and row) used to determine the cell in out_stddev_array are found in x_array and y_array

    :param in_values : float values to project.
    :param x_array : column index in in_array, containing the column index of the destination cell
    :param y_array : row index in in_array, containing the row index of the destination cell
    :param out_tmp_array : the receiving array
    """
    out_shape = out_tmp_array.shape

    for in_value, col, row in zip(in_values.flat, x_array.flat, y_array.flat):
        if not np.isnan(in_value):
            if 0 <= row < out_shape[0] and 0 <= col < out_shape[1]:
                if np.isnan(out_tmp_array[row, col]):
                    out_tmp_array[row, col] = in_value ** 2
                else:
                    out_tmp_array[row, col] += in_value ** 2


def compute_standard_deviation_second_pass(
    in_count_array: np.ndarray,
    in_mean_array: np.ndarray,
    out_stddev_array: np.ndarray,
):
    """
    Finalize the computing of the standard deviation for each cell
    Indexes (column and row) used to determine the cell in out_stddev_array are found in x_array and y_array

    :param in_count_array : number of values
    :param in_mean_array : mean values
    :param out_stddev_array : the receiving array containing all sum of squared values
    """
    out_stddev_array[in_count_array <= 1] = np.nan
    squared_mean = np.square(in_mean_array)  # mean²
    np.divide(out_stddev_array, in_count_array, out=out_stddev_array)  # sum(value²)/n
    np.subtract(out_stddev_array, squared_mean, out=out_stddev_array)  # sum(value²)/n - mean²
    np.sqrt(out_stddev_array, out=out_stddev_array)  # sqrt(sum(value²)/n - mean²)


# pylint: disable=too-many-boolean-expressions, consider-using-with
# In fastmath mode, np.isnan does not work !
@numba.njit(cache=True, fastmath=False)
def project_into_grid_keep_last(
    in_array: np.ndarray,
    x_array: np.ndarray,
    y_array: np.ndarray,
    out_array: np.ndarray,
    missing_value: Union[float, int, None] = None,
    factor: float = 1.0,
) -> None:
    """
    Project the values contained in in_array to the out_array.
    Only the last value is kept in the cell
    Indexes (column and row) used to determine the cell in out_array are found in x_array and y_array

    :param in_array : float values to aggregate.
    :param x_array : column index in in_array, containing the column index of the destination cell
    :param y_array : row index in in_array, containing the row index of the destination cell
    :param out_array : the receiving array containing the last value per cell
    :param factor : factor applied to values
    """
    out_shape = out_array.shape
    for in_value, col, row in zip(in_array.flat, x_array.flat, y_array.flat):
        # Sanity check
        if not (0 <= row < out_shape[0] and 0 <= col < out_shape[1]):
            continue

        # Really a value ?
        if np.isnan(in_value):
            continue

        # Any missing value to check ?
        if missing_value is None or np.isnan(missing_value):
            out_array[row, col] = in_value * factor
            continue

        # Check if missing value
        value = in_value * factor
        if value != missing_value:
            out_array[row, col] = value


def to_memmap(in_array: np.ndarray) -> np.ndarray:
    """
    Utility method to create and initialize memory-map to an array stored in a binary file on disk .
    """
    map_file = tempfile.TemporaryFile(suffix=".memmap")
    result = np.memmap(map_file, shape=in_array.shape, dtype=in_array.dtype, mode="w+")
    # Write data to memmap array
    result[:] = in_array[:]

    return result


def to_utc(in_datetime: datetime.datetime) -> np.datetime64:
    """Convert a naive or aware datetime to np datetime64 in UTC timezone
    @:param in_datetime: the naive/aware datetime to convert
    @:return datetime64 object
    """
    return np.datetime64(datetime.datetime.utcfromtimestamp(in_datetime.timestamp()))

def project_coords(
    xs: np.ndarray,
    ys: np.ndarray,
    geobox: arg_util.Geobox,
    spatial_resolution: float,
    spatial_reference: osr.SpatialReference = None,
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Project all coordinates to the grid to obtain the cell position.
    xs are the longitudes when spatial_reference is not projected (LonLat). Otherwise, xs are abscissas
    xy are the latitudes when spatial_reference is not projected (LonLat). Otherwise, xs are ordinates
    geobox represents the coordinates of the centers of the cells in the corners
    spatial_reference is the SRS of the xs and ys. None means that the CRS is the same than GeoBox and no transformation is required
    returns (columns, rows) calculated by the projection
    """
    # Compare xy and geobox CRS. If not same, transformation is required
    if spatial_reference is not None:
        xy_crs = crs.CRS.from_proj4(spatial_reference.ExportToProj4())
        geobox_crs = crs.CRS.from_proj4(geobox.spatial_reference.ExportToProj4())
        if not xy_crs.is_exact_same(geobox_crs):
            # Transform input coordinates to the target CRS
            xs, ys = Transformer.from_crs(
                crs.CRS.from_epsg(4326),
                crs.CRS.from_proj4(geobox.spatial_reference.ExportToProj4()),
                always_xy=True,
            ).transform(xs, ys, radians=False)

    # Project coords of target CRS to the cell position in the DTM grid
    columns = (
        __project_longitude_to_grid(xs, geobox.left, geobox.right, spatial_resolution)
        if geobox.spatial_reference.IsGeographic()
        else __project_value_to_axis(xs, geobox.left, spatial_resolution)
    )
    rows = __project_value_to_axis(ys, geobox.lower, spatial_resolution)

    return (columns, rows)


@numba.vectorize([numba.int32(numba.float64, numba.float64, numba.float64)])
def __project_value_to_axis(value: np.array, axis_origin: float, cell_size: float) -> int:
    return math.floor((value - axis_origin) / cell_size)


@numba.vectorize([numba.int32(numba.float64, numba.float64, numba.float64, numba.float64)])
def __project_longitude_to_grid(long: np.array, west: float, east: float, spatial_resolution: float) -> int:
    # Check if longitude span the 180th meridian
    if west > 0 > east and long < west:
        long += 360.0
    return math.floor((long - west) / spatial_resolution)
