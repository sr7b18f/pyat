import math

import numpy as np


def get_roll_matrix(roll: float) -> np.ndarray:
    """
    Return the roll transformation matrix
    """
    cos_roll = math.cos(roll)
    sin_roll = math.sin(roll)
    return np.array([[1, 0, 0], [0, cos_roll, -sin_roll], [0, sin_roll, cos_roll]])


def get_pitch_matrix(pitch: float) -> np.ndarray:
    """
    Return the pitch transformation matrix
    """
    cos_pitch = math.cos(pitch)
    sin_pitch = math.sin(pitch)
    return np.array([[cos_pitch, 0, sin_pitch], [0, 1, 0], [-sin_pitch, 0, cos_pitch]])


def get_vcs_attitude(pitch: float, roll: float) -> np.ndarray:
    """
    Return the attitude matrix in Vessel Coordinate System
    """
    return np.matmul(get_pitch_matrix(pitch), get_roll_matrix(roll))


def transform_vcs_to_scs(pitch: float, roll: float, x_vcs: np.ndarray) -> np.ndarray:
    """
    Transform coordinates from the Vessel Coordinate System to Surface Coordinate System
    """
    return np.matmul(get_vcs_attitude(pitch, roll), x_vcs)


if __name__ == "__main__":
    print(transform_vcs_to_scs(0.63, 1.61, [4.223, 0.01, 1.735]))
