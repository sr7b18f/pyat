#! /usr/bin/env python3
# coding: utf-8

import pyat.core.utils.pyat_logger as log


class ProgressMonitor:
    """
    Same as NullProgressMonitor in Java
    """

    def __init__(self):
        self.acc = 0
        self.desc = ""
        self.size = 0
        self.father = None
        self.father_ticks = 0
        self.last_log_percent = 0
        self.logger = log.logging.getLogger(ProgressMonitor.__name__)

    def _print_level(self):
        if self.father is not None:
            return "#" + self.father._print_level()
        return "#"

    def begin_task(self, name, n):
        self.desc = name
        self.size = n
        self.logger.info(f"{self._print_level()}Task {name} starting")

    def set_work_remaining(self, n):
        self.size = n
        self.acc = 0

    def done(self):
        self.logger.info(f"{self._print_level()}Task {self.desc} done 100%")
        if self.father is not None:
            self.father.worked(self.father_ticks)

    def check_cancelled(self):
        pass

    def set_cancelled(self):
        pass

    def split(self, ticks):  # pylint: disable=unused-argument
        sm = ProgressMonitor()
        sm.father = self
        sm.father_ticks = ticks
        sm.size = ticks
        return sm

    def worked(self, doneTicks):
        if self.size > 0:
            self.acc = self.acc + doneTicks
            percent = 100.0 * self.acc / self.size
            # log progress every 10%
            if percent - self.last_log_percent > 10:
                self.logger.info(f"{self._print_level()}Task {self.desc} {int(percent)}% done")
                self.last_log_percent = percent
        else:
            self.logger.warning(f"progress bar is not well initialized")


DefaultMonitor = ProgressMonitor()


class JavaMonitor(ProgressMonitor):
    """Class used to report task progress to java"""

    def __init__(self, javaMonitorObject):
        super().__init__()
        self.javaMonitorObject = javaMonitorObject

    def begin_task(self, name, n):
        super().begin_task(name, n)

        self.javaMonitorObject.beginTask(name, n)

    def set_work_remaining(self, n):
        super().set_work_remaining(n)
        self.javaMonitorObject.setWorkRemaining(n)

    def done(self):
        super().done()
        self.javaMonitorObject.done()

    def check_cancelled(self):
        return self.javaMonitorObject.isCanceled()

    def set_cancelled(self):
        self.javaMonitorObject.setCancelled()

    def split(self, ticks):
        """Returns a new monitor for a new subtask"""
        # self.logger.info(f"Create new subtask of size {ticks}")

        javaSubMonitor = self.javaMonitorObject.split(ticks)
        if javaSubMonitor:
            return JavaMonitor(javaSubMonitor)
        else:
            return DefaultMonitor

    def worked(self, doneTicks: int):
        """Reports progress of task
        doneTicks can be float
        """
        super().worked(doneTicks)
        self.javaMonitorObject.worked(doneTicks)
