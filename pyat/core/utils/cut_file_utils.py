#! /usr/bin/env python3
# coding: utf-8

import datetime as dt
from logging import Logger
import re
from typing import List

import pytz

from pyat.core.xsf.struct.sonar_netcdf.utils import nc_merger as nc_m


def parse_cut_file(cut_file_path: str, logger: Logger) -> List[nc_m.Timeline]:
    result: List[nc_m.Timeline] = []
    cut_regex = re.compile(
        r".*(\d{2}/\d{2}/\d{4}\s*\d{2}:\d{2}:\d{2}\.\d{3})\s*(\d{2}/\d{2}/\d{4}\s*\d{2}:\d{2}:\d{2}\.\d{3})\s*(.*)"
    )
    cut_matches = []

    try:
        with open(cut_file_path, "r", encoding="utf8") as cut_file:
            cut_lines = cut_file.read()
            cut_matches = cut_regex.findall(cut_lines)

        for cut_match in cut_matches:
            if len(cut_match) == 3:
                date_start = pytz.utc.localize(dt.datetime.strptime(cut_match[0], r"%d/%m/%Y %H:%M:%S.%f"))
                date_stop = pytz.utc.localize(dt.datetime.strptime(cut_match[1], r"%d/%m/%Y %H:%M:%S.%f"))
                result.append(nc_m.Timeline(cut_match[2], date_start, date_stop))
    except OSError as e:
        logger.error(f"Unparsable cut file : {str(e)}")

    logger.info(f"Number of cut lines found : {len(result)}")
    return result
