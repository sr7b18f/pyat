#! /usr/bin/env python3
# coding: utf-8

import datetime
import os
import shutil
from typing import Callable, Dict, List

import numpy as np

import pyat.core.dtm.utils.process_utils as process_util
import pyat.core.utils.argument_utils as arg_util
import pyat.core.utils.pyat_logger as log
import pyat.core.xsf.xsf_driver as xsf_driver
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor
from pyat.core.xsf.migration.history_updater import HistoryUpdater

# List of all XSF variable to transfert from reference file to target file
VARS_TO_TRANSFERT = [
    xsf_driver.STATUS,
    xsf_driver.STATUS_DETAIL,
    xsf_driver.DELTA_DRAUGHT,
    xsf_driver.TIDE_INDICATIVE,
    xsf_driver.PLATFORM_VERTICAL_OFFSET,
    xsf_driver.WATERLINE_TO_CHART_DATUM,
    xsf_driver.TX_TRANSDUCER_DEPTH,
    xsf_driver.PLATFORM_HEADING,
    xsf_driver.DETECTION_X,
    xsf_driver.DETECTION_Y,
    xsf_driver.DETECTION_Z,
    xsf_driver.DETECTION_LONGITUDE,
    xsf_driver.DETECTION_LATITUDE,
]

MINIMUM_XSF_VERSION = 0.2


class XsfUpdater:
    """
    Callable used by pyat/app to launch an upgrade of XSF files.
    This class aim to report validity flags and bias corrections on a set of xsf files.

    Update depends on version on reference and input files.
    When a new version of XSF is available, the following dicts can be completed to bring the possible conversions
     - __check_variables_funcs : which variables to update
     - __update_history_funcs : how to update history
     - __transfert_variables : how to transfert the variables
    """

    def __call__(self) -> None:
        """Run method."""
        self.monitor.set_work_remaining(len(self.i_paths))
        begin = datetime.datetime.now()
        file_in_error = []

        for ref_xsf, o_xsf in zip(self.i_paths, self.o_paths):
            try:
                self.logger.info(f"Starting to migrate {ref_xsf} to {o_xsf}")
                if not self.overwrite and os.path.exists(o_xsf):
                    self.logger.warning("File exists and overwrite is not allowed. Migration aborted.")
                    file_in_error.append(ref_xsf)
                    continue

                # Search reference file
                i_ref_xsf = os.path.join(self.i_ref, os.path.basename(ref_xsf))
                if not os.path.exists(i_ref_xsf) or os.path.samefile(ref_xsf, i_ref_xsf):
                    self.logger.warning("Bad reference XSF file. Migration aborted.")
                    file_in_error.append(ref_xsf)
                    continue

                # input file == output file => this is an update
                updating_i_xsf = os.path.exists(o_xsf) and os.path.samefile(ref_xsf, o_xsf)
                if not updating_i_xsf:
                    # Copy input xsf to o_xsf before migrating
                    shutil.copy(ref_xsf, o_xsf)

                now = datetime.datetime.now()
                with xsf_driver.open_xsf(i_ref_xsf) as i_xsf_driver, xsf_driver.open_xsf(o_xsf, "r+") as o_xsf_driver:
                    ref_version = self.__check_version(i_xsf_driver, o_xsf_driver)

                    # Get the variables to transfert
                    check_variables_func = self._get_suitable_function(ref_version, self.__check_variables_funcs)
                    self.logger.info(f"Checking variables with {str(check_variables_func)}()")
                    vars_to_transfert = check_variables_func(i_xsf_driver, o_xsf_driver)

                    # Update history.
                    update_history_func = self._get_suitable_function(ref_version, self.__update_history_funcs)
                    self.logger.info(f"Process history with {update_history_func.__name__}()")
                    history = f"{str(datetime.datetime.now())}  Process with Python {self.__class__.__name__}"
                    update_history_func(i_xsf_driver, o_xsf_driver, history)

                    # Process transfert
                    transfert_variables_func = self._get_suitable_function(
                        ref_version, self.__transfert_variables_funcs
                    )
                    self.logger.info(f"Transfering variables with {transfert_variables_func.__name__}()")
                    transfert_variables_func(i_xsf_driver, o_xsf_driver, vars_to_transfert)

                self.monitor.worked(1)
                self.logger.info(f"End of migration of {ref_xsf} : {datetime.datetime.now() - now} time elapsed\n")

            except ValueError as e:
                file_in_error.append(ref_xsf)
                self.logger.error(str(e))
            except Exception as e:
                file_in_error.append(ref_xsf)
                self.logger.error(f"An exception was thrown : {str(e)}", exc_info=True, stack_info=True)

        self.monitor.done()
        process_util.log_result(self.logger, begin, file_in_error)

    def __check_version(self, ref_xsf: xsf_driver.XsfDriver, o_xsf: xsf_driver.XsfDriver) -> float:
        """
        Check version of XSF files
        Return a float value representing the version of the reference file
        """
        try:
            ref_version = float(ref_xsf.dataset.xsf_convention_version)
        except ValueError as e:
            raise ValueError(f"Unsupported XSF version for the reference file") from e
        try:
            o_version = float(o_xsf.dataset.xsf_convention_version)
        except ValueError as e:
            raise ValueError(f"Unsupported XSF version for the input file") from e

        if ref_version < MINIMUM_XSF_VERSION:
            raise ValueError(f"The version must be at least {MINIMUM_XSF_VERSION} for the reference file")
        if o_version < MINIMUM_XSF_VERSION:
            raise ValueError(f"The version must be at least {MINIMUM_XSF_VERSION} for file to update")

        # Can't migrate data from a newer version of file
        if o_version < ref_version:
            raise ValueError(f"Downgrading XSF is not intended")

        self.logger.info(f"Processing update from XSF version {ref_version} to {o_version}")

        return ref_version

    def __check_variables(self, ref_xsf: xsf_driver.XsfDriver, o_xsf: xsf_driver.XsfDriver) -> List[str]:
        """
        Check the dimensions variable to transfert. Must be the same of both files
        Return the list of correct variables to transfert
        """
        self.logger.info(f"Checking variables...")

        result = []
        for var_to_check in VARS_TO_TRANSFERT:
            self.logger.debug(f"Process variable {var_to_check}")
            i_var = ref_xsf.get_layer(var_to_check)
            if i_var is None:
                self.logger.warning(f"Variable {var_to_check} skipped : not present in the reference file")
                continue
            o_var = o_xsf.get_layer(var_to_check)
            if o_var is None:
                self.logger.warning(f"Variable {var_to_check} skipped : not present in the target file")
                continue

            # Check dimensions
            i_dims = {dim.name: dim.size for dim in i_var.get_dims()}
            o_dims = {dim.name: dim.size for dim in o_var.get_dims()}
            if i_dims != o_dims:
                self.logger.warning(f"Variable {var_to_check} skipped : not the same dimension definition")
                continue

            result.append(var_to_check)

        if not result:
            raise ValueError("All layers were rejected by the control process : tranfert aborted")

        return result

    def __transfert_variables(
        self, ref_xsf: xsf_driver.XsfDriver, o_xsf: xsf_driver.XsfDriver, vars_to_transfert: List[str]
    ) -> None:
        """
        Transfert variables from reference file to output files.
        No transformation is applyed on values
        """
        self.logger.info("Transfering variables...")
        for var_to_transfert in vars_to_transfert:
            self.logger.debug(f"Process variable {var_to_transfert}")
            i_var = ref_xsf.get_layer(var_to_transfert)
            o_var = o_xsf.get_layer(var_to_transfert)
            o_var[:] = i_var[:]
            self.logger.debug(f"{var_to_transfert} transfered")

    def _get_suitable_function(self, version: int, functions: Dict[float, Callable]):
        """
        Method responsible for finding the suitable function to run for the given version number
        """
        if len(functions) == 1:
            # Only one function : this is the default one
            return next(iter(functions.values()))

        versions = np.array(list(functions.keys()))
        greater_or_equal_version = np.max(np.ma.masked_greater(versions, version))
        if np.ma.is_masked(greater_or_equal_version):
            raise ValueError("No suitable updating function found : tranfert aborted")

        return functions[greater_or_equal_version]

    def __init__(
        self,
        i_paths: List[str],
        o_paths: List[str],
        i_ref: str,
        overwrite: bool = False,
        monitor: ProgressMonitor = DefaultMonitor,
    ):
        """
        Constructor
        """
        self.logger = log.logging.getLogger(self.__class__.__name__)
        self.monitor = monitor

        # Parsing parameters
        self.i_paths = arg_util.parse_list_of_files("i_paths", i_paths, True)
        self.overwrite = overwrite
        self.o_paths = arg_util.parse_list_of_files("o_paths", o_paths, False)
        self.i_ref = i_ref
        if not os.path.exists(i_ref):
            raise ValueError(f"Invalid value for argument {i_ref} : folder does not exist")
        if not os.path.isdir(i_ref):
            raise ValueError(f"Invalid value for argument {i_ref} : not a folder")

        # Map all checking variable functions. Designates the function to call from a starting version number
        self.__check_variables_funcs = {
            MINIMUM_XSF_VERSION: self.__check_variables,  # Default behavior
        }
        # Map all functions to transfert variables. Designates the function to call from a starting version number
        self.__transfert_variables_funcs = {
            MINIMUM_XSF_VERSION: self.__transfert_variables,  # Default behavior
        }
        # Map all functions to update history metadata. Designates the function to call from a starting version number
        historyUpdater = HistoryUpdater()
        self.__update_history_funcs = {
            0.2: historyUpdater.update_history_from_0_2,  # Manage history from version 0.2
            0.3: historyUpdater.update_history_from_0_3,  # Manage history from version 0.3
        }
