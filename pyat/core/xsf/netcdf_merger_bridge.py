#! /usr/bin/env python3
# coding: utf-8

import datetime as dt
from typing import List, Optional

import pyat.core.dtm.utils.process_utils as process_util
import pyat.core.utils.argument_utils as arg_util
import pyat.core.utils.cut_file_utils as cut_util
import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor
from pyat.core.xsf.struct.sonar_netcdf.utils import nc_merger as nc_m


class NcMergerBridge:
    """
    Callable used by pyat/app to launch a Cut/Merge process on Netcdf files.
    This is a bridge to sonar_netcdf generic merger class (NCMerger).
    This class aim to adapt arguments comming from Globe and pass them to NCMerger.

    In specific cases, a sub-type of NCMerger can be provided with nc_merger_class argument.
    For XSF or any other sonar file it is recommended to specify SNMerger of pyat.core.xsf.struct.sonar_netcdf instead.
    """

    def __init__(
        self,
        i_paths: List[str],
        o_paths: List[str],
        cut_file: Optional[str] = None,
        start_date: Optional[dt.datetime] = None,
        end_date: Optional[dt.datetime] = None,
        overwrite: bool = False,
        monitor: ProgressMonitor = DefaultMonitor,
        nc_merger_class=nc_m.NCMerger,
    ):
        """
        Constructor
        """
        self.logger = log.logging.getLogger(self.__class__.__name__)
        self.monitor = monitor

        # Parsing parameters
        i_paths = arg_util.parse_list_of_files("i_paths", i_paths)
        o_paths = arg_util.parse_list_of_files("o_paths", o_paths, False)
        start_date = arg_util.parse_datetime(start_date)
        end_date = arg_util.parse_datetime(end_date)

        # Computing timelines
        timelines: List[nc_m.Timeline] = []
        if cut_file is not None:
            self.logger.info("Using cut_file argument to determine the cutting time intervals")
            timelines = cut_util.parse_cut_file(cut_file, self.logger)
            if len(timelines) == 0:
                self.logger.error(f"No cut line found in cut file. Merge abort")
                return
        elif start_date is not None and end_date is not None:
            self.logger.info("Using start_date and end_date arguments to determine the cutting time intervals")
            timelines.append(nc_m.Timeline("Single", start_date, end_date))
        else:
            self.logger.info("No cutting time interval specified. Merging files...")

        self.merger = nc_merger_class(i_paths, o_paths, timelines, overwrite)

    def __call__(self) -> None:
        """Run method."""
        begin = dt.datetime.now()
        self.merger.merge()
        process_util.log_result(self.logger, begin, [])
