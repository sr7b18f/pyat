#! /usr/bin/env python3
# coding: utf-8

from typing import List

import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as sg
from pyat.core.xsf.xsf_driver import XsfDriver


class HistoryUpdater:
    """Class managing the transfert of history"""

    def update_history_from_0_2(self, ref_xsf: XsfDriver, o_xsf: XsfDriver, history_info: str) -> None:
        """
        Transfert history attribute from a reference XSF version 0.2 to the last version
        Since version 0.3, history is stored in attribute. Previously, was a variable
        """
        # Starting history with migration informations
        history = [history_info]
        self.__append__history_0_2(ref_xsf, history)
        self.__insert_history_0_3(o_xsf, history)

    def update_history_from_0_3(self, ref_xsf: XsfDriver, o_xsf: XsfDriver, history_info: str) -> None:
        """
        Transfert history attribute for XSF version 0.3
        Since version 0.3, history is stored in attribute
        """
        # Starting history with migration informations
        history = [history_info]
        if "Provenance" in ref_xsf.dataset.groups:
            ref_provenanceGrp = ref_xsf.dataset.groups["Provenance"]
            if ref_provenanceGrp.history:
                for hist_line in ref_provenanceGrp.history:
                    history.append(f" From migration : {hist_line}")
        self.__insert_history_0_3(o_xsf, history)

    def __insert_history_0_3(self, o_xsf: XsfDriver, history: List[str]) -> None:
        """Insert specified history to Provenance.history attribute"""
        provenanceGrpStub = sg.ProvenanceGrp()
        o_provenanceGrp = (
            provenanceGrpStub.create_group(o_xsf.dataset)
            if "Provenance" not in o_xsf.dataset.groups
            else o_xsf.dataset.groups["Provenance"]
        )
        if o_provenanceGrp.history:
            history.append(o_provenanceGrp.history)

        o_provenanceGrp.history = history

    def __append__history_0_2(self, xsf: XsfDriver, history: List[str]) -> None:
        """Append history of a history variable to the specified list"""
        if "history" in xsf.dataset.variables:
            for hist_line in list(xsf.dataset["history"][:]):
                history.append(f" From migration : {hist_line}")
