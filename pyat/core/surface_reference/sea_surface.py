"""
Allow to change sea surface reference
"""
import os
from datetime import date

import numpy as np
import scipy.interpolate as interpol
from osgeo import gdal

from pyat.core.utils import gdal_utils
from pyat.core.utils.exceptions.exception_list import BadParameter
from pyat.core.utils.exceptions.exception_list import BadAssumption
from pyat.core.dtm import dtm_driver
import pyat.core.dtm.emodnet.dtm_standard_constants as DtmConstants


class SeaSurfaceModel:
    lat_variable = "lat_elevation"
    msl_variable = "msl_elevation"

    @staticmethod
    def validate_model(filename):
        if not os.path.exists(filename):
            raise BadParameter(f"LAT model data file does not exist, expected {filename}")
        dataset = gdal.Open(f"NETCDF:{filename}:{SeaSurfaceModel.msl_variable}")
        if dataset is None:
            raise BadAssumption(
                f"Data file {filename} is missing variable {SeaSurfaceModel.msl_variable},\
            an update of the sea surface file is probably needed"
            )

    @staticmethod
    def create_final_world_model(filename, lat_tiff_file, msl_tiff_file):
        """Create the final world model containing two layers
            a msl layer containing the mean sea surface referenced on the T/P ellipsoid)
            a lat layer containing the lowest astronomical tide surface referenced to the mean sea level

            Nota bene used by create tide project: https://gitlab.ifremer.fr/fleet/acoustic/lat_tide
        Args:
            filename: the output filename
            lat_values: an array of lat_values over
            msl_value:


        """
        step = 1 / 16
        demi_step = step / 2
        longitudes = np.arange(
            -180.0,  # Start
            180,  # Stop
            step,  # Step
        )
        longitudes += demi_step
        latitudes = np.arange(
            -90,  # Start
            90,  # Stop
            step,  # Step
        )
        latitudes += demi_step
        print(f"Create world model file {filename}")
        # create dtm from values, this will not change anything except ensure that convention are well filled
        o_dtm_driver = dtm_driver.DtmDriver(filename)
        spatial_resolution_y = step
        spatial_resolution_x = step
        with o_dtm_driver.create_file(
            col_count=len(longitudes),
            origin_x=longitudes[0] - 0.5 * spatial_resolution_x,
            spatial_resolution_x=spatial_resolution_x,
            row_count=len(latitudes),
            origin_y=latitudes[0] - 0.5 * spatial_resolution_y,
            spatial_resolution_y=spatial_resolution_y,
            overwrite=True,
            metadata={
                "title": "LAT MSL surface reference",
                "institution": "Ifremer",
                "source": "FES2014 prediction 2003-2021",
                "references": "FES2014 was produced by Noveltis, Legos and CLS and "
                           "distributed by Aviso+, with support from Cnes (https://www.aviso.altimetry.fr/), "
                          "MSS_CNES_CLS18 was produced by CLS and distributed by Aviso+, with support from Cnes ("
                           "https://www.aviso.altimetry.fr/)",

                "comment": "The dataset is a netcdf surface model at a 1/16 deg resolution over the globe. The "
                           "dataset contains two surfaces : Mean Sea Level (MSL) above WGS84 reference ellipsoid and "
                           "Lowest Astronomical Tide (LAT) referred to mean sea level surface.\n "
                "The LAT model is derived from FES2014 : FES2014 was produced by Noveltis, Legos and CLS and "
                           "distributed by Aviso+, with support from Cnes (https://www.aviso.altimetry.fr/)\n "
                "The LAT prediction module was run over 18 years (2003-2020) to retrieve a min values surface all "
                           "over the world.\n "
                "The MSL surface is derived from MSS_CNES_CLS18 by resampling to LAT surface extend and resolution. "
                           "MSS_CNES_CLS18 was produced by CLS and distributed by Aviso+, with support from Cnes ("
                           "https://www.aviso.altimetry.fr/)\n "
                "This surface model is used by Globe prediction module to predict tide referred to ellipsoid, "
                           "MSL or LAT",
                "history": f"Generation date : {date.today().isoformat()}",
            },  # initialize model version number
        ) as outDataset:
            variable = outDataset.createVariable(
                SeaSurfaceModel.lat_variable,
                np.float32,
                (DtmConstants.DIM_LAT, DtmConstants.DIM_LON),
                fill_value=np.nan,
                zlib=True,
                complevel=5,
                chunksizes=(160, 160),
            )
            variable.long_name = "Lowest Astronomical Tide elevation relative to MSL"
            variable.units = "m"
            variable.grid_mapping = DtmConstants.CRS_NAME
            variable.coordinates = f"{DtmConstants.DIM_LAT} {DtmConstants.DIM_LON}"
            lat_dataset = gdal.Open(lat_tiff_file)
            if lat_dataset.RasterXSize != len(longitudes) or lat_dataset.RasterYSize != len(latitudes):
                raise BadAssumption(
                    f"Expecting LAT raster to be of size ({len(longitudes)},{len(latitudes)}) : got ({lat_dataset.RasterXSize},{lat_dataset.RasterYSize})"
                )

            band = lat_dataset.GetRasterBand(1)
            outDataset[SeaSurfaceModel.lat_variable][:] = gdal_utils.gdal_to_netcdf(band)

            variable = outDataset.createVariable(
                SeaSurfaceModel.msl_variable,
                np.float32,
                (DtmConstants.DIM_LAT, DtmConstants.DIM_LON),
                fill_value=np.nan,
                zlib=True,
                complevel=5,
                chunksizes=(160, 160),
            )
            variable.long_name = "Mean Sea Level elevation relative to T/P ellipsoid"
            variable.units = "m"
            variable.grid_mapping = DtmConstants.CRS_NAME
            variable.coordinates = f"{DtmConstants.DIM_LAT} {DtmConstants.DIM_LON}"
            lat_dataset = gdal.Open(msl_tiff_file)
            if lat_dataset.RasterXSize != len(longitudes) or lat_dataset.RasterYSize != len(latitudes):
                raise BadAssumption(
                    f"Expecting MSL raster to be of size ({len(longitudes)},{len(latitudes)}) : got ({lat_dataset.RasterXSize},{lat_dataset.RasterYSize})"
                )

            band = lat_dataset.GetRasterBand(1)
            outDataset[SeaSurfaceModel.msl_variable][:] = gdal_utils.gdal_to_netcdf(band)


class SeaSurfaceProjector:
    def __init__(self, model_dir):
        self.sea_surface_model = os.path.join(model_dir, "reference_surfaces", "LAT_MSL_world_model_2003_2020.nc")
        # self.msl_model = os.path.join(model_dir, "reference_surfaces", "MSL_2015.tiff")
        #
        # if not os.path.exists(self.msl_model):
        #     raise BadParameter(f"MSL model data file does not exist, expected {self.msl_model}")
        SeaSurfaceModel.validate_model(self.sea_surface_model)

    def project_ellipsoid_to_lat(self, latitudes: np.ndarray, longitudes: np.ndarray, values: np.ndarray) -> np.ndarray:
        """
        Compute offset to apply to data to switch from the ellipsoid surface to LAT. Values are positives up
        Args:
            latitudes: array of latitudes points
            longitudes: array of longitudes points

        Returns:
        """
        latitudes = latitudes.transpose()
        longitudes = longitudes.transpose()
        points = np.column_stack((latitudes, longitudes))
        interpolator_function = self._create_interpolator(netcdf_variable_name=SeaSurfaceModel.lat_variable)
        interpolated_lat = interpolator_function(points)

        interpolator_function = self._create_interpolator(netcdf_variable_name=SeaSurfaceModel.msl_variable)
        interpolated_msl = interpolator_function(points)

        # to switch to LAT we need to substract LAT values rel/MSL and MSL rel/Ellipsoid
        return values - interpolated_lat - interpolated_msl

    def project_msl_to_lat(self, latitudes: np.ndarray, longitudes: np.ndarray, values: np.ndarray) -> np.ndarray:
        """
        Compute offset to apply to data to switch from MSL to LAT. Values are positives up
        Args:
            latitudes: array of latitudes points
            longitudes: array of longitudes points

        Returns:
        """
        latitudes = latitudes.transpose()
        longitudes = longitudes.transpose()
        points = np.column_stack((latitudes, longitudes))
        interpolator_function = self._create_interpolator(netcdf_variable_name=SeaSurfaceModel.lat_variable)
        interpolated_lat = interpolator_function(points)
        # to switch to LAT we need to substract LAT values rel/MSL
        msl_to_lat = values - interpolated_lat
        return msl_to_lat

    def _create_interpolator(self, netcdf_variable_name: str):
        dataset = gdal.Open(f"NETCDF:{self.sea_surface_model}:{netcdf_variable_name}")
        # we read all values but of course, this could be improved by reading only the usefull values

        Xgeo, Ygeo = gdal_utils.get_x_y_coordinated(dataset)
        # reverse Ygeo because RegularGridInterpolator require strictly ascending points
        Ygeo = Ygeo[::-1]
        # reverse data to match Ygeo reversion
        values = gdal_utils.gdal_to_netcdf(dataset.GetRasterBand(1))

        interpolator_function = interpol.RegularGridInterpolator(
            (Ygeo, Xgeo), values=values, method="linear", bounds_error=False, fill_value=None
        )
        return interpolator_function


if __name__ == "__main__":
    proj = SeaSurfaceProjector(model_dir="C:/data/datasets/Tide/tide_models/")
    re = proj.project_msl_to_lat(
        longitudes=np.array([-6 + 1 / 32, 1 + 1 / 32]),
        latitudes=np.array([49 + 1 / 32, 50 + 1 / 32]),
        values=np.array([0, 0]),
    )
    # compare to expected result extracted from netcdf file
    expected_result = -1 * np.array([-3.0798, -5.163297])
    if (np.abs(expected_result - re) > 10e-3).any():
        raise Exception(f"Bad result, interpolation failed expected {expected_result} vs {re}")

    re = proj.project_ellipsoid_to_lat(
        longitudes=np.array([-6 + 1 / 32, 1 + 1 / 32]),
        latitudes=np.array([49 + 1 / 32, 50 + 1 / 32]),
        values=np.array([0, 0]),
    )
    # compare to expected result extracted from netcdf file
    expected_result = np.array([-50, -40])
    if (np.abs(expected_result - re) > 2).any():
        raise Exception(f"Bad result, interpolation failed expected {expected_result} vs {re}")
