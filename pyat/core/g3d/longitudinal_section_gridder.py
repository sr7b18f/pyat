# pylint:disable=no-member
import math
import tempfile

import netCDF4 as nc
import numba as nb
import numpy as np
import pandas as pd

import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import ProgressMonitor


class LongitudinalSectionGridder:
    def __init__(self, grid_count: int, col_count: int, min_elevation, max_elevation, delta_elevation):
        """
        Args:
            col_count: number of column
        """
        self.grid_count = grid_count
        self.col_count = col_count
        self.row_count = 0
        self.max_elevation = max_elevation
        self.min_elevation = min_elevation
        self.delta_elevation = delta_elevation

        # Position of column head
        self.col_head_lon = np.full((grid_count, col_count), np.nan)
        self.col_head_lat = np.full((grid_count, col_count), np.nan)
        # Nb of column to search to
        self.column_scope = np.zeros((col_count,), dtype=int)

        self.logger = log.logging.getLogger(self.__class__.__name__)

    def add(self, lon: float, lat: float, grid_idx: int, col_idx: int) -> None:
        """
        add point in the numpy arrays.
        This points are the head of the columns.
        Args:
            lon: longitudes of the reference grids
            lat: latitudes of the reference grids
            grid_idx : grid index
            col_idx : column index
        """
        self.col_head_lon[grid_idx, col_idx] = lon
        self.col_head_lat[grid_idx, col_idx] = lat

    # pylint: disable=consider-using-with
    def initialize_grid(self):
        self.row_count = int(math.ceil(self.max_elevation - self.min_elevation) / self.delta_elevation)
        self.logger.info(f"Preparing {self.grid_count} grids, size {self.row_count}x{self.col_count} cells each")

        # Optimize access to data with numpy array
        self.temp_map_file_echo_mean = tempfile.NamedTemporaryFile(suffix="_reflectivity_mean.memmap")
        self.map_file_reflectivity_mean = np.memmap(
            self.temp_map_file_echo_mean,
            shape=(self.grid_count, self.row_count, self.col_count),
            dtype=np.float32,
            mode="w+",
        )
        self.map_file_reflectivity_mean.fill(0)

        # Optimize access to data with numpy array
        self.temp_map_file_echo_max = tempfile.NamedTemporaryFile(suffix="_reflectivity_max.memmap")
        self.map_file_reflectivity_max = np.memmap(
            self.temp_map_file_echo_max,
            shape=(self.grid_count, self.row_count, self.col_count),
            dtype=np.float32,
            mode="w+",
        )
        self.map_file_reflectivity_max.fill(0)

        self.temp_map_file_nb_value = tempfile.NamedTemporaryFile(suffix="_nb_value.memmap")
        self.map_file_nb_values = np.memmap(
            self.temp_map_file_nb_value,
            shape=(self.grid_count, self.row_count, self.col_count),
            dtype=int,
            mode="w+",
        )
        self.map_file_nb_values.fill(0)

    def values_count(self):
        return np.nansum(self.map_file_nb_values)


    @staticmethod
    @nb.njit(cache=True, fastmath=True, parallel=True)
    def _find_closest_ref_points(
        col_head_lon: np.ndarray,
        col_head_lat: np.ndarray,
        start_col_index: int,
        stop_col_index: int,
        init_col_index: int,
        sound_lon: np.ndarray,
        sound_lat: np.ndarray,
        grid_idx: np.ndarray,
        o_col_idx: np.ndarray,
    ):
        """
        Function aiming to find the closest column head point for each sound (in sound_lon/sound_lat)
        Parameters :
         - col_head_lon / col_head_lat : column head positions
         - start_col_index / stop_col_index : range of column index where seaching the nearest column head
         - init_col_index : initial column index for optimal search
         - sound_lon / sound_lat : sound positions
         - grid_idx: index of grid of each sound
        Resulting array is o_col_idx

        The algorithm search for a local minimum around init_col_index. First forward, then backward.
        """
        grid_size = col_head_lon.shape[0]
        for i in nb.prange(len(sound_lon)):
            min_dist = np.Inf
            o_col_idx[i] = -1
            # Sanity checks
            if grid_idx[i] < 0 or grid_idx[i] >= grid_size:
                continue
            if np.isnan(sound_lon[i]) or np.isnan(sound_lat[i]):
                continue

            col_incr = 1
            col_idx = init_col_index
            stop = False
            output_col = o_col_idx[i]
            while stop is False:
                # check column range
                if col_idx < start_col_index or col_idx >= stop_col_index:
                    stop = True
                else:
                    dist = (col_head_lon[grid_idx[i], col_idx] - sound_lon[i]) ** 2 + (
                        col_head_lat[grid_idx[i], col_idx] - sound_lat[i]) ** 2
                    # check if a minimum is reached
                    if dist < min_dist:
                        output_col = col_idx
                        min_dist = dist
                    else:
                        stop = True
                # check stop condition
                if stop and col_incr == 1:
                    col_incr = -1
                    col_idx = init_col_index
                    stop = False
                # next step
                col_idx += col_incr

            # end for loop
            o_col_idx[i] = output_col

    @staticmethod
    @nb.njit(cache=True, fastmath=True, parallel=True)
    def _find_closest_ref_points_dummy(
        col_head_lon: np.ndarray,
        col_head_lat: np.ndarray,
        start_col_index: int,
        stop_col_index: int,
        sound_lon: np.ndarray,
        sound_lat: np.ndarray,
        grid_idx: np.ndarray,
        o_col_idx: np.ndarray,
    ):
        """
        Function aiming to find the closest column head point for each sound (in sound_lon/sound_lat)
        Parameters :
         - col_head_lon / col_head_lat : column head positions
         - start_col_index / stop_col_index : range of column index where seaching the nearest column head
         - sound_lon / sound_lat : sound positions
         - grid_idx: index of grid of each sound
        Resulting array is o_col_idx
        """
        grid_size = col_head_lon.shape[0]
        for i in nb.prange(len(sound_lon)):
            min_dist = np.Inf
            o_col_idx[i] = -1
            # Sanity checks
            if grid_idx[i] < 0 or grid_idx[i] >= grid_size:
                continue
            if np.isnan(sound_lon[i]) or np.isnan(sound_lat[i]):
                continue

            output_col = o_col_idx[i]
            for col_idx in np.arange(start_col_index, stop_col_index):
                dist = (col_head_lon[grid_idx[i], col_idx] - sound_lon[i]) ** 2 + (
                    col_head_lat[grid_idx[i], col_idx] - sound_lat[i]) ** 2
                # check if a minimum is reached
                if dist < min_dist:
                    output_col = col_idx
                    min_dist = dist

            # end for loop
            o_col_idx[i] = output_col

    def fill_grid(
        self,
        sound_lon: np.ndarray,
        sound_lat: np.ndarray,
        sound_elev: np.ndarray,
        sound_backscatter: np.ndarray,
        grid_idx: np.ndarray,
        init_col_idx: int,
    ) -> np.ndarray:
        """
        Param :
           - sound_lon / sound_lat / sound_elev : position of the sounds
           - sound_backscatter : value of the sound
           - grid_idx : index of the grid of each sound
           - init_col_idx : approximative column index in grids
        """
        # Range of index of column where searching the nearest cell of each sound
        start_col_index = 0
        stop_col_index = self.col_count
        col_idx = np.zeros_like(sound_lon, dtype=int)

        self._find_closest_ref_points(
            self.col_head_lon,
            self.col_head_lat,
            start_col_index,
            stop_col_index,
            init_col_idx,
            sound_lon,
            sound_lat,
            grid_idx,
            col_idx,
        )
        row_idx = np.abs(np.round((sound_elev - self.max_elevation) / self.delta_elevation)).astype(int)

        _fill_grids(
            grid_idx,
            col_idx,
            row_idx,
            sound_backscatter,
            self.map_file_reflectivity_mean,
            self.map_file_reflectivity_max,
            self.map_file_nb_values,
        )

    def _interpolate(self, map_file_reflectivity: np.memmap, monitor: ProgressMonitor):
        nb_grid = map_file_reflectivity.shape[0]
        nb_row = map_file_reflectivity.shape[1]
        monitor.begin_task("Interpolating", nb_grid)
        for grid_idx in range(nb_grid):
            for row_idx in range(nb_row):
                map_file_reflectivity[grid_idx][row_idx] = pd.Series(
                    map_file_reflectivity[grid_idx][row_idx]).interpolate(limit=2,
                                                                          limit_direction="both",
                                                                          limit_area="inside")
            monitor.worked(1)

    def finalize(self, monitor: ProgressMonitor, interpolate: bool = False):
        self.map_file_reflectivity_mean[self.map_file_nb_values == 0] = np.nan
        self.map_file_reflectivity_max[self.map_file_nb_values == 0] = np.nan

        # # post interpolation :
        if interpolate:
            self.logger.info("Interpolate mean reflectivity")
            self._interpolate(self.map_file_reflectivity_mean, monitor=monitor.split(100))
            self.logger.info("Interpolate max reflectivity")
            self._interpolate(self.map_file_reflectivity_max, monitor=monitor.split(100))

        # return values in db
        # reflectivity mean
        np.log10(self.map_file_reflectivity_mean, out=self.map_file_reflectivity_mean,
                 where=self.map_file_reflectivity_mean > 0.0)
        np.multiply(self.map_file_reflectivity_mean, 10.0, out=self.map_file_reflectivity_mean)

        # reflectivity max
        np.log10(self.map_file_reflectivity_max, out=self.map_file_reflectivity_max,
                 where=self.map_file_reflectivity_max > 0.0)
        np.multiply(self.map_file_reflectivity_max, 10.0, out=self.map_file_reflectivity_max)

    def generate_g3d_file(self, path_g3d: str):
        with nc.Dataset(path_g3d, "w", format="NETCDF4") as dataset:
            dataset.dataset_type = "FlyTexture"
            dataset.history = "Created by Pyat"
            dataset.createDimension("datalayer_count", 2)
            datalayer_variable_name = dataset.createVariable("datalayer_variable_name", str, ("datalayer_count",))
            datalayer_variable_name[0] = "reflectivity_mean"
            datalayer_variable_name[1] = "reflectivity_max"

            height = self.row_count
            length = position = self.col_count
            vector = 2
            nb_grid = self.map_file_reflectivity_mean.shape[0]
            for grid_idx in range(nb_grid):
                grp = dataset.createGroup(f"{grid_idx + 1}".zfill(3))
                grp.createDimension("height", height)
                grp.createDimension("length", length)
                grp.createDimension("vector", vector)
                grp.createDimension("position", position)

                elevations = grp.createVariable("elevation", "f4", ("vector", "position"))
                elevations.units = "m"
                elevations.long_name = "elevation"
                elevations.standard_name = "elevation"
                elevations[0, :] = self.max_elevation
                elevations[1, :] = self.min_elevation

                longitude = grp.createVariable("longitude", "f8", ("vector", "position"))
                longitude.units = "degrees_north"
                longitude.long_name = "longitude"
                longitude.standard_name = "longitude"
                longitudes = self.col_head_lon[grid_idx::nb_grid]
                longitude[0, :] = longitudes[:]
                longitude[1, :] = longitudes[:]

                latitude = grp.createVariable("latitude", "f8", ("vector", "position"))
                latitude.units = "degrees_east"
                latitude.long_name = "latitude"
                latitude.standard_name = "latitude"
                latitudes = self.col_head_lat[grid_idx::nb_grid]
                latitude[0, :] = latitudes[:]
                latitude[1, :] = latitudes[:]

                reflectivity_mean = grp.createVariable("reflectivity_mean", "f4", ("height", "length"))
                reflectivity_mean.units = "dB"
                reflectivity_mean.long_name = "reflectivity_mean"
                reflectivity_mean.standard_name = "reflectivity_mean"
                reflectivity_mean[:] = self.map_file_reflectivity_mean[grid_idx][::-1, :]

                reflectivity_max = grp.createVariable("reflectivity_max", "f4", ("height", "length"))
                reflectivity_max.units = "dB"
                reflectivity_max.long_name = "reflectivity_max"
                reflectivity_max.standard_name = "reflectivity_max"
                reflectivity_max[:] = self.map_file_reflectivity_max[grid_idx][::-1, :]


@nb.njit(cache=True, fastmath=True)
def _fill_grids(
    grid_idxs: np.ndarray,
    col_idxs: np.ndarray,
    row_idxs: np.ndarray,
    backscatters: np.ndarray,
    o_mean_array: np.ndarray,
    o_max_array: np.ndarray,
    o_count_array: np.ndarray,
):
    """
    Function aiming to find the closest grid reference point (index in ref_lons/ref_lats) for each sounder point (in longitudes/latitudes)
    """
    grid_max_idx, row_max_idx, col_max_idx = o_mean_array.shape
    for grid_idx, col_idx, row_idx, backscatter in zip(grid_idxs, col_idxs, row_idxs, backscatters):
        # Sanity checks
        if grid_idx < 0 or grid_idx >= grid_max_idx:
            continue
        if row_idx < 0 or row_idx >= row_max_idx:
            continue
        if col_idx < 0 or col_idx >= col_max_idx:
            continue

        prev_count = o_count_array[grid_idx][row_idx][col_idx]
        prev_mean = o_mean_array[grid_idx][row_idx][col_idx]

        o_mean_array[grid_idx][row_idx][col_idx] = (prev_count * prev_mean + backscatter) / (prev_count + 1)
        o_max_array[grid_idx][row_idx][col_idx] = max(backscatter, o_max_array[grid_idx][row_idx][col_idx])
        o_count_array[grid_idx][row_idx][col_idx] += 1
