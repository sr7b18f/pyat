# pylint:disable=no-member
import json
import os
from typing import List

import numpy as np
import scipy.interpolate
import sonarnative
from sonarnative import SpatializerHolder

import pyat.core.sounder.sounder_driver_factory as sounder_driver_factory
import pyat.core.utils.argument_utils as arg_util
import pyat.core.utils.pyat_logger as log
from pyat.core.g3d.longitudinal_section_gridder import LongitudinalSectionGridder
from pyat.core.utils.coords import compute_detection_position, compute_distance
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor
from pyat.core.utils.numpy_utils import interp1d_nan
from pyat.core.xsf.xsf_driver import PLATFORM_HEADING


class LongitudinalSection:
    """
    longitudinal section of backscatters
    """

    def __init__(
        self,
        i_paths: List,
        o_paths: List,
        monitor: ProgressMonitor = DefaultMonitor,
        delta_across: float = 0,
        delta_elevation: float = 0,
        delta_along: float = 0,
        grid_count: int = 0,
        interpolate: bool = False,
        filters: str = None,
        overwrite: bool = False,
    ):
        """
        Constructor.
        """
        self.logger = log.logging.getLogger(LongitudinalSection.__name__)
        self.i_paths: List = i_paths
        self.o_paths: List = o_paths
        self.monitor = monitor
        self.delta_elevation: float = arg_util.parse_float("delta_elevation", delta_elevation)
        self.delta_along: float = arg_util.parse_float("delta_along", delta_along)
        self.interpolate = interpolate
        self.json_filters = filters
        self.overwrite = overwrite
        delta_across = arg_util.parse_float("delta_across", delta_across)
        grid_count = arg_util.parse_int("grid_count", grid_count)

        # Nb of swath per grid
        self.swath_count = 0
        # Nb of column per grid
        self.col_count = 0
        # Min/Max values read in xsf files
        self.min_across, self.max_across, self.min_elevation, self.max_elevation = (np.nan, np.nan, np.nan, np.nan)

        # Nb of expected grids
        if delta_across == 0 and grid_count == 0:
            raise ValueError("arguments delta_across or grid_count must take one positive value")

        self.delta_across = delta_across
        self.grid_count = grid_count

    def __call__(self):
        if len(self.i_paths) > 1 and len(self.o_paths) == 1:
            # Case of a merge
            self._convert(self.i_paths, self.o_paths[0], self.monitor)
        else:
            # Case of a single conversion
            self.monitor.set_work_remaining(len(self.i_paths * 100))
            for in_xsf, out_g3d in zip(self.i_paths, self.o_paths):
                if os.path.exists(out_g3d) and not self.overwrite:
                    self.logger.error(
                        f"File already exists and overwrite not allowed (allow overwrite with option : '-o --overwrite)"
                    )
                    continue

                self._convert([in_xsf], out_g3d, self.monitor.split(100))

    def _convert(self, in_xsfs: List[str], out_g3d: str, monitor: ProgressMonitor):
        """
        Initiate a gridder
        Invoke the spatialization process on each xsf and fill the gridder with the echoes
        Finalize the gridder
        Write the resulting G3D
        """
        gridder = self._prepare_gridder(in_xsfs)
        self._grid_xsf(in_xsfs, gridder, monitor)
        self.logger.info(f"Finalizing")
        if gridder.values_count() == 0:
            self.logger.error(f"No output data")
            return

        gridder.finalize(monitor=monitor, interpolate=self.interpolate)
        self.logger.info(f"Writing file: {out_g3d}")
        gridder.generate_g3d_file(out_g3d)

    def _grid_xsf(self, in_xsfs: List[str], gridder: LongitudinalSectionGridder, monitor: ProgressMonitor):
        """
        Invoke the spatialization process on each xsf and fill the gridder with the echoes
        """
        monitor.begin_task("Gridding", self.swath_count)
        echoes_count = self._get_biggest_echoes_count(in_xsfs, 1)
        mem_echos = sonarnative.MemEchos(echoes_count)
        swath_origin = 0
        for i_xsf in in_xsfs:
            # memory reservation
            spatializer = sonarnative.open_spatializer(i_xsf, True)
            self._apply_filters(spatializer)
            self.logger.info(f"read file {i_xsf}")

            for sp_swath in range(spatializer.get_swath_count()):
                monitor.worked(1)

                sonarnative.spatialize_in_memory(spatializer, sp_swath, 1, mem_echos)

                if mem_echos.size == 0:
                    continue
                # transform reflectivity to natural energy
                echos = np.power(10, mem_echos.echo / 10)

                across = mem_echos.across
                grid_idx = np.around(
                    ((across - self.min_across) / (self.max_across - self.min_across)) * (self.grid_count - 1)
                ).astype(int)

                col_idx = int(self.nav_distances[swath_origin + sp_swath] / self.delta_along)

                # values from sonarnative are sent in the gridder
                gridder.fill_grid(
                    sound_lon=mem_echos.longitude,
                    sound_lat=mem_echos.latitude,
                    sound_elev=mem_echos.elevation,
                    grid_idx=grid_idx,
                    sound_backscatter=echos,
                    init_col_idx=col_idx,
                )
            swath_origin += spatializer.get_swath_count()
            sonarnative.close_spatializer(spatializer)

    def _apply_filters(self, spatializer: SpatializerHolder):
        """
        parse json filters configuration file and apply defined filters on current spatializer
        """
        if not self.json_filters or not os.path.exists(self.json_filters):
            return
        with open(self.json_filters, "r", encoding="utf-8") as json_config_file:
            conf = json.load(json_config_file)
            if "acrossDistance" in conf:
                param = conf["acrossDistance"]
                enable = param["enable"]
                min_value = param["minValue"]
                max_value = param["maxValue"]
                native_param = sonarnative.AcrossDistanceParameter(enable, min_value, max_value)
                sonarnative.apply_across_distance_filter(spatializer, native_param)
            if "threshold" in conf:
                param = conf["threshold"]
                enable = param["enable"]
                min_value = param["minValue"]
                max_value = param["maxValue"]
                native_param = sonarnative.ThresholdParameter(enable, min_value, max_value)
                sonarnative.apply_threshold_filter(spatializer, native_param)
            if "beam" in conf:
                param = conf["beam"]
                enable = param["enable"]
                min_value = param["minValue"]
                max_value = param["maxValue"]
                native_param = sonarnative.BeamIndexParameter(enable, min_value, max_value)
                sonarnative.apply_beam_index_filter(spatializer, native_param)
            if "depth" in conf:
                param = conf["depth"]
                enable = param["enable"]
                min_value = param["minValue"]
                max_value = param["maxValue"]
                native_param = sonarnative.DepthParameter(enable, min_value, max_value)
                sonarnative.apply_depth_filter(spatializer, native_param)
            if "sidelobe" in conf:
                param = conf["sidelobe"]
                enable = param["enable"]
                threshold = param["threshold"]
                native_param = sonarnative.SideLobeParameter(enable, threshold)
                sonarnative.apply_side_lobe_filter(spatializer, native_param)
            if "bottom" in conf:
                param = conf["bottom"]
                enable = param["enable"]
                tolerance_absolute = param["toleranceAbsolute"]
                tolerance_percent = param["tolerancePercent"]
                angle_coefficient = param["angleCoefficient"]
                tolerance_type = param["type"]
                if tolerance_type == "RANGEPERCENT":
                    native_param = sonarnative.BottomFilterParameter.new_range_percent(angle_coefficient,
                                                                                       tolerance_percent, enable)
                    sonarnative.apply_bottom_filter(spatializer, native_param)
                elif tolerance_type == "SAMPLE":
                    native_param = sonarnative.BottomFilterParameter.new_sample(angle_coefficient,
                                                                                tolerance_absolute, enable)
                    sonarnative.apply_bottom_filter(spatializer, native_param)
            if "sampling" in conf:
                param = conf["sampling"]
                sampling = param["sampling"]
                native_param = sonarnative.SamplingParameter(sampling)
                sonarnative.apply_sampling_filter(spatializer, native_param)
            if "thresholdSphere" in conf:
                param = conf["thresholdSphere"]
                enable = param["enable"]
                min_value = param["minValue"]
                max_value = param["maxValue"]
                tolerance = param["tolerance"]
                native_param = sonarnative.ThresholdSphereParameter(enable, min_value, max_value, tolerance)
                sonarnative.apply_threshold_sphere_filter(spatializer, native_param)
            if "thresholdBelowSphere" in conf:
                param = conf["thresholdBelowSphere"]
                enable = param["enable"]
                min_value = param["minValue"]
                max_value = param["maxValue"]
                tolerance = param["tolerance"]
                native_param = sonarnative.ThresholdBelowSphereParameter(enable, min_value, max_value, tolerance)
                sonarnative.apply_threshold_below_sphere_filter(spatializer, native_param)

    def _compute_grid_features(self, in_xsfs: List[str]):
        """
        Compute the grids features to allow the gridder initialization :
         - Number of grids
         - Gap in meter between to grids
         - Number of columns in each grid
         - elevation min and max
        """
        self.nav_latitudes = np.ndarray(0)
        self.nav_longitudes = np.ndarray(0)
        self.nav_headings = np.ndarray(0)
        self.swath_count = 0
        for i_xsf in in_xsfs:
            with sounder_driver_factory.open_sounder(i_xsf) as xsf_driver:
                xsf_min_across, xsf_max_across, xsf_min_elevation, xsf_max_elevation = self._get_xsf_statistics(
                    xsf_driver
                )
                self.min_across = np.nanmin([self.min_across, xsf_min_across])
                self.max_across = np.nanmax([self.max_across, xsf_max_across])
                self.min_elevation = np.nanmin([self.min_elevation, xsf_min_elevation])
                self.max_elevation = np.nanmax([self.max_elevation, xsf_max_elevation])
                self.nav_latitudes = np.append(self.nav_latitudes, xsf_driver.read_platform_latitudes())
                self.nav_longitudes = np.append(self.nav_longitudes, xsf_driver.read_platform_longitudes())
                self.nav_headings = np.append(self.nav_headings, xsf_driver[PLATFORM_HEADING][:])
                self.swath_count += int(xsf_driver.sounder_file.swath_count)

        self.nav_longitudes = interp1d_nan(self.nav_longitudes)
        self.nav_latitudes = interp1d_nan(self.nav_latitudes)
        self.nav_headings = interp1d_nan(self.nav_headings)
        self.nav_distances = compute_distance(self.nav_longitudes, self.nav_latitudes)
        self.nav_distances = np.cumsum(self.nav_distances)

    def _prepare_gridder(self, in_xsfs: List[str]) -> LongitudinalSectionGridder:
        """
        Create and initialize a LongitudinalSectionGridder
        """
        self._compute_grid_features(in_xsfs)

        # compute col_count
        if self.delta_along == 0:
            self.col_count = self.swath_count
            self.delta_along = self.nav_distances[-1] / self.col_count
        else:
            self.col_count = int(np.ceil(self.nav_distances[-1] / self.delta_along))

        # compute grid_count
        if self.delta_across != 0.0:
            if self.grid_count == 0:
                min_across_index = np.floor(self.min_across / self.delta_across + 0.5)
                max_across_index = np.ceil(self.max_across / self.delta_across - 0.5)
                self.grid_count = int(max_across_index - min_across_index + 1)
            else:
                min_across_index = -np.floor(self.grid_count / 2)
                max_across_index = min_across_index + self.grid_count - 1
            self.min_across = (min_across_index - 0.5) * self.delta_across
            self.max_across = (max_across_index + 0.5) * self.delta_across

        self.delta_across = (self.max_across - self.min_across) / self.grid_count
        self.logger.info(f"grid_count: {self.grid_count} delta_across: {self.delta_across}")

        grid_gap_across = np.linspace(self.min_across + self.delta_across / 2, self.max_across - self.delta_across / 2,
                                      self.grid_count)
        grid_gap_along = np.linspace(0, (self.col_count - 1) * self.delta_along, self.col_count)
        # latitudes
        f_lat_cos = scipy.interpolate.interp1d(self.nav_distances, np.cos(np.radians(self.nav_latitudes)))
        f_lat_sin = scipy.interpolate.interp1d(self.nav_distances, np.sin(np.radians(self.nav_latitudes)))
        grid_latitudes = np.degrees(np.arctan2(f_lat_sin(grid_gap_along), f_lat_cos(grid_gap_along)))
        # longitudes
        f_lon_cos = scipy.interpolate.interp1d(self.nav_distances, np.cos(np.radians(self.nav_longitudes)))
        f_lon_sin = scipy.interpolate.interp1d(self.nav_distances, np.sin(np.radians(self.nav_longitudes)))
        grid_longitudes = np.degrees(np.arctan2(f_lon_sin(grid_gap_along), f_lon_cos(grid_gap_along)))
        # headings
        f_head_cos = scipy.interpolate.interp1d(self.nav_distances, np.cos(np.radians(self.nav_headings)))
        f_head_sin = scipy.interpolate.interp1d(self.nav_distances, np.sin(np.radians(self.nav_headings)))
        grid_headings = np.degrees(np.arctan2(f_head_sin(grid_gap_along), f_head_cos(grid_gap_along)))

        gridder = LongitudinalSectionGridder(
            self.grid_count,
            col_count=self.col_count,
            min_elevation=self.min_elevation,
            max_elevation=self.max_elevation,
            delta_elevation=self.delta_elevation
        )

        grid_along = np.zeros_like(grid_gap_across)
        for col_idx in range(0, self.col_count):
            # with sended values, compute the positions of the reference grids
            col_lons, col_lats = compute_detection_position(
                grid_along, grid_gap_across, grid_longitudes[col_idx], grid_latitudes[col_idx], grid_headings[col_idx]
            )
            for grid_idx, (lon, lat) in enumerate(zip(col_lons, col_lats)):
                gridder.add(lon, lat, grid_idx, col_idx)

        gridder.initialize_grid()

        return gridder

    def _get_xsf_statistics(self, xsf_driver):
        """
        read the xsf files and get the variables to compute statistics
        Returns: min_across, max_across, min_elevation, max_elevation
        """
        # ACROSS
        across = xsf_driver.read_across_distances(0, xsf_driver.sounder_file.swath_count)
        valid = xsf_driver.read_validity_flags(0, xsf_driver.sounder_file.swath_count)
        across[valid is False] = np.nan
        min_across = np.nanmin(across)
        max_across = np.nanmax(across)
        # DEPTH
        transducer_depth = xsf_driver.read_transducer_depth(0, xsf_driver.sounder_file.swath_count)
        detection_z = xsf_driver.read_vertical_distances(0, xsf_driver.sounder_file.swath_count)
        across[transducer_depth is False] = np.nan
        across[detection_z is False] = np.nan
        max_elevation = -np.nanmin(transducer_depth)
        min_elevation = -(np.nanmax(transducer_depth) + np.nanmax(detection_z))

        return min_across, max_across, min_elevation, max_elevation

    def _get_biggest_echoes_count(self, in_xsfs: List[str], swaths_wanted):
        """
        return the biggest echo count per swath in the input files
        Args:
            swaths_wanted: number of swath you want iterate to

        Returns:
            the biggest echo count found in the input files

        """
        echoes_count = 0
        for input_file in in_xsfs:
            spatializer = sonarnative.open_spatializer(input_file, True)
            swath_count = spatializer.get_swath_count()
            swath_list = list(range(swath_count))
            for i in swath_list[::swaths_wanted]:
                # arg: file / swath index / number of swath
                echoes_count = max(echoes_count, sonarnative.estimate_beam_echo_count(spatializer, i, swaths_wanted))
            sonarnative.close_spatializer(spatializer)
        return echoes_count


if __name__ == "__main__":
    longitudinal_section = LongitudinalSection(
        i_paths=[r"E:\aphilippot\data_test\XSF/0015_20190724_073901_raw.xsf.nc"],
        o_paths=[r"E:\aphilippot\data_test\g3d/0015_20190724_073901_raw.g3d.nc"],
        monitor=DefaultMonitor,
        delta_across=0,
        delta_elevation=5,
        grid_count=0,
        overwrite=True,
    )
    longitudinal_section()
