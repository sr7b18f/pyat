#! /usr/bin/env python3
# coding: utf-8

import datetime
import os
from typing import List, Optional

import numpy as np
from osgeo import gdal, gdalconst, osr

import pyat.core.dtm.dtm_driver as dtm_driver
import pyat.core.dtm.emodnet.dtm_standard_constants as DTM
import pyat.core.dtm.utils.process_utils as process_util
import pyat.core.utils.pyat_logger as log
from pyat.core.common.geo_file import SR_WGS_84
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor
from pyat.core.utils.string_utils import xstr


class TiffToDtmExporter:
    """
    Utility class to convert TIFF files (or any other GDAL raster file) as DTM (netcdf4 format)
    """

    def __init__(
        self,
        i_paths: List[str],
        o_paths: Optional[List[str]] = None,
        overwrite: bool = False,
        title: str = "",
        institution: str = "",
        source: str = "",
        references: str = "",
        comment: str = "",
        remove_abnormal_elevation = True,
        monitor: ProgressMonitor = DefaultMonitor,
    ):
        """
        Constructor.
        :param : i_paths : path of the imput file to convert
        :param : o_paths : resulting dtm file path
        """
        self.logger = log.logging.getLogger(self.__class__.__name__)
        self.i_paths = i_paths
        if o_paths:
            self.o_paths = list(o_paths)
        else:
            # Create output name from the input with the nc extension.
            self.o_paths = [path[: path.rfind(".")] + DTM.EXTENSION for path in i_paths]
        if len(self.o_paths) != len(self.i_paths):
            raise AttributeError("Number of Output/Input paths must be the same.")
        self.overwrite = overwrite
        self.remove_abnormal_evelation = remove_abnormal_elevation
        self.title = title
        self.institution = institution
        self.source = source
        self.references = references
        self.comment = comment

        self.monitor = monitor

    def __export_data(self, tiff_file: str, o_dtm_driver: dtm_driver.DtmDriver) -> None:
        """
        Launch the export of the file.
        Raised exception : IOError when error occurs while parsing the file
        """
        tiff_ds = self.__open_tiff(tiff_file)
        tmp_tiff = None
        try:
            # reproject tiff if not the expected SRS
            tiff_srs = SR_WGS_84
            if tiff_ds.GetProjectionRef():
                tiff_srs = osr.SpatialReference()
                tiff_srs.ImportFromWkt(tiff_ds.GetProjectionRef())
            self.logger.info(f"Tiff projection is {tiff_srs.ExportToProj4()}")
            self.__export_dataset(tiff_ds, o_dtm_driver)
        finally:
            # Close the tiff
            tiff_ds = None
            # Delete tmp file
            if tmp_tiff is not None:
                os.remove(tmp_tiff)

    def __export_dataset(self, tiff_ds: gdal.Dataset, o_dtm_driver: dtm_driver.DtmDriver) -> None:
        """
        Export the gdal dataset to the dtm.
        """
        # Creates dimensions and grid mapping in the output file
        o_dtm_driver.dtm_file.initialize_with_gdal_dataset(tiff_ds)

        metadata = {}
        metadata["title"] = xstr(self.title)
        metadata["institution"] = xstr(self.institution)
        metadata["source"] = xstr(self.source)
        metadata["references"] = xstr(self.references)
        metadata["comment"] = xstr(self.comment)
        metadata["history"] = f"Created from {os.path.basename(tiff_ds.GetFileList()[0])}"
        o_dtm_driver.initialize_file(metadata)

        # create elevation layer
        data = np.array(tiff_ds.ReadAsArray(), dtype=dtm_driver.get_type(DTM.ELEVATION_NAME))
        data = data[::-1]
        # Manage NoData
        tiff_band: gdal.Band = tiff_ds.GetRasterBand(1)
        no_data_value = tiff_band.GetNoDataValue()
        data[data == no_data_value] = dtm_driver.get_missing_value(DTM.ELEVATION_NAME)

        # filter extreme elevation values
        if self.remove_abnormal_evelation:
            self.logger.info("Remove elevation values above or less than +/- 32000m")
            data[data > 32000] = dtm_driver.get_missing_value(DTM.ELEVATION_NAME)
            data[data < -32000] = dtm_driver.get_missing_value(DTM.ELEVATION_NAME)

        # Write DTM
        o_dtm_driver.add_layer(layer_name=DTM.ELEVATION_NAME, data=data)

    def __open_tiff(self, tiff_file: str) -> gdal.Dataset:
        """
        Open the tiff. Return the resulting dataset
        """
        dataset = gdal.Open(tiff_file, gdalconst.GA_ReadOnly)
        if dataset is None or dataset.GetDriver().ShortName != "GTiff":
            dataset = None
            raise AttributeError("File is not a Tiff.")
        return dataset

    def __call__(self) -> None:
        """Run method."""
        begin = datetime.datetime.now()
        self.monitor.set_work_remaining(len(self.i_paths))
        file_in_error = []
        for ind, tiff_file in enumerate(self.i_paths):
            try:
                self.logger.info(f"Starting to convert {tiff_file} to {self.o_paths[ind]}")
                if not self.overwrite and os.path.exists(self.o_paths[ind]):
                    self.logger.warning("File exists and overwrite is not allowed. Convertion aborted.")
                else:
                    now = datetime.datetime.now()
                    with dtm_driver.open_dtm(self.o_paths[ind], "w") as o_dtm_driver:
                        self.__export_data(tiff_file, o_dtm_driver)

                    self.logger.info(
                        f"End of conversion for {tiff_file} : {datetime.datetime.now() - now} time elapsed\n"
                    )
            except Exception as error:
                file_in_error.append(tiff_file)
                self.logger.error("An exception was thrown!", exc_info=True, stack_info=True)
            self.monitor.worked(1)

        self.monitor.done()
        process_util.log_result(self.logger, begin, file_in_error)


if __name__ == "__main__":
    exporter = TiffToDtmExporter(
        i_paths=["E:/temp/utm_test.tif"], o_paths=["E:/temp/sample_utm.dtm.nc"], overwrite=True
    )
    exporter()
