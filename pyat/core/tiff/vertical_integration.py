# pylint:disable=no-member
import time
from typing import List, Optional, Dict

import sonarnative
import numpy as np
from osgeo import osr

import pyat.core.utils.pyat_logger as log
from pyat.core.tiff import tiff_gridder
from pyat.core.utils import argument_utils
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor


class VerticalIntegration:
    """
    Vertical integration of backscatters
    """
    @property
    def geobox(self) -> argument_utils.Geobox:
        return self._geobox

    @geobox.setter
    def geobox(self, geobox: argument_utils.Geobox) -> None:
        self._geobox = geobox

    def __init__(
        self,
        i_paths: List,
        o_path: str,
        monitor: ProgressMonitor = DefaultMonitor,
        target_resolution: float = 1.0 / 3600.0,
        coord: Optional[Dict] = None,
        threshold_min: float = None,
        threshold_max: float = None,
        bottom_filter_sample_tolerance_absolute: int = None,
        bottom_filter_rangepercent_tolerence_percent: float = None,
        bottom_filter_angle_coefficient: float = None,
    ):
        """
        Constructor.
        """
        self.logger = log.logging.getLogger(VerticalIntegration.__name__)
        self.i_paths = i_paths
        self.o_path = o_path
        self.monitor = monitor
        self.tiff_gridder = tiff_gridder
        self.spatial_resolution = float(target_resolution)
        self.coord = coord
        self.threshold_min = threshold_min
        self.threshold_max = threshold_max
        self.bottom_filter_sample_tolerance_absolute = bottom_filter_sample_tolerance_absolute
        self.bottom_filter_rangepercent_tolerence_percent = bottom_filter_rangepercent_tolerence_percent
        self.bottom_filter_angle_coefficient = bottom_filter_angle_coefficient

        if coord is not None:
            self.geobox = argument_utils.parse_geobox("coord", coord)
            self.geobox.spatial_reference = osr.SpatialReference()
            self.geobox.spatial_reference.ImportFromProj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")

        self.tiff_gridder = self.tiff_gridder.TiffGridder(
            self.o_path,
            geobox=self.geobox,
            spatial_resolution=self.spatial_resolution,
            monitor=self.monitor,
        )

    def __call__(self):
        self.monitor.set_work_remaining(len(self.i_paths))
        # verify not two bottom filters
        if self.bottom_filter_rangepercent_tolerence_percent and self.bottom_filter_sample_tolerance_absolute:
            self.logger.warning(f"Two bottom filters in params: tolerence_percent and tolerance_absolute")

        self.logger.info(f"read input files")

        swaths_wanted = 1
        echoes_count = self.get_biggest_echoes_count(echoes_count=0, swaths_wanted=swaths_wanted)

        # memory reservation
        mem_echos = sonarnative.MemEchos(echoes_count)

        # file initialization
        self.tiff_gridder.initialize_tiff_file(float)

        for input_file in self.i_paths:
            spatializer = sonarnative.open_spatializer(input_file, True)

            self.apply_filters(spatializer)

            swath_count = spatializer.get_swath_count()
            self.logger.info(f"read file {input_file}")

            swath_list = list(range(swath_count))
            try:
                for i in swath_list[::swaths_wanted]:
                    # use of memory
                    # arg: file / swath index / number of swath wanted
                    sonarnative.spatialize_in_memory(spatializer, i, swaths_wanted, mem_echos)

                    longitudes = mem_echos.longitude
                    latitudes = mem_echos.latitude
                    echos = mem_echos.echo

                    # if longitude is nan, delete it
                    echos = echos[np.logical_not(np.isnan(longitudes))]
                    latitudes = latitudes[np.logical_not(np.isnan(longitudes))]
                    longitudes = longitudes[np.logical_not(np.isnan(longitudes))]

                    # transform reflectivity to natural energy
                    echos = np.power(10, echos / 10)

                    # First, compute columns and rows index
                    columns, rows = self.tiff_gridder.project_coords(longitudes, latitudes)
                    # Then, process values
                    self.tiff_gridder.grid_average(columns, rows, echos)

            finally:
                # release memory
                sonarnative.close_spatializer(spatializer)
                self.monitor.worked(1)

        # return values in db
        self.tiff_gridder.map_file = 10 * np.log10(self.tiff_gridder.map_file)

        # tiff finalized
        self.tiff_gridder.finalize_tiff()
        self.logger.info(f"file created")
        self.monitor.done()

    def apply_filters(self, spatializer):
        """
        apply the filters if True
        Args:
            spatializer:
        """
        # threshold_filter
        if self.threshold_min or self.threshold_max:
            sonarnative.apply_threshold_filter(
                spatializer,
                sonarnative.ThresholdParameter(True, float(self.threshold_min), float(self.threshold_max)),
            )
        # bottom_filter
        # sample
        if self.bottom_filter_sample_tolerance_absolute:
            sonarnative.apply_bottom_filter(
                spatializer,
                sonarnative.BottomFilterParameter.new_sample(
                    float(self.bottom_filter_angle_coefficient),
                    int(self.bottom_filter_sample_tolerance_absolute),
                    True,
                ),
            )
        # rangepercent
        if self.bottom_filter_rangepercent_tolerence_percent:
            #
            sonarnative.apply_bottom_filter(
                spatializer,
                sonarnative.BottomFilterParameter.new_range_percent(
                    float(self.bottom_filter_angle_coefficient),
                    float(self.bottom_filter_rangepercent_tolerence_percent),
                    True,
                ),
            )

    def get_biggest_echoes_count(self, echoes_count, swaths_wanted):
        """
        return the biggest echo count per swath in the input files
        Args:
            echoes_count: number of echo in a swath
            swaths_wanted: number of swath you want iterate to

        Returns:
            the biggest echo count found in the input files

        """
        for input_file in self.i_paths:
            spatializer = sonarnative.open_spatializer(input_file, True)
            swath_count = spatializer.get_swath_count()
            swath_list = list(range(swath_count))
            for i in swath_list[::swaths_wanted]:
                # arg: file / swath index / number of swath
                echoes_count = max(echoes_count, sonarnative.estimate_beam_echo_count(spatializer, i, swaths_wanted))
            sonarnative.close_spatializer(spatializer)
        return echoes_count


if __name__ == "__main__":
    date = int(time.time())
    xsf_to_tiff = VerticalIntegration(
        i_paths=["list", "of", "input_files.xsf.nc"],
        o_path=f"path\\to\\outfile\\xsf_to_geotiff_{date}.tiff",
        monitor=DefaultMonitor,
        target_resolution=0.00002777777778,
        coord={
            "north": -12.805393949090519,
            "south": -12.823830586993873,
            "west": 45.35544657735835,
            "east": 45.367953410896924,
        },
        threshold_min=None,
        threshold_max=None,
        bottom_filter_sample_tolerance_absolute=None,  # int
        bottom_filter_rangepercent_tolerence_percent=None,
        bottom_filter_angle_coefficient=None,
    )
    xsf_to_tiff()
