#! /usr/bin/env python3
# coding: utf-8
from pyat.core.sounder.sounder_driver import SounderDriver
from pyproj import Geod
import numpy as np
import json
from typing import List, Optional, Tuple

import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor
import pyat.core.utils.argument_utils as arg_util
import pyat.core.sounder.sounder_driver_factory as sounder_driver_factory


class SpatialResolutionEvaluator:
    def __init__(
        self,
        i_paths: List[str],
        o_path: Optional[str] = None,
        monitor: ProgressMonitor = DefaultMonitor,
    ):
        """
        Constructor.
        :param : i_paths : path of the sounding files to analyse
        :param : result : path of the resulting json file
        """
        i_paths = arg_util.parse_list_of_files("i_paths", i_paths)
        self.i_path = i_paths[0]
        self.o_path = o_path
        self.monitor = monitor
        self.logger = log.logging.getLogger(self.__class__.__name__)

    def __evaluate_beams(self, beam_iter, geod: Geod) -> float:
        """
        Iterate over the iterator for extracting longitudes and latitudes.
        Evaluate the mean distance between:
          - 2 consecutive beams of the same swath
          - the beams of 2 consecutive swaths
        """
        lons, lats = next(beam_iter)
        mean_distance_between_swath = self.evaluate_mean_distance_between_swath(lons, lats, geod)
        mean_distance_between_beam = self.evaluate_mean_distance_between_beam(lons, lats, geod)
        return max(mean_distance_between_swath, mean_distance_between_beam)

    def evaluate_mean_distance_between_swath(self, lons: np.ndarray, lats: np.ndarray, geod: Geod) -> float:
        """
        returns the mean of all differences between 2 consecutive swath
        """
        _, _, distance = geod.inv(lons[0:-1], lats[0:-1], lons[1:], lats[1:])
        distance[distance == 0] = np.nan  # useful to ignore 0 when using np.nanmean
        return np.nanmean(distance)

    def evaluate_mean_distance_between_beam(self, lons: np.ndarray, lats: np.ndarray, geod: Geod) -> float:
        """
        returns the mean of all differences between 2 consecutive beam of same swath
        """
        _, _, distance = geod.inv(lons[:, 0:-1], lats[:, 0:-1], lons[:, 1:], lats[:, 1:])
        distance[distance == 0] = np.nan  # useful to ignore 0 when using np.nanmean
        return np.nanmean(distance)

    def _evaluate_resolution_meter(self, i_sounder_driver: SounderDriver, geod: Geod) -> float:
        """
        Browse some swaths to evaluate the spatial resolution in meter
        """
        swath_count = i_sounder_driver.sounder_file.swath_count
        if swath_count > 20:
            res_meter = np.nanmean(
                [
                    # Analysing the 10th first swaths
                    self.__evaluate_beams(i_sounder_driver.iter_beam_positions(10), geod),
                    # Analysing the 10th last swaths
                    self.__evaluate_beams(i_sounder_driver.iter_beam_positions(10, swath_count - 10), geod),
                ]
            )
        else:
            # Analysing all swaths
            res_meter = self.__evaluate_beams(i_sounder_driver.iter_beam_positions(20), geod)

        self.logger.info(f"Evaluation of the spatial resolution in meter is {res_meter}")

        return res_meter

    def _round_resolution_meter(self, res_meter: float) -> float:
        """
        Round the resolution in meter according to its precision
        """
        if res_meter > 100.0:
            # Precision 10m
            return round(res_meter, -1)

        if res_meter > 10.0:
            # Precision 1m
            return round(res_meter)

        # Precision 0.1m
        return round(res_meter, 1)

    def _evaluate_resolution_degree(self, i_sounder_driver: SounderDriver, res_meter: float, geod: Geod) -> float:
        """
        Use the spatial resolution in meter as distance from one point of the navigation to evaluate the resolution in degree
        """
        nav_point = int(i_sounder_driver.sounder_file.swath_count / 2)
        lons = i_sounder_driver.read_platform_longitudes()
        lats = i_sounder_driver.read_platform_latitudes()

        lon = lons.flat[nav_point]
        lon2, _, _ = geod.fwd(lon, lats.flat[nav_point], 90.0, res_meter)
        res_degree = abs(lon - lon2)

        self.logger.info(f"Evaluation of the spatial resolution in degrre {res_degree}")

        return res_degree

    def __call__(self) -> Tuple[float, float]:
        """Run method."""
        self.logger.info(f"Starting spatial resolution evaluation of {self.i_path}")
        geod = Geod(ellps="WGS84")
        res_meter = 2.0
        with sounder_driver_factory.open_sounder(self.i_path) as i_sounder_driver:
            res_meter = self._evaluate_resolution_meter(i_sounder_driver, geod)
            res_meter = self._round_resolution_meter(res_meter)
            res_degree = self._evaluate_resolution_degree(i_sounder_driver, res_meter, geod)

        self.write_result(res_meter, res_degree)
        return res_meter, res_degree

    def write_result(self, res_meter: float, res_degree: float) -> None:
        """
        Serialize the result in JSON format
        """
        result = {
            "meter": res_meter,
            "degree": res_degree,
        }

        if self.o_path:
            with open(self.o_path, "w", encoding="utf-8") as f:
                self.logger.info(f"Writing result to {self.o_path}")
                json.dump({"result": result}, f, indent=4)
        else:
            self.logger.info(json.dumps({"result": result}, indent=4))


if __name__ == "__main__":
    evaluator = SpatialResolutionEvaluator(["e:/temp/0136_20120607_083636_ShipName_ref.mbg"])
    evaluator()
