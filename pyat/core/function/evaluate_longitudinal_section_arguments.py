#! /usr/bin/env python3
# coding: utf-8

import datetime
import json

import numpy as np
import netCDF4 as nc
from typing import List, Optional

import pyat.core.dtm.utils.process_utils as process_util
import pyat.core.utils.pyat_logger as log
from pyat.core.function.evaluate_sounder_spatial_resolution import SpatialResolutionEvaluator
from pyat.core.sounder import sounder_driver_factory
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor


class LongitudinalSectionArgumentsEvaluator:
    """ """

    def __init__(
        self,
        i_paths: List[str],
        o_path: Optional[str] = None,
        monitor: ProgressMonitor = DefaultMonitor,
    ):
        """
        Constructor.
        """
        self.i_paths = i_paths
        self.o_path = o_path
        self.monitor = monitor
        self.logger = log.logging.getLogger(self.__class__.__name__)

    def __call__(self) -> None:
        """Run method."""
        begin = datetime.datetime.now()
        file_in_error: List[str] = []

        self.logger.info("Evalutating...")

        delta_elevation, delta_across, delta_along = self._evaluate()

        self._write_result(delta_elevation, delta_across, delta_along)

        process_util.log_result(self.logger, begin, file_in_error)

    def _evaluate(self):
        """

        """
        _min_across = float('inf')
        _max_across = float('-inf')
        _min_depth = float('inf')
        _max_depth = float('-inf')
        _max_sample_interval = float('-inf')
        _max_sound_speed = float('-inf')

        for i_path in self.i_paths:
            with sounder_driver_factory.open_sounder(i_path) as xsf_driver:

                # ACROSS
                across = xsf_driver.read_across_distances(0, xsf_driver.sounder_file.swath_count)
                valid = xsf_driver.read_validity_flags(0, xsf_driver.sounder_file.swath_count)
                across[valid is False] = np.nan
                # min_across
                min_across = np.nanmin(across)
                if min_across < _min_across:
                    _min_across = min_across
                # max_across
                max_across = np.nanmax(across)
                if max_across > _max_across:
                    _max_across = max_across
                # DEPTH
                transducer_depth = xsf_driver.read_transducer_depth(0, xsf_driver.sounder_file.swath_count)
                detection_z = xsf_driver.read_vertical_distances(0, xsf_driver.sounder_file.swath_count)
                # min_elevation
                min_depth = np.nanmin(transducer_depth)
                if min_depth < _min_depth:
                    _min_depth = min_depth
                # max_elevation
                max_depth = min_depth + np.nanmax(detection_z)
                if max_depth > _max_depth:
                    _max_depth = max_depth
                # sample_interval
                sample_interval = xsf_driver["Sonar"]["Beam_group1"]["sample_interval"][:]
                max_sample_interval = np.nanmax(sample_interval)
                if max_sample_interval > _max_sample_interval:
                    _max_sample_interval = max_sample_interval
                # sound_speed
                sound_speed = xsf_driver["Sonar"]["Beam_group1"]["sound_speed_at_transducer"][:]
                max_sound_speed = np.nanmax(sound_speed)
                if max_sound_speed > _max_sound_speed:
                    _max_sound_speed = max_sound_speed

        # deltas
        delta_elevation = round(_max_sample_interval * _max_sound_speed * 2, 2)
        delta_across = round(np.abs(_max_depth) / 100)

        along_evaluator = SpatialResolutionEvaluator(self.i_paths)
        delta_along, _ = along_evaluator()

        self.logger.info(f"delta elevation: {delta_elevation}\ndelta across: {delta_across}")
        return delta_elevation, delta_across, delta_along

    def _write_result(self, delta_elevation: float, delta_across: float, delta_along: float) -> None:
        """
        Serialize the result in JSON format
        """
        result = {
            "delta_elevation": delta_elevation,
            "delta_across": delta_across,
            "delta_along": delta_along,
        }
        if self.o_path:
            with open(self.o_path, "w", encoding="utf-8") as f:
                self.logger.info(f"Writing result to {self.o_path}")
                json.dump({"result": result}, f, indent=4)
        else:
            print(json.dumps({"result": result}, indent=4))


if __name__ == "__main__":
    longitudinal_section_arguments_evaluator = LongitudinalSectionArgumentsEvaluator(
        i_paths=[
            r"E:\aphilippot\data_test\XSF/0005_20190724_052308_raw.xsf.nc",
            r"E:\aphilippot\data_test\XSF/0015_20190724_073901_raw.xsf.nc",
        ],
        o_path=""
    )
    longitudinal_section_arguments_evaluator.__call__()
