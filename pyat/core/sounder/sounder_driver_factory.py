#! /usr/bin/env python3
# coding: utf-8

import os
from typing import Optional

from pyat.core.mbg.mbg_driver import MbgDriver
from pyat.core.sounder.sounder_driver import SounderDriver
from pyat.core.xsf.xsf_driver import XsfDriver


def open_sounder(file_path: str, mode: str = "r"):
    """
    Define a With Statement Context Managers for a SounderDriver
    Allow opening a SounderDriver in a With Statement
    """
    driver: Optional[SounderDriver] = None
    file_extension = os.path.splitext(file_path)[1]

    if file_extension == ".mbg":
        driver = MbgDriver(file_path)
    elif file_extension in (".nc", ".xsf"):
        driver = XsfDriver(file_path)

    if driver is None:
        raise ValueError(f"Unsupported sounder file : {file_path}.")

    class ContextManager:
        def __enter__(self):
            if not driver is None:
                driver.open(mode)
            return driver

        def __exit__(self, exc_type, exc_value, traceback):
            if not driver is None:
                driver.close()

    return ContextManager()
