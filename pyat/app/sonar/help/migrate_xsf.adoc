:last-update-label!:

== Migrates XSF files

=== Overview
This this application reports validity flags and bias corrections on a set of xsf files.

Flags and corrections are retrieved in xsf files of same name from the reference folder.
