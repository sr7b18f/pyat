#! /usr/bin/env python3
# coding: utf-8

import json
import logging
import os
import sys
from typing import Callable

from py4j.java_gateway import JavaGateway, Py4JError

import pyat.core.utils.argument_utils as arg_util
from pyat.core.utils.monitor import DefaultMonitor, JavaMonitor


def launch_application(json_conf_file_path: str, callable_process: Callable, json_parameters=None) -> None:
    """
    Launch an application wrapping a callable process.
    When the application is launched by Java (Globe), the progress monitor is searched in Py4J gateway.

    :param json_conf_file_path: the path of the application (*_app.py)
    :param callable_process: a class previously imported. eg:
        'from pyat.core.dtm.peak_detector import PeakFinder'
        -> pass PeakFinder as a parameter
    :param json_parameters: If specified, take priority over the command line arguments. Make it easier to call it
    inside a script

    WHen called through a command line:
    Parameters of the application are read in the json file when present as the first argument of the command line.
    If this json file is missing, all the command line is considered as the parameters of the application

    Then the application is instantiated with all the parameters and finally called
    """

    scriptId = os.environ.get("PYTHON_SCRIPT_ID")

    # Set the logger and the monitor depending on what called the application
    monitor, logger = init_logger(scriptId, callable_process)

    if not json_parameters:
        # Get parameters from command line. First one is the name of the processed file
        arguments = parse_command_line_arguments(sys.argv[1:], json_conf_file_path, callable_process, logger)
    else:
        # if there is a json_parameters given as argument, take priority over the command line arguments
        arguments = json.loads(json_parameters)

    run(arguments, monitor, logger, callable_process)


def run(arguments, monitor, logger, callable_process):
    """
    :param arguments: dictionary containing parameters (previously parsed from json)
    :param monitor: from init_logger
    :param logger: from init_logger
    :param callable_process: a class previously imported cf. launch_application
    """
    if arguments:
        try:
            monitor.begin_task(str(callable_process), 100)
            arguments["monitor"] = monitor
            app = callable_process(**arguments)
            app()
            monitor.done()
        except ValueError as e:
            logger.error(str(e))
        except Exception as e:
            logger.error(f"An exception was thrown : {str(e)}", exc_info=True)
    else:
        logger.error(
            "Useless process without input(s) and parameter(s). Stop the program. Please enter input with "
            "the option -i I_PATHS [I_PATHS ...], --i_paths I_PATHS [I_PATHS ...]."
        )


def init_logger(scriptId, callable_process: Callable):
    """
    :param scriptId:
    :param callable_process: a class previously imported cf. launch_application

    :return monitor: get previously by init_logger
    :return logger: get previously by init_logger
    """
    monitor = DefaultMonitor

    logger = logging.getLogger(callable_process.__name__)
    logger.setLevel(logging.INFO)
    if scriptId:
        try:
            logging.getLogger("py4j").setLevel(100)
            gateway = JavaGateway()
            gateway.jvm.System.getProperty("java.runtime.name")

            api = gateway.entry_point
            if api.getScriptMonitor(scriptId):
                monitor = JavaMonitor(api.getScriptMonitor(scriptId))

            logger.info(f"Py4J connection enabled: SCRIPT_ID = {scriptId}")
            return monitor, logger
        except Py4JError:
            logger.info("No JVM listening.")
            return monitor, logger
    return monitor, logger


def parse_command_line_arguments(argv, json_conf_file_path: str, callable_process: Callable, logger):
    """
    :param argv: command line arguments passed as argument through 'sys.argv[1:]'
    :param json_conf_file_path:
    :param callable_process: a class previously imported cf. launch_application
    :param logger: get previously by init_logger

    :return json_dict: a dictionary containing the parameters passed
    """
    json_dict = {}
    if not argv:
        logger.error("Parameters required")
        return json_dict
    logger.debug(f'Received parameters = "{argv}"')

    if argv[0].endswith("json"):
        # Arguments are passed in a json arguments file
        with open(argv[0], "r", encoding="utf-8") as file:
            json_dict = json.load(file)
    else:
        # Arguments are passed in the command line
        parser = arg_util.create_argv_parser(callable_process.__name__, json_conf_file_path)
        json_dict = vars(parser.parse_args(argv))
    return json_dict


def get_json_configuration_file(application_file_path: str) -> str:
    """
    Compute the json configuration file of the an application.

    Arguments:
        application_file_path -- the path of the application (*_app.py).

    Returns:
        [str] -- the json configuration file

    """

    bn = os.path.basename(application_file_path)
    dir_path = os.path.dirname(application_file_path)
    return os.path.join(dir_path, "conf", bn[: bn.rfind("_")] + ".json")
