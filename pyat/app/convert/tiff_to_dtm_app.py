#! /usr/bin/env python3
# coding: utf-8

import pyat.app.application_utils as app_util
from pyat.core.tiff.tiff_to_dtm_exporter import TiffToDtmExporter

if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), TiffToDtmExporter)
