#! /usr/bin/env python3
# coding: utf-8

from datetime import datetime
from os import path

from netCDF4 import Dataset
from osgeo import gdal, ogr, osr

import pyat.app.application_utils as app_util
import pyat.core.utils.pyat_logger as log

gdal.UseExceptions()

# Constant fields
FILE_FIELD = "FicData"
PROFILE_FIELD = "Profil"
START_DATE_FIELD = "DateDeb"
END_DATE_FIELD = "DateFin"
START_TIME_FIELD = "HeureDeb"
END_TIME_FIELD = "HeureFin"
START_LAT_FIELD = "LatDeb"
START_LON_FIELD = "LonDeb"
END_LAT_FIELD = "LatFin"
END_LON_FIELD = "LonFin"
CAMPAIGN_NAME = "Campagne"
CAMPAIGN_NUM = "NumCamp"
NAVIGATION = "Navigation"
TOOL = "Outil"


class Convert2Shp:
    """
    This class provides methods to convert MBG/NVI to shape files.
    """

    def __init__(self, **params):
        """
        Initialize parameters.
        """
        if "i_paths" in params:
            self.input_files = params["i_paths"]
        if "o_path" in params:
            self.output_file = params["o_path"]

        self.added_const_fields = {}
        if "campaign" in params:
            self.added_const_fields[CAMPAIGN_NAME] = params["campaign"]
        if "campNum" in params:
            self.added_const_fields[CAMPAIGN_NUM] = params["campNum"]
        if "navigation" in params:
            self.added_const_fields[NAVIGATION] = params["navigation"]
        if "tool" in params:
            self.added_const_fields[TOOL] = params["tool"]

        self.overwrite = bool(params["overwrite"]) if "overwrite" in params else False

    @staticmethod
    def __get_utc_date(julian_date, julian_time):
        """
        Converts julian date to UTC
        """
        epoch = (julian_date - 2440588) * 24 * 3600 + (julian_time / 1000)
        return datetime.utcfromtimestamp(epoch)

    def fill_polyline_and_fields_from_mbg(self, filein, polyline, feature):
        """
        Reads an MBG file, and fill the provided polyline and feature.
        """
        print("Input file (mbg) :", filein)
        with Dataset(filein) as mbg:
            lon = mbg.variables["mbAbscissa"][:]
            lat = mbg.variables["mbOrdinate"][:]
            flag_variable = mbg.variables["mbCFlag"]
            flag_variable.set_auto_chartostring(False)  # disable char to string autoconversion
            flag_variable.set_auto_maskandscale(False)  # do not try to compare byte to invalid value as int
            validity_flag = flag_variable[:]  # read the flags

            start_date = Convert2Shp.__get_utc_date(mbg.getncattr("mbStartDate"), mbg.getncattr("mbStartTime"))
            end_date = Convert2Shp.__get_utc_date(mbg.getncattr("mbEndDate"), mbg.getncattr("mbEndTime"))

            # Populate polyline
            if len(lat) > 1:
                for lg, lt, flag in zip(lon, lat, validity_flag):
                    # keep only valid points
                    if flag[0] == b"\x02":
                        polyline.AddPoint(lg[0], lt[0])
                    elif len(flag) > 1 and flag[1] == b"\x02":  # try to get value from second antenna
                        polyline.AddPoint(lg[1], lt[1])

                count = polyline.GetPointCount()
                if count == 1:
                    polyline.AddPoint(polyline.GetPoint(0)[0], polyline.GetPoint(0)[1])
                # set fields value
                feature.SetField(FILE_FIELD, path.basename(filein))
                feature.SetField(PROFILE_FIELD, path.splitext(path.basename(filein))[0])
                feature.SetField(START_DATE_FIELD, start_date.strftime("%d/%m/%Y"))
                feature.SetField(END_DATE_FIELD, end_date.strftime("%d/%m/%Y"))
                feature.SetField(START_TIME_FIELD, start_date.strftime("%H:%M:%S"))
                feature.SetField(END_TIME_FIELD, end_date.strftime("%H:%M:%S"))
                feature.SetField(START_LON_FIELD, polyline.GetPoint(0)[0])
                feature.SetField(START_LAT_FIELD, polyline.GetPoint(0)[1])
                feature.SetField(END_LON_FIELD, polyline.GetPoint(count - 1)[0])
                feature.SetField(END_LAT_FIELD, polyline.GetPoint(count - 1)[1])
                self.logger.info(f"Create {filein} geometry : {polyline.GetPointCount()} points")

            else:
                self.logger.warning(f"Skip file {filein} having {len(lat)} points")

    def fill_polyline_and_fields_from_nvi(self, filein, polyline, feature):
        """
        Reads an NVI file, and fill the provided polyline and feature.
        """
        print("Input file (nvi) :", filein)
        with Dataset(filein) as nvi:
            lon = nvi.variables["mbAbscissa"][:]
            lat = nvi.variables["mbOrdinate"][:]
            flag_variable = nvi.variables["mbPFlag"]
            flag_variable.set_auto_chartostring(False)  # disable char to string autoconversion
            flag_variable.set_auto_maskandscale(False)  # do not try to compare byte to invalid value as int
            validity_flag = flag_variable[:]  # read the flags

            start_date = Convert2Shp.__get_utc_date(nvi.getncattr("mbStartDate"), nvi.getncattr("mbStartTime"))
            end_date = Convert2Shp.__get_utc_date(nvi.getncattr("mbEndDate"), nvi.getncattr("mbEndTime"))

            # Populate polyline
            # Populate polyline
            if len(lat) > 1:
                for lg, lt, flag in zip(lon, lat, validity_flag):
                    # keep only valid points
                    if flag == b"\x02":
                        polyline.AddPoint(lg, lt)
                # set fields value
                feature.SetField(FILE_FIELD, path.basename(filein))
                feature.SetField(PROFILE_FIELD, path.splitext(path.basename(filein))[0])
                feature.SetField(START_DATE_FIELD, start_date.strftime("%d/%m/%Y"))
                feature.SetField(END_DATE_FIELD, end_date.strftime("%d/%m/%Y"))
                feature.SetField(START_TIME_FIELD, start_date.strftime("%H:%M:%S"))
                feature.SetField(END_TIME_FIELD, end_date.strftime("%H:%M:%S"))
                count = polyline.GetPointCount()
                feature.SetField(START_LON_FIELD, polyline.GetPoint(0)[0])
                feature.SetField(START_LAT_FIELD, polyline.GetPoint(0)[1])
                feature.SetField(END_LON_FIELD, polyline.GetPoint(count - 1)[0])
                feature.SetField(END_LAT_FIELD, polyline.GetPoint(count - 1)[1])
                self.logger.info(f"Create {filein} geometry : {polyline.GetPointCount()} points")

            else:
                self.logger.warning(f"Skip file {filein} having {len(lat)} points")

    def __call__(self):
        """
        Main method : converts NVI/MBG to a shape file.
        """
        # shape file driver
        shp_driver = ogr.GetDriverByName("ESRI Shapefile")
        self.logger = log.logging.getLogger("Convert2Shp")

        # data source
        if path.exists(self.output_file):
            if self.overwrite:
                shp_driver.DeleteDataSource(self.output_file)
            else:
                print("Error: file already exists: " + str(self.output_file))
                raise IOError("Error: file already exists: " + str(self.output_file))

        data_source = shp_driver.CreateDataSource(self.output_file)

        if data_source is None:
            print("Error while opening file " + str(self.output_file))
            raise IOError("Error while opening file " + str(self.output_file))

        # spatial reference, WGS84
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)

        # creation layer
        basename, extension = path.splitext(self.output_file)
        layer_name = path.split(basename)[-1]
        layer = data_source.CreateLayer("NAV", srs, ogr.wkbLineString)

        if layer is None:
            msg = f"Could not create layer {layer_name}"
            raise Exception(msg)

        # create fields
        layer.CreateField(ogr.FieldDefn(FILE_FIELD, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(PROFILE_FIELD, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(START_DATE_FIELD, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(END_DATE_FIELD, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(START_TIME_FIELD, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(END_TIME_FIELD, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(START_LON_FIELD, ogr.OFTReal))
        layer.CreateField(ogr.FieldDefn(START_LAT_FIELD, ogr.OFTReal))
        layer.CreateField(ogr.FieldDefn(END_LON_FIELD, ogr.OFTReal))
        layer.CreateField(ogr.FieldDefn(END_LAT_FIELD, ogr.OFTReal))
        layer.CreateField(ogr.FieldDefn(CAMPAIGN_NAME, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(CAMPAIGN_NUM, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(NAVIGATION, ogr.OFTString))
        layer.CreateField(ogr.FieldDefn(TOOL, ogr.OFTString))

        # log constant fields
        print("Exporting to shape file : ", self.output_file)
        for field_name, field_value in self.added_const_fields.items():
            print(field_name, ":", str(field_value))

        for filein in self.input_files:
            polyline = ogr.Geometry(ogr.wkbLineString)
            feature = ogr.Feature(layer.GetLayerDefn())

            # fill polyline and fields (abstract method specific for each input file type)
            if filein.endswith(".mbg"):
                self.fill_polyline_and_fields_from_mbg(filein, polyline, feature)
            elif filein.endswith(".nvi"):
                self.fill_polyline_and_fields_from_nvi(filein, polyline, feature)
            else:
                raise IOError("Input file not managed : " + filein)

            # sets constant fields
            for field_name, field_value in self.added_const_fields.items():
                feature.SetField(field_name, str(field_value))

            feature.SetGeometry(polyline)
            # Create the feature in the layer (shape file)
            layer.CreateFeature(feature)



if __name__ == "__main__":
    #Main method (entry point)
    app_util.launch_application(app_util.get_json_configuration_file(__file__), Convert2Shp)
