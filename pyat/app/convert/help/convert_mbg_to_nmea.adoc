:last-update-label!:

== MBG -> NMEA

=== Overview
This application takes MBG file(s) as input and will export it (or them) as Techsas like nmead file(s) in containing vertical depth data.
