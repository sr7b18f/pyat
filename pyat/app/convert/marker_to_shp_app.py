#! /usr/bin/env python3
# coding: utf-8

import pyat.app.application_utils as app_util
from pyat.core.csv.marker_to_shape_exporter import MarkersToShapefileExporter

if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), MarkersToShapefileExporter)
