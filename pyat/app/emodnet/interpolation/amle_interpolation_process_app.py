#! /usr/bin/env python3
# coding: utf-8


import pyat.app.application_utils as app_util
import pyat.core.dtm.interpolation.amle_interpolation_process as amle
from abstract_interpolation_process import InterpolationProcessAdapter


class AmleInterpolationProcessAdapter(InterpolationProcessAdapter):
    """
    Adapts an instance of AMLEInterpolationProcess to launch a AMLE interpolation on DTMs.
    """

    def __init__(self, **kwargs):
        super().__init__(amle.AMLEInterpolationProcess(**kwargs), **kwargs)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), AmleInterpolationProcessAdapter)
