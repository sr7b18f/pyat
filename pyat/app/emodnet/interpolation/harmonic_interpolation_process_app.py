#! /usr/bin/env python3
# coding: utf-8


import pyat.app.application_utils as app_util
import pyat.core.dtm.interpolation.harmonic_interpolation_process as harmonic
from abstract_interpolation_process import InterpolationProcessAdapter


class HarmonicInterpolationProcessAdapter(InterpolationProcessAdapter):
    """
    Adapts an instance of HarmonicInterpolationProcess to launch a Harmonic interpolation on DTMs.
    """

    def __init__(self, **kwargs):
        super().__init__(harmonic.HarmonicInterpolationProcess(**kwargs), **kwargs)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), HarmonicInterpolationProcessAdapter)
