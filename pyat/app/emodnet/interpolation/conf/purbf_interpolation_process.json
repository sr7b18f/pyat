{
    "name": "PURBF interpolation (Coronis)",
    "file": "app/emodnet/interpolation/purbf_interpolation_process_app.py",
    "group": "Dtm/Interpolation",
    "help": "app/emodnet/interpolation/help/purbf_interpolation_process.html",
    "parameters": [
        {
            "name": "i_paths",
            "key": "-i",
            "long_key": "--i_paths",
            "nargs": "+",
            "help": "Input file list (*.nc)",
            "type": "infile(content_type=DTM_NETCDF_4)"
        },
        {
            "name": "o_paths",
            "key": "-o",
            "long_key": "--o_paths",
            "nargs": "+",
            "help": "Output file list (*.nc)",
            "type": "outfile(content_type=DTM_NETCDF_4,suffix=purbf_interpolated)"
        },
        {
            "name": "overwrite",
            "key": "-ow",
            "long_key": "--overwrite",
            "help": "Allow overwrite file if already exists.",
            "type": "outfile#overwrite"
        },
        {
            "name": "areas",
            "key": "-areas",
            "help": "KML file containing the areas that will be interpolated",
            "type": "file(content_type=KML_XML)",
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "query_block_size",
            "type": "int",
            "default": 1000,
            "key": "-query_block_size",
            "help": "Apply the interpolant using maximum this number of points at a time to avoid large memory consumption.",
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "rbf_distance_type",
            "type": "string",
            "key": "-rbf_distance_type",
            "choices": [
                "euclidean",
                "haversine",
                "vincenty"
            ],
            "help": "Distance type",
            "default": "euclidean",
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "rbf_type",
            "type": "string",
            "key": "-rbf_type",
            "choices": [
                "linear",
                "cubic",
                "quintic",
                "gaussian",
                "multiquadric",
                "green",
                "regularized",
                "tension",
                "thinplate",
                "wendland"
            ],
            "help": "RBF type",
            "default": "thinplate",
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "rbf_epsilon",
            "type": "float",
            "key": "-rbf_epsilon",
            "help": "Epsilon parameter of the RBF. Please check each RBF documentation for its meaning. Required just for the following RBF types: gaussian, multiquadric, regularized, tension, wendland",
            "default": 1.0,
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "rbf_regularization",
            "type": "float",
            "key": "-rbf_regularization",
            "help": "Regularization scalar to use while creating the RBF interpolant (optional)",
            "default": 0.0,
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "rbf_polynomial_degree",
            "type": "int",
            "key": "-rbf_polynomial_degree",
            "choices": [
                "-1",
                "0",
                "1",
                "2",
                "3"
            ],
            "help": "Degree of the global polynomial fit used in the RBF formulation. Valid: -1 (no polynomial fit), 0 (constant), 1 (linear), 2 (quadric), 3 (cubic)",
            "default": 1,
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "pu_overlap",
            "type": "float",
            "key": "-pu_overlap",
            "help": "Overlap factor between circles in neighboring sub-domains in the partition. The radius of a QuadTree cell, computed as half its diagonal, is enlarged by this factor",
            "default": 0.25,
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "pu_min_point_in_cell",
            "type": "int[1,2147483647]",
            "key": "-pu_min_point_in_cell",
            "help": "Minimum number of points in a QuadTree cell",
            "default": 1000,
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "pu_min_cell_size_percent",
            "type": "float",
            "key": "-pu_min_cell_size_percent",
            "help": "Minimum cell size, specified as a percentage [0..1] of the max(width, height) of the query domain",
            "default": 0.005,
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "pu_overlap_increment",
            "type": "float",
            "key": "-pu_overlap_increment",
            "help": "If, after creating the QuadTree, a cell contains less than pu_min_point_in_cell, the radius will be iteratively incremented until this condition is satisfied. This parameter specifies how much the radius of a cell increments at each iteration",
            "default": 0.001,
            "page": "Partition of Unity Radial Basis Function interpolation parameters"
        },
        {
            "name": "verbose",
            "type": "bool",
            "key": "-verbose",
            "help": "Verbosity flag, activate it to have feedback of the current steps of the process in the command line.",
            "default": false,
            "page": "Feedback parameters"
        },
        {
            "name": "show",
            "type": "bool",
            "key": "-show",
            "help": "Show interpolation problem and results on screen.",
            "default": false,
            "page": "Feedback parameters"
        }
    ]
}