#! /usr/bin/env python3
# coding: utf-8


import pyat.app.application_utils as app_util
import pyat.core.dtm.interpolation.rbf_interpolation_process as rbf
from pyat.app.emodnet.interpolation.abstract_interpolation_process import InterpolationProcessAdapter


class RbfInterpolationProcessAdapter(InterpolationProcessAdapter):
    """
    Adapts an instance of RbfInterpolationProcess to launch a rbf interpolation on DTMs.
    """

    def __init__(self, **kwargs):
        super().__init__(rbf.RbfInterpolationProcess(**kwargs), **kwargs)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), RbfInterpolationProcessAdapter)
