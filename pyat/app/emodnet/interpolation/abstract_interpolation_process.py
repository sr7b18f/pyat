#! /usr/bin/env python3
# coding: utf-8

from abc import ABC
from typing import List, Optional

from pyat.core.dtm.interpolation.heightmap_interpolation_process import HeightmapInterpolationProcess
from pyat.core.dtm.interpolation.interpolation_process import interpolate_dtms
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor


class InterpolationProcessAdapter(ABC):
    """
    Callable used by application utils to perform an interpolation on DTMs.
    """

    def __init__(
        self,
        interpolation_process_delegate: HeightmapInterpolationProcess,
        i_paths: List,
        o_paths: List,
        areas: Optional[str] = None,
        overwrite: bool = False,
        monitor: ProgressMonitor = DefaultMonitor,
        **kwargs,
    ):
        # Call method
        self.interpolates = interpolate_dtms(
            i_paths,
            o_paths,
            lambda i_path, o_path: interpolation_process_delegate.interpolates(i_path, o_path),
            overwrite,
            areas,
            monitor,
        )

    def __call__(self) -> None:
        """
        Simply call the interpolates method defined in the constructor
        """
        self.interpolates
