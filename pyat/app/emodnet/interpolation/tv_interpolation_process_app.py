#! /usr/bin/env python3
# coding: utf-8


import pyat.app.application_utils as app_util
import pyat.core.dtm.interpolation.tv_interpolation_process as tv
from abstract_interpolation_process import InterpolationProcessAdapter


class TvInterpolationProcessAdapter(InterpolationProcessAdapter):
    """
    Adapts an instance of TVInterpolationProcess to launch a TV interpolation on DTMs.
    """

    def __init__(self, **kwargs):
        super().__init__(tv.TVInterpolationProcess(**kwargs), **kwargs)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), TvInterpolationProcessAdapter)
