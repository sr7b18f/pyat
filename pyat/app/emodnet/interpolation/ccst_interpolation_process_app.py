#! /usr/bin/env python3
# coding: utf-8


import pyat.app.application_utils as app_util
import pyat.core.dtm.interpolation.ccst_interpolation_process as ccst
from abstract_interpolation_process import InterpolationProcessAdapter


class CcstInterpolationProcessAdapter(InterpolationProcessAdapter):
    """
    Adapts an instance of CcstInterpolationProcess to launch a Harmonic interpolation on DTMs.
    """

    def __init__(self, **kwargs):
        super().__init__(ccst.CcstInterpolationProcess(**kwargs), **kwargs)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), CcstInterpolationProcessAdapter)
