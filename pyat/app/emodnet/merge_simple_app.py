#! /usr/bin/env python3
# coding: utf-8

import os
import pyat.app.application_utils as app_util
from pyat.core.dtm.emodnet.merge_simple_process import MergeSimpleProcess

if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), MergeSimpleProcess)
