import datetime
import shutil

import pyat.app.application_utils as app_util
import pyat.core.dtm.dtm_driver as dtm_driver
import pyat.core.dtm.emodnet.dtm_standard_constants as DtmConstants
import pyat.core.utils.argument_utils as arg_util
import pyat.core.utils.pyat_logger as log
from pyat.core.dtm.emodnet.heightmap_interpolation.apps import interpolate_netcdf4
from pyat.core.utils.monitor import DefaultMonitor


class HeighMapInterpolationProcess:
    def __init__(
        self,
        i_paths: list,
        o_paths: list = None,
        suffix="_interpolated",
        overwrite=False,
        mask: str = None,
        monitor=DefaultMonitor,
        **kwargs,
    ):
        self.i_paths = i_paths
        self.o_paths = o_paths
        self.suffix = suffix
        self.overwrite = overwrite
        self.mask = mask
        self.monitor = monitor
        self.params = kwargs

        self.class_name = self.__class__.__name__
        self.logger = log.logging.getLogger(self.class_name)

    def __call__(self) -> None:
        """Run the process."""

        try:
            # Check files
            arg_util.check_output_paths(self.i_paths, self.o_paths)

            self.logger.info(f"Starting to {self.class_name} with { ', '.join(self.i_paths)}")
            begin = datetime.datetime.now()
            self.monitor.set_work_remaining(len(self.i_paths))

            # Open files
            for ind, input_path in enumerate(self.i_paths):
                self.logger.info(f"Processing file {input_path}")
                # compute a mapping between arguments from globe and the ones expected by heighmap_interpolation_process
                output_path = arg_util.create_ouput_path(
                    input_path,
                    suffix=self.suffix,
                    overwrite=self.overwrite,
                    o_path=(None if not self.o_paths else self.o_paths[ind]),
                )
                # copy file to output path
                self.logger.info(f"Copying {input_path} to {output_path}")
                # prepare parameters
                arg = [output_path, "-v"]
                if not self.mask is None:
                    arg += ["--areas", self.mask]
                # parse remaining parameters
                if "rbf_type" in self.params:
                    arg += ["--rbf_type", self.params["rbf_type"]]
                if "rbf_distance_type" in self.params:
                    arg += ["--rbf_distance_type", self.params["rbf_distance_type"]]
                if "rbf_epsilon" in self.params:
                    arg += ["--rbf_epsilon", self.params["rbf_epsilon"]]
                if "rbf_regularization" in self.params:
                    arg += ["--rbf_regularization", self.params["rbf_regularization"]]
                if "rbf_polynomial_degree" in self.params:
                    arg += ["--rbf_polynomial_degree", self.params["rbf_polynomial_degree"]]
                if "rbf_max_ref_points" in self.params:
                    arg += ["--rbf_max_ref_points", self.params["rbf_max_ref_points"]]
                if "pu_min_point_in_cell" in self.params:
                    arg += ["--pu_min_point_in_cell", self.params["pu_min_point_in_cell"]]

                if "pu_overlap" in self.params:
                    arg += ["--pu_overlap", self.params["pu_overlap"]]

                if "pu_overlap_increment" in self.params:
                    arg += ["--pu_overlap_increment", self.params["pu_overlap_increment"]]
                if "pu_min_cell_size_percent" in self.params:
                    arg += ["--pu_min_cell_size_percent", self.params["pu_min_cell_size_percent"]]
                if "fill_missing_values" in self.params:
                    if self.params["fill_missing_values"]:
                        arg += ["-interpolate_missing_values"]
                else:
                    arg += ["-interpolate_missing_values"]  # default behabviour activate missing values

                shutil.copy(input_path, output_path)

                # add elevation layer if not already done
                self.logger.info(f"Creating layer {DtmConstants.INTERPOLATION_FLAG}")
                with dtm_driver.open_dtm(input_path) as i_driver, dtm_driver.open_dtm(output_path, "r+") as o_driver:
                    o_driver.create_missing_layer(
                        layer_name=DtmConstants.INTERPOLATION_FLAG,
                        elevation_reference=o_driver[DtmConstants.ELEVATION_NAME],
                    )
                    self.logger.info(f"Calling interpolation process...")

                    # now run interpolation
                    interpolate_netcdf4.interpolate(i_driver, o_driver, interpolate_netcdf4.parse_args(arg))

            end = datetime.datetime.now()
            self.logger.info(f"End of {self.class_name}, {end - begin} time elapsed.\n")

        except FileExistsError as e:
            self.logger.error(
                f"{e.filename} already exists and overwrite not allowed (allow overwrite with option: '-ow --overwrite)"
            )
        except Exception:
            self.logger.error("An exception was thrown!", exc_info=True)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), HeighMapInterpolationProcess)
