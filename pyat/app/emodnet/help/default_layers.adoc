:last-update-label!:

== Default layers process

=== Overview
This application takes DTM file(s) in new format, and create default layers for each missing
layer. List of layers which can be created:

* elevation / elevation_min / elevation_max / elevation_smoothed
* stdev
* cdi_index
* interpolation_flag
