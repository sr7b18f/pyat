#! /usr/bin/env python3
# coding: utf-8
import datetime
import os
import shutil
from typing import List

import pyat.app.application_utils as app_util
import pyat.core.sensor.navigation_file as navigation
import pyat.core.utils.pyat_logger as log
from pyat.core.techsas.pytechsas.sensor import techsas_file as techsas
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor


class TechsasFilesGeolocation:
    def __init__(
        self,
        i_paths: List[str],
        o_paths: List[str],
        i_nav_files: List[str],
        overwrite: bool = False,
        monitor: ProgressMonitor = DefaultMonitor,
        use_elevation: bool = False,
    ):
        """
        Constructor.
        """
        self.logger = log.logging.getLogger(TechsasFilesGeolocation.__name__)
        self.i_paths: List[str] = i_paths
        self.o_paths: List[str] = o_paths
        self.overwrite: bool = overwrite
        self.i_nav_files: List[str] = i_nav_files
        self.monitor = monitor
        self.use_elevation = use_elevation
        self.nav_data = None

    def __call__(self):
        self.monitor.set_work_remaining(len(self.i_paths))

        for i_path, o_path in zip(self.i_paths, self.o_paths):
            try:
                # generate o_path
                if os.path.getsize(i_path) <= 0:
                    self.logger.warning(f"File {i_path} is empty, skipping it")
                    continue

                # Copy input file to output path"
                if self.overwrite or not os.path.exists(o_path):
                    shutil.copy(i_path, o_path)
                else:
                    self.logger.warning(f"File {o_path} already exists, skipping it")
                    continue

                # generate_data
                interpolated_longitudes, interpolated_latitudes, interpolated_altitudes = self.generate_data(
                    techsas_file=o_path
                )
                self.logger.info(f"Apply interpolated navigation to: {o_path}")
                if not self.use_elevation:
                    interpolated_altitudes = None

                # add navigation variables in the techsas file
                techsas.add_navigation_to_techsas_file(
                    use_elevation=self.use_elevation,
                    interpolated_altitudes=interpolated_altitudes,
                    interpolated_latitudes=interpolated_latitudes,
                    interpolated_longitudes=interpolated_longitudes,
                    techsas_file=o_path,
                )
                self.logger.info(f"Geolocalisation done for file {i_path} (output = {o_path})")
            except Exception as e:
                self.logger.error(f"An exception was thrown : {str(e)}", exc_info=True, stack_info=True)
            finally:
                self.monitor.worked(1)

        self.monitor.done()

    def read_navigation_data(self):
        """
        get the navigation time vector from the techsas file giver (techsas_file) and the navigation files.
        Interpolate navigation and return
        """
        if self.nav_data is None:
            self.nav_data = navigation.aggregate_navigation_files(self.i_nav_files)
        return self.nav_data

    def generate_data(self, techsas_file):
        """
        get the navigation time vector from the techsas file giver (techsas_file) and the navigation files.
        Interpolate navigation and return
        """
        # read techsas file
        self.logger.info(f"Interpolate navigation data for {techsas_file}")
        techsas_time_values, dictionary, frame_period = techsas.read_techsas_file(techsas_file)

        # read navigation files
        nav_data = self.read_navigation_data()

        interpolated_longitudes, interpolated_latitudes, interpolated_altitudes = navigation.interpolate_navigation(
            time_navigation=nav_data.times,
            latitudes=nav_data.latitudes,
            longitudes=nav_data.longitudes,
            altitudes=nav_data.altitudes,
            time_sensor=techsas_time_values,
        )
        return interpolated_longitudes, interpolated_latitudes, interpolated_altitudes


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), TechsasFilesGeolocation)
