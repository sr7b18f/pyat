:last-update-label!:

== Navigation tide

=== Overview
This processus computes an estimated tide based on navigation sensor data. Sensor data (gps) are first converted relatively to a reference surface (LAT or ZeroHydro) then a lowpass filter is applied to generate the estimated tide.


=== Parameters
* model_dir : a directory where MSL/LAT models are available (containing ./fes2014, ./reference_surfaces, ./BATHYELLI_ZH_ell_V2, ...)
* input_files : navigation files containing datapoints
* output_file : a file name where results will be stores
* positioning_type_filter : filtering out non-RTK values
* reference_surface : compute tide relative to LAT(FES2014) or ZeroHydro(BathyElli/SHOM)
* predictive_mode : use FES2014 prediction model to improve lowpass filtering and interpolation of navigation data

include::tide_filtering.adoc[]

=== Tide I/O file formats
include::tide_output_file_format.adoc[]

include::tide_processing.adoc[]
