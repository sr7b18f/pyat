#! /usr/bin/env python3
# coding: utf-8

import tempfile

import netCDF4 as nc
import numpy as np
import pyproj
import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as sg


class XsfGenerator:
    """Class generator of XSF file for the processes tests."""

    def __init__(self, folder=tempfile.tempdir):
        self.folder = folder

    def initialize_file(
        self,
        latitude_min_deg: float,
        latitude_max_deg: float,
        longitude_min_deg: float,
        longitude_max_deg: float,
        ping_count: int,
        beam_count: int,
        min_depth_m: float,
        max_depth_m: float,
    ) -> str:
        """
        Generates a XSF file with
            As many navigation positions as "ping_count"
            "beam_count" beams on both sides of the navigation positions
            Configured Layers in such a way that depths are the same in FCS and SCS coordinates system
        """
        result = tempfile.mktemp(suffix=".xsf.nc", dir=self.folder)
        with nc.Dataset(result, "w", format="NETCDF4") as dataset:
            root_structure = sg.RootGrp()
            root = root_structure.create_group(dataset)

            # create /annotation
            ano_structure = sg.AnnotationGrp()
            ano = ano_structure.create_group(root)
            ano_structure.create_dimension(ano, {sg.AnnotationGrp.TIME_DIM_NAME: 2})
            ano_structure.create_time(ano, long_name="ANO_TIME")
            ano_structure.create_annotation_text(ano)

            # create /Sonar
            sonar_structure = sg.SonarGrp()
            sonar = sonar_structure.create_group(root)

            # create group /Sonar/Beam_group1
            beam_structure = sg.BeamGroup1Grp()
            beam_group1 = beam_structure.create_group(sonar, ident="Beam_group1")
            beam_structure.create_beam_type(beam_group1)
            beam_structure.create_dimension(beam_group1, {sg.BeamGroup1Grp.PING_TIME_DIM_NAME: ping_count})
            beam_structure.create_dimension(beam_group1, {sg.BeamGroup1Grp.BEAM_DIM_NAME: beam_count})

            platform_latitude = beam_structure.create_platform_latitude(beam_group1)
            platform_latitude[:] = np.linspace(latitude_min_deg, latitude_max_deg, num=ping_count).reshape(
                1, ping_count
            )

            platform_longitude = beam_structure.create_platform_longitude(beam_group1)
            platform_longitude[:] = np.linspace(longitude_min_deg, longitude_max_deg, num=ping_count).reshape(
                1, ping_count
            )

            heading = self.compute_heading(
                latitude_min_deg,
                latitude_max_deg,
                longitude_min_deg,
                longitude_max_deg,
            )
            platform_heading = beam_structure.create_platform_heading(beam_group1)
            platform_heading[:] = np.full((1, ping_count), heading)

            ping_time = beam_structure.create_ping_time(beam_group1)
            ping_time[:] = np.linspace(1359898806964000000, 1359898948064000000, num=ping_count, dtype="i8")
            platform_vertical_offset = beam_structure.create_platform_vertical_offset(beam_group1)
            platform_vertical_offset[:] = np.zeros((1, ping_count))
            waterline_to_chart_datum = beam_structure.create_waterline_to_chart_datum(beam_group1)
            waterline_to_chart_datum[:] = np.zeros((1, ping_count))
            tx_transducer_depth = beam_structure.create_tx_transducer_depth(beam_group1)
            tx_transducer_depth[:] = np.random.default_rng().random((1, ping_count)) * 2.0 + 1.0

            bathymetry_structure = sg.BathymetryGrp()
            bathymetry = bathymetry_structure.create_group(beam_group1, ident="Bathymetry")
            bathymetry_structure.create_dimension(bathymetry, {sg.BathymetryGrp.DETECTION_DIM_NAME: beam_count})
            detection_x = bathymetry_structure.create_detection_x(bathymetry)
            detection_x[:] = np.linspace(2.0, 5.0, num=beam_count)
            detection_y = bathymetry_structure.create_detection_y(bathymetry)
            detection_y[:] = np.linspace(-150, 150, num=beam_count)
            detection_z = bathymetry_structure.create_detection_z(bathymetry)
            detection_z[:] = np.linspace(min_depth_m, max_depth_m, num=beam_count)

            lons, lats = self.compute_beam_positions(
                beam_count,
                platform_longitude[:],
                platform_latitude[:],
                detection_y[:],
                heading,
            )
            detection_longitude = bathymetry_structure.create_detection_longitude(bathymetry)
            detection_longitude[:] = lons
            detection_latitude = bathymetry_structure.create_detection_latitude(bathymetry)
            detection_latitude[:] = lats
            create_detection_rx_transducer_index = bathymetry_structure.create_detection_rx_transducer_index(bathymetry)
            create_detection_rx_transducer_index[:] = np.zeros((ping_count, beam_count))

            status = bathymetry_structure.create_status(bathymetry)
            status[:] = np.zeros((ping_count, beam_count))
            status_detail = bathymetry_structure.create_status_detail(bathymetry)
            status_detail[:] = np.zeros((ping_count, beam_count))
            detection_backscatter_r = bathymetry_structure.create_detection_backscatter_r(bathymetry)
            detection_backscatter_r[:] = np.linspace(-26.0, -31.0, num=beam_count)
            detection_beam_pointing_angle = bathymetry_structure.create_detection_beam_pointing_angle(bathymetry)
            detection_beam_pointing_angle[:] = np.linspace(-40.0, 40.0, num=beam_count)

            platform_structure = sg.PlatformGrp()
            platform = platform_structure.create_group(root)
            platform_structure.create_dimension(platform, {sg.PlatformGrp.TRANSDUCER_DIM_NAME: 5})
            transducer_function = platform_structure.create_transducer_function(platform)
            transducer_function[:] = [0, 0, 1, 1, 1]
            transducer_offset_x = platform_structure.create_transducer_offset_x(platform)
            transducer_offset_x[:] = [4.239, 4.24, 4.223, 4.223, 4.223]
            transducer_offset_y = platform_structure.create_transducer_offset_y(platform)
            transducer_offset_y[:] = [-0.372, 0.389, -0.0454, 0.02315, 0.0654]
            transducer_offset_z = platform_structure.create_transducer_offset_z(platform)
            transducer_offset_z[:] = [1.646, 1.644, 1.723, 1.729, 1.723]
            transducer_rotation_x = platform_structure.create_transducer_rotation_x(platform)
            transducer_rotation_x[:] = [39.79, -39.86, -0.08, -0.08, -0.08]
            transducer_rotation_y = platform_structure.create_transducer_rotation_y(platform)
            transducer_rotation_y[:] = [-0.02, -0.26, -0.04, -0.04, -0.04]
            transducer_rotation_z = platform_structure.create_transducer_rotation_z(platform)
            transducer_rotation_z[:] = [0.22, -0.91000366, -1.6099854, -1.6099854, -1.6099854]

            # create group /Platform/Dynamic_draught
            dynamic_draught_structure = sg.DynamicDraughtGrp()
            dynamic_draught = dynamic_draught_structure.create_group(root)
            dynamic_draught_structure.create_dimension(
                dynamic_draught, {sg.DynamicDraughtGrp.TIME_DIM_NAME: ping_count}
            )
            dynamic_draught_time = dynamic_draught_structure.create_time(dynamic_draught)
            dynamic_draught_time[:] = ping_time[:]
            dynamic_draught_structure.create_delta_draught(dynamic_draught)

            # create group /Environment/Tide/
            tide_structure = sg.TideGrp()
            tide = tide_structure.create_group(root)
            tide_structure.create_dimension(tide, {sg.TideGrp.TIME_DIM_NAME: ping_count})
            tide_time = tide_structure.create_time(tide)
            tide_time[:] = ping_time[:]
            tide_structure.create_tide_indicative(tide)

        return result

    def compute_heading(
        self, latitude_min_deg: float, latitude_max_deg: float, longitude_min_deg: float, longitude_max_deg: float
    ):
        geodesic = pyproj.Geod(ellps="WGS84")
        fwd_azimuth, back_azimuth, distance = geodesic.inv(
            longitude_min_deg, latitude_min_deg, longitude_max_deg, latitude_max_deg
        )
        return fwd_azimuth

    def compute_beam_positions(
        self,
        beam_count: int,
        platform_longitude: np.ndarray,
        platform_latitude: np.ndarray,
        detection_y: np.ndarray,
        heading: float,
    ):
        ping_count = platform_longitude.shape[0]
        lons = np.ndarray((ping_count, beam_count), dtype=float)
        lons[:] = platform_longitude.reshape((ping_count, 1))

        lats = np.ndarray((ping_count, beam_count), dtype=float)
        lats[:] = platform_latitude.reshape((ping_count, 1))

        az = np.full((ping_count, beam_count), heading + 90.0)

        geodesic = pyproj.Geod(ellps="WGS84")
        lons, lats, az = geodesic.fwd(lons, lats, az, detection_y)
        return lons, lats


if __name__ == "__main__":
    generator = XsfGenerator()

    print(
        generator.initialize_file(
            latitude_min_deg=48.0,
            latitude_max_deg=48.005,
            longitude_min_deg=-4.004,
            longitude_max_deg=-4.0,
            ping_count=2,
            beam_count=2,
            min_depth_m=10.0,
            max_depth_m=20.0,
        )
    )
