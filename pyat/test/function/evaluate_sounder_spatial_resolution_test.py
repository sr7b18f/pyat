#! /usr/bin/env python3
# coding: utf-8
import numpy as np
import pytest

from pyat.core.function.evaluate_sounder_spatial_resolution import SpatialResolutionEvaluator

MBG_PATH = "data/external/pyat_test_file/mbg/0136_20120607_083636_ShipName_ref.mbg"


def test_evaluate_spatial_resolution_mbg():
    """
    Evaluates the resolution spatial of a MBG
    """
    evaluator = SpatialResolutionEvaluator(i_paths=[MBG_PATH])
    res_meter, res_degree = evaluator()
    assert res_meter == pytest.approx(11)
    assert res_degree == pytest.approx(1.363979e-4, abs=1e-7)


test_evaluate_spatial_resolution_mbg()
