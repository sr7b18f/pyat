import sys

import os

from pyat.core.utils import process
from pyat.core.utils import pyat_logger


def test_process_with_exception():

    logger = pyat_logger.logging.getLogger(__name__)
    value = process.run_and_log([r"C:\Windows\System32\netstat2.exe", "-e"], logger)
    assert value < 0

def test_process_without_exception():
    logger = pyat_logger.logging.getLogger(__name__)
    if sys.platform == "win32":
        value = process.run_and_log([r"C:\Windows\System32\netstat2.exe", "-e"], logger)
    else:
        value = process.run_and_log([r"/usr/bin/ls"], logger)
    assert value < 0
