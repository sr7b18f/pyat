#! /usr/bin/env python3
# coding: utf-8

import os
import shutil
import tempfile as tmp

import numpy as np

import pyat.core.dtm.dtm_driver as dtm_driver
import pyat.core.dtm.emodnet.dtm_standard_constants as DTM
import pyat.test.generator.dtm_generator as dtm_generator
import pyat.test.generator.kml_generator as kml_generator
from pyat.core.dtm.emodnet.heightmap_interpolation.apps import interpolate_netcdf4


def make_dtm(temp_dir: str) -> str:
    """
    Generate a DTM with 2 holes
    Cell's longitude = [-30°, -29.944444°..., -29.5°],
    Cell's latitude = [39.5°, 39.555555°, ..., 40°]
    Resolution = 200'' (0,055555°)
    """
    # Generations from -90 to -100
    elevations = 10 * np.random.default_rng().random((10, 10)) - 100
    elevations[2, 0] = np.nan
    elevations[9, 8] = np.nan
    # 2 holes ?
    assert np.count_nonzero(np.isnan(elevations)) == 2

    cdi_index = np.full_like(elevations, dtm_driver.get_missing_value(DTM.CDI_INDEX))
    cdi_index[1, 0] = 0
    cdi_index[3, 0] = 0
    cdi_index[2, 1] = 1
    cdi_index[9, 7] = 0
    cdi_index[9, 9] = 1
    cdi_index[8, 8] = 1
    return dtm_generator.make_dtm_with_data(
        (-30.0, 40.0), (-29.5, 39.5), {DTM.ELEVATION_NAME: elevations, DTM.CDI_INDEX: cdi_index}, temp_dir
    )


def test_nominal_heighmap_interpolation():
    """
    Nominal test for interpolate_netcdf4 module.
    """
    with tmp.TemporaryDirectory() as temp_dir:
        path_i_dtm = make_dtm(temp_dir)
        path_o_dtm = tmp.mktemp(suffix=".dtm.nc", dir=temp_dir)
        shutil.copy(path_i_dtm, path_o_dtm)
        with dtm_driver.open_dtm(path_i_dtm) as i_driver, dtm_driver.open_dtm(path_o_dtm, "r+") as o_driver:
            arg = [path_o_dtm, "-v"]
            interpolate_netcdf4.interpolate(i_driver, o_driver, interpolate_netcdf4.parse_args(arg))
            # Always 2 holes in input file
            assert np.count_nonzero(np.isnan(i_driver[DTM.ELEVATION_NAME][:])) == 2
            # No more hole in ouput file
            assert np.count_nonzero(np.isnan(o_driver[DTM.ELEVATION_NAME][:])) == 0


def test_mask_heighmap_interpolation():
    """
    Test of interpolate_netcdf4 module with geo mask.
    """
    with tmp.TemporaryDirectory() as temp_dir:
        # Generate a KML, covering the northern half, over 1 hole
        coord = [[-31.0, 41.0], [-28.0, 41.0], [-28.0, 39.75], [-31.0, 39.75]]
        kml_path = kml_generator.create_kml(temp_dir, {"zone": coord})

        path_i_dtm = make_dtm(temp_dir)
        path_o_dtm = tmp.mktemp(suffix=".dtm.nc", dir=temp_dir)
        shutil.copy(path_i_dtm, path_o_dtm)
        with dtm_driver.open_dtm(path_i_dtm) as i_driver, dtm_driver.open_dtm(path_o_dtm, "r+") as o_driver:
            arg = [path_o_dtm, "-v"]
            arg += ["--areas", kml_path]
            interpolate_netcdf4.interpolate(i_driver, o_driver, interpolate_netcdf4.parse_args(arg))
            # Always 2 holes in input file
            assert np.count_nonzero(np.isnan(i_driver[DTM.ELEVATION_NAME][:])) == 2
            # Only one hole left
            assert np.count_nonzero(np.isnan(o_driver[DTM.ELEVATION_NAME][:])) == 1


def test_cdi_heighmap_interpolation():
    """
    Test of interpolate_netcdf4 module with geo mask.
    """
    with tmp.TemporaryDirectory() as temp_dir:
        path_i_dtm = make_dtm(temp_dir)
        path_o_dtm = tmp.mktemp(suffix=".dtm.nc", dir=temp_dir)
        shutil.copy(path_i_dtm, path_o_dtm)
        with dtm_driver.open_dtm(path_i_dtm) as i_driver, dtm_driver.open_dtm(path_o_dtm, "r+") as o_driver:
            arg = [path_o_dtm, "-v"]
            interpolate_netcdf4.interpolate(i_driver, o_driver, interpolate_netcdf4.parse_args(arg))
            # Expecting CDI 0 at [2, 2] because it is the most encountered value around this point
            assert o_driver[DTM.CDI_INDEX][2, 0] == 0
            # Expecting CDI 1 at [1, 1] because it is the most encountered value around this point
            assert o_driver[DTM.CDI_INDEX][9, 8] == 1
