import numpy as np
from scipy.interpolate import interp1d

import pyat.core.utils.tiff_driver as tiff
import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as XConstants
from playground.stage.utils.nc_file_cache import DetectionProjectionCache
from playground.stage.utils.nc_file_cache import ShipProjectionCache
from playground.stage.utils.workspace_cache import WorkArea_Cache


def get_normal_xsf(detectionX, detectionY, detectionZ):
    normal_z = np.gradient(detectionZ)
    dzx = normal_z[0]
    dzy = normal_z[1]
    x_res = (np.nanmax(detectionX) - np.nanmin(detectionX)) / detectionX.shape[0]
    y_res = (np.nanmax(detectionY) - np.nanmin(detectionY)) / detectionY.shape[1]
    dx = y_res * dzx
    dy = x_res * dzy
    dz = x_res * y_res
    norm = np.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
    dx = dx / norm
    dy = dy / norm
    return dx, dy


class InsonifiedAreaComputer:
    def __init__(self, incidence_computer_work_area, files, output_directory):
        self.work_area = incidence_computer_work_area
        self.files = files
        self.output_directory = output_directory
        self.kongsberg_area, self.ifremer_area = 0, 0

    def get_area(self, xsf_file, incidence_angle):
        """
        Compute insonified area, formula used : https://archimer.ifremer.fr/doc/00429/54016/57434.pdf
        :param xsf_file: a single xsf file
        :param incidence_angle: array of incidence angle
        :return: Array from konsberg & ifremer
        """
        # Get ship and beam position
        # Also get all variables necessary
        shipX, shipY = self.work_area.cache.ship_position_projected[xsf_file].getXY()
        detectionX, detectionY = self.work_area.cache.projected_detection[xsf_file].getXY()
        detectionZ = xsf_file.get_as_detection_masked_array(
            XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_Z
        )
        detectionX, detectionY, detectionZ = xsf_file.takeoff_offset_transducer(detectionX, detectionY, detectionZ)
        beam_along_width = xsf_file.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BEAMWIDTH_TRANSMIT_MINOR)
        beam_along_width = beam_along_width[:, 1].reshape((beam_along_width.shape[0], 1))
        beam_across_width = xsf_file.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BEAMWIDTH_RECEIVE_MAJOR)
        depth_sound = xsf_file.get_as_array(XConstants.RootGrp.EnvironmentGrp.SoundSpeedProfileGrp.DEPTH)
        sound_speed = xsf_file.get_as_array(XConstants.RootGrp.EnvironmentGrp.SoundSpeedProfileGrp.SOUND_SPEED)
        transmit_duration = xsf_file.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.TRANSMIT_DURATION_NOMINAL)
        transmit_duration = transmit_duration[:, 1].reshape((transmit_duration.shape[0], 1))
        pointing_angle = xsf_file.get_as_detection_masked_array(
            XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_BEAM_POINTING_ANGLE
        )

        # Get normal to the xsf
        dx, dy = get_normal_xsf(detectionX, detectionY, detectionZ)
        f = interp1d(depth_sound[0, :], sound_speed[0, :])
        speed_bottom = f(detectionZ.filled(np.nan))
        speed_top = sound_speed[0][0]
        # Calculate Range
        beamX = shipX.reshape((shipX.size, 1)) - detectionX  # = detectionX_tilt
        beamY = shipY.reshape((shipY.size, 1)) - detectionY  # = detectionY_tilt
        R = np.sqrt(beamX ** 2 + beamY ** 2 + detectionZ ** 2)
        # Calculate across and along angle
        slope_across = np.degrees(np.pi / 2 - np.arccos(dx ** 2 / dx))
        slope_along = np.degrees(np.pi / 2 - np.arccos(dy ** 2 / dy))
        # Snell-Descartes
        incidence_bottom = np.degrees(np.arcsin(speed_bottom * np.sin(np.radians(pointing_angle)) / speed_top))
        incidence_across = incidence_bottom - slope_across
        # Minimum range
        rN = np.nanmin(np.sqrt(beamX ** 2 + beamY ** 2 + detectionZ ** 2))

        # Konsberg Formula
        # A0 = Beam area, A1 = Under the ship area
        A0 = (
            R
            * np.abs(np.sin(np.radians(incidence_across)))
            * (np.sqrt(1 + (speed_bottom * transmit_duration / (R * np.sin(np.radians(incidence_across)) ** 2))) - 1)
        )
        A1 = R * np.radians(beam_across_width) / np.cos(np.radians(incidence_angle)) / np.cos(np.radians(slope_across))
        # Ifremer Correction
        # w0 = Beam area, wN = Under the ship area
        w0, wN = self.compute_Ifremer_area(beam_along_width, R, speed_bottom, transmit_duration, rN, beam_across_width)
        # Return the min between A0 & A1 / w0 & wN
        kongsberg_area, ifremer_area = self.compute_area_min(A0, A1, w0, wN)
        kongsberg_area = 10 * np.log10(
            R * np.radians(beam_along_width) / np.cos(np.radians(slope_along)) * kongsberg_area
        )
        return kongsberg_area, ifremer_area

    def compute_Ifremer_area(
        self, beam_along_width, R, speed_bottom, transmit_duration, rN, beam_across_width,
    ):
        w0 = np.radians(beam_along_width) * R * speed_bottom * transmit_duration / (2 * np.sqrt(1 - (rN ** 2 / R ** 2)))
        wN = np.radians(beam_across_width) * np.radians(beam_along_width) * R ** 2
        return w0, wN

    def compute_area_min(self, a0, a1, w0, wN):
        """
        Compute min between 2 arrays
        :param a0: Konsberg array of the beam
        :param a1: Konsberg array under the ship
        :param w0: Ifremer array of the beam
        :param wN: Ifremer array under the ship
        :return: min of array
        """
        min_area = np.zeros((w0.shape[0], w0.shape[1]))
        correction_area = np.zeros((w0.shape[0], w0.shape[1]))
        for i in range(w0.shape[0]):
            for j in range(w0.shape[1]):
                min_area[i][j] = min((a0[i][j], a1[i][j]))
                correction_area[i][j] = min(10 * np.log10(w0[i][j]), 10 * np.log10(wN[i][j]))
        return min_area, correction_area

    def compute_insonified_area(self, xsf_file, incidence_angle):
        """
        Compute the insonified area
        :param xsf_file: a single xsf file
        :param incidence_angle: array of incidence angle
        :return: a pingdetection matrix filled with insonified area in dB
        """
        # First we check if the projection is done, if not we do it
        if self.work_area is None:
            self.work_area = WorkArea_Cache(self.files, self.output_directory)
            self.work_area.define_projection()
            self.work_area.cache.projected_detection[xsf_file] = DetectionProjectionCache(
                xsf_file, self.work_area.projection
            )
            self.work_area.cache.ship_position_projected[xsf_file] = ShipProjectionCache(
                xsf_file, self.work_area.projection
            )
        incidence_angle = tiff.read_tiff(incidence_angle)
        # We call the function to get area
        self.kongsberg_area, self.ifremer_area = self.get_area(xsf_file, incidence_angle)
        # According to ridha, we add (or sub) the two area. Add seems to have the best result
        FinalArea = self.ifremer_area + self.kongsberg_area

        print("end computing insonified area for file " + xsf_file.file_name)
        x_UTM = self.work_area.cache.projected_detection[xsf_file]._x
        y_UTM = self.work_area.cache.projected_detection[xsf_file]._y
        return (
            x_UTM,
            y_UTM,
            self.work_area.cache.projected_detection[xsf_file].projection_proj4_def,
            FinalArea,
        )
