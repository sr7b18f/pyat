import glob

import numpy as np
from scipy.interpolate import interp1d

import pyat.core.utils.matrix_inspector as scope
import pyat.core.utils.tiff_driver as tiff
import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as XConstants
import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as constants
import playground.stage.utils.directory_settings as directories
from playground.stage.utils.nc_file_cache import DetectionProjectionCache
from playground.stage.utils.nc_file_cache import ShipProjectionCache
from playground.stage.utils.nc_file_cache import XSFFile
from playground.stage.utils.workspace_cache import WorkArea_Cache

file_mnt = "C:/Users/krodrigu/Documents/pyat/2019_11_04_10_51_42.tif"
filename = "W:/traitement_Campagnes/Stage_Kevin/backscatter/boite1/0032_20180914_183119_ShipName.xsf"
filename_incidence = "C:/Users/krodrigu/Documents/pyat/incidence_angle_2019_10_25_13_43_03.tif"

show_figure_immediately = False
file_names = glob.glob(directories.input_directory + "/backscatter/boite1/*.xsf")
# Récupère les données du xsf
files = []
for file_name in file_names:
    if "0047_20180914_222017_ShipName" not in file_name:
        files.append(XSFFile(file_name))

file = []
file.append(XSFFile(file_names[0]))
single_file = [value for value in files if value.file_name.find("0032_20180914_183119_ShipName") > 0]
single_file = single_file[0]

work_area = WorkArea_Cache(file, None)
work_area.define_projection()
work_area.cache.projected_detection[single_file] = DetectionProjectionCache(single_file, work_area.projection)
work_area.cache.ship_position_projected[single_file] = ShipProjectionCache(single_file, work_area.projection)

shipX, shipY = work_area.cache.ship_position_projected[single_file].getXY()
detectionX, detectionY = work_area.cache.projected_detection[single_file].getXY()
detectionZ = single_file.get_as_detection_masked_array(
    XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_Z
)
detectionX_tilt = single_file.get_as_detection_masked_array(
    XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_X
)
detectionY_tilt = single_file.get_as_detection_masked_array(
    XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_Y
)
detectionX, detectionY, detectionZ = single_file.takeoff_offset_transducer(detectionX, detectionY, detectionZ)

normal_z = np.gradient(detectionZ)

dzx = normal_z[0]
dzy = normal_z[1]
x_res = (np.nanmax(detectionX) - np.nanmin(detectionX)) / detectionX.shape[0]
y_res = (np.nanmax(detectionY) - np.nanmin(detectionY)) / detectionY.shape[1]
dx = y_res * dzx
dy = x_res * dzy
dz = x_res * y_res

norm = np.sqrt(dx ** 2 + dy ** 2 + dz ** 2)

dx = dx / norm
dy = dy / norm

beam_along_width = single_file.get_as_array(constants.RootGrp.SonarGrp.BeamGroup1Grp.BEAMWIDTH_TRANSMIT_MINOR)
beam_along_width = beam_along_width[:, 1].reshape((beam_along_width.shape[0], 1))
beam_across_width = single_file.get_as_array(constants.RootGrp.SonarGrp.BeamGroup1Grp.BEAMWIDTH_RECEIVE_MAJOR)

depth_sound = single_file.get_as_array(constants.RootGrp.EnvironmentGrp.SoundSpeedProfileGrp.DEPTH)
sound_speed = single_file.get_as_array(constants.RootGrp.EnvironmentGrp.SoundSpeedProfileGrp.SOUND_SPEED)
transmit_duration = single_file.get_as_array(constants.RootGrp.SonarGrp.BeamGroup1Grp.TRANSMIT_DURATION_NOMINAL)
transmit_duration = transmit_duration[:, 1].reshape((transmit_duration.shape[0], 1))
pointing_angle = single_file.get_as_detection_masked_array(
    constants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_BEAM_POINTING_ANGLE
)
incidence_angle = tiff.read_tiff(filename_incidence)

f = interp1d(depth_sound[0, :], sound_speed[0, :])
speed_bottom = f(detectionZ.filled(np.nan))
speed_top = sound_speed[0][0]

beamX = shipX.reshape((shipX.size, 1)) - detectionX  # = detectionX_tilt
beamY = shipY.reshape((shipY.size, 1)) - detectionY  # = detectionY_tilt

R = np.sqrt(beamX ** 2 + beamY ** 2 + detectionZ ** 2)

slope_along = np.degrees(np.pi / 2 - np.arccos(dx ** 2 / dx))
slope_across = np.degrees(np.pi / 2 - np.arccos(dy ** 2 / dy))

# scope.display_matrix(slope_along, "Slope Along", False)
# scope.display_matrix(slope_across, "Slope Across", True)
################################## SonarScope way ##################################
pointing_angle_theo = np.degrees(np.arcsin(speed_bottom * np.sin(np.radians(incidence_angle)) / speed_top))
tilt_angle = np.degrees(np.arctan2(detectionZ, detectionY_tilt)) - 90
incidence_across = pointing_angle_theo - slope_across
incidence_along = slope_along - tilt_angle

# https://archimer.ifremer.fr/doc/00429/54016/57434.pdf
A0 = (
    R
    * np.abs(np.sin(np.radians(incidence_across)))
    * (np.sqrt(1 + (speed_bottom * transmit_duration / (R * np.sin(np.radians(incidence_across)) ** 2))) - 1)
)
A1 = R * np.radians(beam_across_width) / np.cos(np.radians(pointing_angle_theo)) / np.cos(np.radians(slope_across))

################################## Correction  ######################################
rN = np.nanmin(np.sqrt(beamX ** 2 + beamY ** 2 + detectionZ ** 2))
w0 = np.radians(beam_along_width) * R * speed_bottom * transmit_duration / (2 * np.sqrt(1 - (rN ** 2 / R ** 2)))
wN = np.radians(beam_across_width) * np.radians(beam_along_width) * R ** 2

min_area = np.zeros((w0.shape[0], w0.shape[1]))
correction_area = np.zeros((w0.shape[0], w0.shape[1]))
for i in range(w0.shape[0]):
    for j in range(w0.shape[1]):
        min_area[i][j] = min((A0[i][j], A1[i][j]))
        correction_area[i][j] = min(10 * np.log10(w0[i][j]), 10 * np.log10(wN[i][j]))

Area = R * np.radians(beam_along_width) / np.cos(np.radians(slope_along)) * min_area
FinalArea = 10 * np.log10(Area) + correction_area

scope.display_matrix(FinalArea, "Insonified Area", False)

################################## Comparaison ##################################

# sonarscope file
sonarscope_insonified_area = (
    "W:/traitement_Campagnes/Stage_Kevin/SurveyReport/all/SurveyReport/"
    "stage/SonarEquation/0032_20180914_183119_ShipName/0032_20180914_183119_ShipName_SSc_PingBeam_InsonifiedAreadB.tif"
)
sonarscope_area = tiff.read_tiff(sonarscope_insonified_area)
sonarscope_area = np.rot90(sonarscope_area.transpose(), 1)
sonarscope_area[sonarscope_area == 18] = np.nan
scope.display_matrix(sonarscope_area, "Insonified Area from sonarscope", False)

scope.display_matrix(np.abs(FinalArea) - np.abs(sonarscope_area), "Diff of Insonified Area", False)

################################## Echo Level ##################################

detection_backscatter = single_file.get_as_detection_masked_array(
    XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_BACKSCATTER
)
echo = detection_backscatter - 10 * np.log10(Area) + correction_area

scope.display_matrix(echo, "Echo Level", True)

print("End")
