import datetime
from os import path

import numpy as np

# import pyat.core.utils.MatrixInspector as scope
import pyat.core.utils.tiff_driver as tiff
import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as XConstants
import src.Matrix_Inspector as scope
import playground.stage.utils.directory_settings as directories
from pyat.core.dtm.diff_geotiff import DiffGeotiff
from playground.stage.backscatter_processing import IncidenceAngleComputer
from playground.stage.backscatter_processing import InsonifiedAreaComputer
from playground.stage.dtm import GeotiffGenerator
from playground.stage.dtm.geotiff_normal_computer import GeoTiffNormalComputer
from playground.stage.utils import Export_File

# tell if we show all figures immediately or not,
# If set to True in jupyter they will be added right below every step,
# otherwise in standalone it will lock until the figure is closed
show_figure_immediately = False
files = directories.files
# set step to 1.5m to allow comparaison with sonarscope
grid_step = 1.5
mnt = GeotiffGenerator(files, directories.output_dir)
mnt.compute_dtm(grid_step)

sonarscope_dtm = (
    directories.input_directory
    + "/SurveyReport/all/SurveyReport/stage/GEOTIF/stage-DTM_1,5m_LatLong_Bathymetry-53088.tif"
)
match_filename = mnt.projected_dtm
now = datetime.datetime.now()
diff_depth_dtm = path.join(
    directories.output_dir, "dtm_difference_with_sonarscope_" + now.strftime("%Y_%m_%d_%H_%M_%S") + ".tif",
)

diff = DiffGeotiff()
diff.compute(sonarscope_dtm, match_filename, diff_depth_dtm)
scope.display_geotiff_data(
    file_name=diff_depth_dtm, title="MNT difference with sonarscope", show=show_figure_immediately,
)

##################### now compute slope #####################
# Compute normal to the dtm
normal_filename_z = path.join(directories.output_dir, "max_slope_degree" + now.strftime("%Y_%m_%d_%H_%M_%S") + ".tif",)

normal = GeoTiffNormalComputer(match_filename)
normal.compute_normal()
nx, ny, nz = normal.get_normal()

# compare with sonarscope slopes
# dot product between the normal and a normalized (x,y,0) vector give the angle between the normal and the terrain
slope = np.pi / 2 - np.arccos((nx ** 2 + ny ** 2) / np.sqrt(nx ** 2 + ny ** 2))
# on triche et détourne une méthode privée de normal computer pour écrire le geotiff
# noinspection PyProtectedMember
reference_dataset, grid = normal.get_reference()

export_to_geotiff = Export_File()
export_to_geotiff.export(normal_filename_z, np.degrees(slope), reference_dataset, grid)

sonarscope_slope_dtm = (
    directories.input_directory
    + "/SurveyReport/all/SurveyReport/stage/GEOTIF/stage-DTM_1,5m_slopeMax_LatLong_SlopeMax-53088.tif"
)
diff_slope_geotiff = path.join(
    directories.output_dir, "slope_difference_with_sonarscope" + now.strftime("%Y_%m_%d_%H_%M_%S") + ".tif",
)
diff.compute(sonarscope_slope_dtm, normal_filename_z, diff_slope_geotiff)
scope.display_geotiff_data(
    file_name=diff_slope_geotiff, title="slope difference with sonarscope", show=show_figure_immediately,
)

##################### now compute incidence angles #####################
# for comparaison with sonarscope we will just take into account for 0032_20180914_183119_ShipName file

single_file = directories.get_single_file()

filename_incidence_angle = path.join(
    directories.output_dir, "incidence_angle_" + now.strftime("%Y_%m_%d_%H_%M_%S") + ".tif",
)
incidence_computer = IncidenceAngleComputer(files, directories.output_dir)
x, y, cache, incidence_angles = incidence_computer.compute_incidence_angle(normal, single_file)

max_Angle = np.where(incidence_angles == np.amax(incidence_angles))
incidence_angles[max_Angle[0], max_Angle[1]] = np.nan

export_to_geotiff.xsf_toTIFF(filename_incidence_angle, x, y, incidence_angles, cache)

scope.display_geotiff_data(
    filename_incidence_angle, "incidence angle", show=show_figure_immediately,
)

# sonarscope file
sonarscope_incidence_angle = (
    directories.input_directory
    + directories.sonarscope_equation
    + "0032_20180914_183119_ShipName_PingBeam_IncidenceAngle.tif"
)
scope.display_geotiff_data(
    sonarscope_incidence_angle, "sonarscope incidence angle", show=show_figure_immediately,
)

diff_incidence_angle_geotiff = path.join(
    directories.output_dir, "incidence_angle_difference_with_sonarscope" + now.strftime("%Y_%m_%d_%H_%M_%S") + ".tif",
)

calculated_angle = np.rot90(tiff.read_tiff(filename_incidence_angle), 2).transpose()
sonarscope_angle = np.rot90(tiff.read_tiff(sonarscope_incidence_angle), 1)

diff_angle = np.abs(calculated_angle) - np.abs(sonarscope_angle)
scope.display_matrix(
    diff_angle, "incidence angle difference with sonarscope", show=show_figure_immediately,
)

##################### now compute insonified area #####################
# for comparaison with sonarscope we will just take into account for 0032_20180914_183119_ShipName file

filename_insonified_area = path.join(
    directories.output_dir, "insonified_area_" + now.strftime("%Y_%m_%d_%H_%M_%S") + ".tif",
)

insonified_computer = InsonifiedAreaComputer(incidence_computer.work_area, files, directories.output_dir)
x, y, cache, insonified_area = insonified_computer.compute_insonified_area(single_file, filename_incidence_angle)

export_to_geotiff.xsf_toTIFF(filename_incidence_angle, x, y, insonified_area, cache)

##################### now compute echo level #####################

detection_backscatter = single_file.get_as_detection_masked_array(
    XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_BACKSCATTER
)
kongsberg_area, ifremer_area = (
    insonified_computer.kongsberg_area,
    insonified_computer.ifremer_area,
)
echo = detection_backscatter - kongsberg_area + ifremer_area
scope.display_matrix(echo, "Echo Level", False)

# sonarscope file
sonarscope_insonified_area = (
    directories.input_directory
    + directories.sonarscope_equation
    + "0032_20180914_183119_ShipName_SSc_Step1_PingBeam_Reflectivity.tif"
)
echo_sonarscope = tiff.read_tiff(sonarscope_insonified_area)
echo_sonarscope[echo_sonarscope == 18] = np.nan
calculated_area = np.rot90(echo, 2).transpose().filled(np.nan)
sonarscope_area = np.rot90(echo_sonarscope, 1)

diff_reflectivity = np.abs(sonarscope_area) - np.abs(calculated_area)
scope.display_matrix(sonarscope_area, "Echo Level from sonarscope", False)
scope.display_matrix(diff_reflectivity, "Echo Level difference with sonarscope", True)

print("End of computation")
