import warnings

import pyat.core.utils.matrix_inspector as scope
import playground.stage.utils.directory_settings as directories

warnings.filterwarnings("ignore")
# tell if we show all figures immediately or not,
# If set to True in jupyter they will be added right below every step,
# otherwise in standalone it will lock until the figure is closed
show_figure_immediately = True
# Récupère les données du xsf
files = directories.files
single_file = directories.get_single_file()
# Read beam pointing angle and compare with sonarscope
from pyat.core.xsf.struct.sonar_netcdf.sonar_groups import RootGrp

beam_pointing_angle_transducer = single_file.get_as_detection_masked_array(
    RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_BEAM_POINTING_ANGLE
)
# scope.display_matrix(beam_pointing_angle_transducer,"beam_pointing_angle_transducer" , show=show_figure_immediately,bins=100)
import playground.stage.utils.ss_ref_file as ssc

ssc_values = ssc.get_values(ssc.SSCPingBeamVariable.beampointing_angle)
scope.display_matrix(ssc_values)
# scope.display_matrix(ssc_values, "beam_pointing_angle_transducer from Sonarscope", True,bins=100)
# sonarscope angle use another sign convention, so we compare with ss_values*-1 and it seems to have add the transducer installation parameter
import numpy as np

transducer_roll_offset = np.array(single_file.dataset[RootGrp.PlatformGrp.TRANSDUCER_ROTATION_X])
print(transducer_roll_offset)

scope.display_matrix(
    beam_pointing_angle_transducer + ssc_values + transducer_roll_offset[1],
    "Diff between ss_beam_pointing_angle_transducer and ss_beam_pointing_angle_transducer (degree)",
    display_immediate=show_figure_immediately,
    bins=100,
)

raw_pointing_angle = np.array(
    single_file.dataset[RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_BEAM_POINTING_ANGLE]
)

from pyat.core.xsf.csr import angle_csr

surface_angle = angle_csr.detection_pointing_angle_to_surface_crs(single_file.dataset)
# scope.display_matrix(surface_angle, "beam angle ref surface_crs (degree)",show=show_figure_immediately,bins=100)

ssc_rxbeamAngleEarth = ssc.get_values(ssc.SSCPingBeamVariable.rxangle_earth)
# scope.display_matrix(ssc_rxbeamAngleEarth, "RxAngleEarth from Sonarscope", True)

# compute difference, we ignore sign inversion
diffSSC = raw_pointing_angle - -ssc_rxbeamAngleEarth
# scope.display_matrix(diff,"RxAngleEarth vs beam angle ref surface_crs (degree)", show=show_figure_immediately)
scope.display_signal(diffSSC[0, :], "RxAngleEarth offset for ping 0")
# Installation datagram
S1R = 0.110

# compute difference, we ignore sign inversion
surface_angle = angle_csr.detection_pointing_angle_to_surface_crs(single_file.dataset)
diffPyat = raw_pointing_angle - surface_angle
scope.display_signal(diffPyat[0, :], "surface_angle offset for ping 0")
scope.display_signal(diffPyat[0, :] + diffSSC[0, :], "ecart ssc-globe")
scope.show()
