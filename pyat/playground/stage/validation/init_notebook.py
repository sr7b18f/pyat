"""
Use to be able to retrieve modules and import in core and app directorie
"""
from os import sys, path

sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath("__file__")))))
