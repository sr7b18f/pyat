# calcul de l'absorption , coefficient alpha dans l'équation sonar


# reference http://resource.npl.co.uk/acoustics/techguides/seaabsorption/physics.html

# Sonarscope : start in ifremer\acoustics\sounder\Simrad\all2ssc.m
# puis méthode ALL_AbsorpCoeff_RT


# First read absorption coefficient

import glob

import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as XConstants
import playground.stage.utils.directory_settings as directories
from playground.stage.utils.nc_file_cache import XSFFile

# tell if we show all figures immediately or not,
# If set to True in jupyter they will be added right below every step,
# otherwise in standalone it will lock until the figure is closed
show_figure_immediately = False

file_names = glob.glob(directories.input_directory + "/backscatter/boite1/*.xsf")
# Récupère les données du xsf
files = []
for file_name in file_names:
    if "0047_20180914_222017_ShipName" not in file_name:
        files.append(XSFFile(file_name))

single_file = [value for value in files if value.file_name.find("0032_20180914_183119_ShipName") > 0]
single_file = single_file[0]

# now try to recompute IBA
surface_celerity_per_txbeam = single_file.get_as_array(
    XConstants.RootGrp.SonarGrp.BeamGroup1Grp.VendorSpecificGrp.MEAN_ABS_COEFF
)

# shift from tx indexing to detection indexing
grp = single_file.dataset[XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.GROUP_PATH]
detection_dim = grp.dimensions[XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_DIM_NAME].size

detection_tx_index = single_file.get_as_array(
    XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_TX_SECTOR
)

# TEST TO PROJECT FROM TX Indexing matrix to a filled Detection matrix

print(detection_tx_index)
