import numpy as np
from osgeo import osr

import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as Constants
from playground.stage.utils.nc_file_cache import DetectionProjectionCache
from playground.stage.utils.nc_file_cache import XSFFile


# from enum import Enum
# class Algo(Enum):
#     GDAL=0
#     HAVERSINE=1


class GridComputer:
    """
        Given a xsf file in parameter, compute the best estimated grid size
    """

    def __init__(self, file: XSFFile, projection_cache: DetectionProjectionCache):
        self.file = file
        self.projection_cache = projection_cache

    def compute_grid_step(self):
        """
        Parse data and compute the ideal gridding step
        """

        # compute distance between detections
        interdetection = self.compute_inter_detection_distance()
        mean_interdetection = np.ma.mean(interdetection)

        # compute distance between ping
        distance = self.compute_inter_ping_distance()
        mean_interping = np.ma.mean(distance)

        step = max(mean_interdetection, mean_interping)
        # according to the study lead by kevin, the ideal step is 2 times the max value
        return step * 2

    def compute_inter_ping_distance(self):
        """ retrieve and compute vessel distance between successive ping"""
        platform_longitude = self.file.get_as_array(Constants.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_LONGITUDE)
        platform_latitude = self.file.get_as_array(Constants.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_LATITUDE)
        # compute interping distance
        distance = self._compute_distance_with_gdal(platform_longitude, platform_latitude)
        return distance

    def compute_inter_detection_distance(self):
        x = self.file.get_as_array(Constants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_X)
        y = self.file.get_as_array(Constants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_Y)
        mask = self.file.get_as_array(Constants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.STATUS)
        mask[mask != 0] = 1
        x = np.ma.masked_array(x, mask)
        y = np.ma.masked_array(y, mask)
        x = np.ma.diff(np.abs(x[:]))
        y = np.ma.diff(np.abs(y[:]))
        return np.ma.sqrt(x ** 2 + y ** 2)

    def _compute_distance_with_gdal(self, longitudes, latitudes):
        """
        compute the distance one by one between each lat lon points by projecting in UTM
        and computing their respective distances
        :param longitudes: longitude of the vessel
        :param latitudes: Latitude of the vessel
        :return: None
        """
        center_lon, center_lat = self.file.metadata.get_center()
        number_of_points = longitudes.shape[0]
        x = np.empty(number_of_points)
        y = np.empty(number_of_points)

        spatial_ref_target = osr.SpatialReference()
        spatial_ref_target.ImportFromProj4(geoutils.lat_lon_to_utm_proj4(center_lat, center_lon))
        spatial_ref_source = osr.SpatialReference()
        spatial_ref_source.ImportFromProj4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
        transform = osr.CoordinateTransformation(spatial_ref_source, spatial_ref_target)
        for j in range(number_of_points):
            coord_xyz = transform.TransformPoints([[longitudes[j], latitudes[j]]])
            x[j] = coord_xyz[0][0]
            y[j] = coord_xyz[0][1]
        # x[x == np.inf] = np.nan
        # y[y == np.inf] = np.nan
        x = np.diff(np.abs(x[:]))
        y = np.diff(np.abs(y[:]))
        return np.sqrt(x ** 2 + y ** 2)

    def _compute_distance_with_haversine(self, longitudes, latitudes):
        """ l'idée est de calculer la distance avec haversine
        Geoutils.haversine_distance (ce qui doit être plus rapide)"""
        #
        # TODO : implementer la méthode _compute_distance_with_haversine
        #
        pass
