import netCDF4 as nc
import numpy as np

import playground.stage.utils.directory_settings as directories


class SSCPingBeamVariable:
    bathy = "0032_20180914_183119_ShipName_Depth-Heave_RT_PingBeam_Bathymetry"
    # "0032_20180914_183119_ShipName_DTM_PingBeam_Bathymetry"
    # "0032_20180914_183119_ShipName_DTM_Residuals_PingBeam_Bathymetry"
    # "0032_20180914_183119_ShipName_PingBeam_SlopeAcross"
    # "0032_20180914_183119_ShipName_PingBeam_SlopeAlong"
    beampointing_angle = "0032_20180914_183119_ShipName_PingBeam_BeamPointingAngle"
    rxangle_earth = "0032_20180914_183119_ShipName_PingBeam_RxAngleEarth"
    # "0032_20180914_183119_ShipName_PingBeam_IncidenceAngle"
    # "0032_20180914_183119_ShipName_Residuals_PingBeam_IncidenceAngle"
    # "0032_20180914_183119_ShipName_RT_PingBeam_AbsorptionCoeff"
    # "0032_20180914_183119_ShipName_SSc_PingBeam_AbsorptionCoeff"
    # "0032_20180914_183119_ShipName_SSc_Residuals_PingBeam_AbsorptionCoeff"
    # "0032_20180914_183119_ShipName_RT_PingBeam_TVG"
    # "0032_20180914_183119_ShipName_SSc_PingBeam_TVG"
    # "0032_20180914_183119_ShipName_SSc_Residuals_PingBeam_TVG"
    # "0032_20180914_183119_ShipName_RT_PingBeam_InsonifiedAreadB"
    # "0032_20180914_183119_ShipName_SSc_PingBeam_InsonifiedAreadB"
    # "0032_20180914_183119_ShipName_Residuals_PingBeam_InsonifiedAreadB"
    # "0032_20180914_183119_ShipName_Lambert_RT_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_Lambert_SSc_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_Lambert_Residuals_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_KM_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_SSc_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_SSc_Without_Lambert_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_SSc_Without_Lambert_Residuals_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_SSc_Step1_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_SSc_Step1_Residuals2_PingBeam_Reflectivity"
    # "0032_20180914_183119_ShipName_PingBeam_Latitude"
    # "0032_20180914_183119_ShipName_PingBeam_Longitude"
    # "0032_20180914_183119_ShipName_PingBeam_GeoX"
    # "0032_20180914_183119_ShipName_PingBeam_GeoY"
    ssc_iba_earth = "0032_20180914_183119_ShipName_SnellDescartes_SSc_PingBeam_RxAngleEarth"
    # "0032_20180914_183119_ShipName_SnellDescartes_RT_PingBeam_KongsbergIBA"
    # "0032_20180914_183119_ShipName_SnellDescartes_Residuals_PingBeam_RxAngleEarth"


class SSCFile:
    """
    Class handling opening and closing of a netcdf file
    """

    def __init__(self, filename: str):
        # open the file
        self.file_name = filename
        self.dataset = nc.Dataset(self.file_name)


ssc_pingbeam_file_name = (
    directories.input_directory
    + directories.sonarscope_equation
    + "0032_20180914_183119_ShipName-Layers-PingBeam_Debug.nc"
)


ssc_pingbeam_file = SSCFile(ssc_pingbeam_file_name)


def get_values(variable_name: str) -> np.array:
    """return a variable read from sonarscope summary"""
    ssc_values = np.array(ssc_pingbeam_file.dataset[variable_name])
    return ssc_values.transpose()
