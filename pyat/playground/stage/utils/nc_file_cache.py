import netCDF4 as nc
import numpy as np
from pyproj import Proj, transform

import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as XConstants


class XSFFile:
    """
    Class handling opening and closing of a netcdf file
    """

    def __init__(self, filename: str):
        # open the file
        self.file_name = filename
        self.dataset = nc.Dataset(self.file_name)
        self.cache = dict()
        self.cache_masked_data = dict()
        self.metadata = XSFMetadata(self)

    def __del__(self):
        # close the file
        self.dataset.close()

    def get_as_array(self, variable_name: str):
        """
        retrieve a classical variable and convert it as a numpy array
        The resulting array is stored in a dictionnary serving as a cache
        """
        value = self.cache.get(variable_name)
        if value is not None:
            return value
        value = np.array(self.dataset[variable_name])
        self.cache[variable_name] = value
        return value

    def remove_from_cache(self, variable_name: str):
        self.cache.pop(variable_name)

    def remove_from_detection_masked_cache(self, variable_name: str):
        self.cache_masked_data.pop(variable_name)

    def get_as_detection_masked_array(self, variable_name: str):
        """
            retrieve a detection variable and convert it as a numpy array, and apply the status mask to this variable
            The resulting array is stored in a dictionnary serving as a cache
        """
        value = self.cache_masked_data.get(variable_name)
        if value is not None:
            return value
        if not hasattr(self, "detection_mask"):
            mask = self.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.STATUS)
            mask[mask != 0] = 1
            self.detection_mask = mask

        value = self.get_as_array(variable_name)
        value = np.ma.masked_array(value, self.detection_mask)
        self.cache_masked_data[variable_name] = value
        # remove from non masked cache
        self.remove_from_cache(variable_name)
        return value

    def takeoff_offset_transducer(self, x, y, z):
        """
        Withdraw the offset of the transducer
        """
        offset_x = self.get_as_array(XConstants.RootGrp.PlatformGrp.TRANSDUCER_OFFSET_X)
        offset_y = self.get_as_array(XConstants.RootGrp.PlatformGrp.TRANSDUCER_OFFSET_Y)
        offset_z = self.get_as_array(XConstants.RootGrp.PlatformGrp.TRANSDUCER_OFFSET_Z)
        return x + offset_x[0], y + offset_y[0], z + offset_z[0]

    def takeoff_tide(self, detectionZ):
        """
        Remove tide of detection Z
        """
        tide = self.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.WATERLINE_TO_CHART_DATUM)
        tide = tide.reshape(tide.shape[0], 1)
        return detectionZ - tide

    def takeoff_waterline(self, detectionZ):
        """
        Remove tide of detection Z
        """
        water_line_translation = self.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_VERTICAL_OFFSET)
        water_line_translation = water_line_translation.reshape(water_line_translation.shape[0], 1)
        return detectionZ - water_line_translation


class XSFMetadata:
    """ this class store metadata computed with a given xsf File"""

    def __init__(self, xsf_file):
        self.xsf_file = xsf_file

    def get_center(self):
        """ get a tuple of data being the geographic center of the file
            :return a tuple (longitude,latitude)
        """
        if not hasattr(self, "center"):
            # we use platform position instead of all detection positions
            platform_longitude = self.xsf_file.get_as_array(
                XConstants.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_LONGITUDE
            )
            platform_latitude = self.xsf_file.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_LATITUDE)
            centerLon = np.mean(platform_longitude)
            centerLat = np.mean(platform_latitude)
            self.center = (centerLon, centerLat)
        return self.center


class DetectionProjectionCache:
    """ this class is a cache to store detection position in one projection"""

    def __init__(self, xsf_file, projection_proj4_def: str):
        self.projection_proj4_def = projection_proj4_def
        self.file = xsf_file

    def getXY(self):
        if not hasattr(self, "_x"):
            self.project()
            # self.offset_off(self._x, self._y)
        return self._x, self._y

    def project(self):
        """ project the detection variable to the projection given in parameter and store result in file variables"""
        projSource = Proj("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
        projDest = Proj(self.projection_proj4_def)
        latitudes = self.file.get_as_detection_masked_array(
            XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_LATITUDE
        )
        longitudes = self.file.get_as_detection_masked_array(
            XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_LONGITUDE
        )
        self._x, self._y = transform(projSource, projDest, longitudes, latitudes)
        print(
            "Compute detection latitude and longitude to UTM for file ", self.file.file_name,
        )


class ShipProjectionCache:
    """ this class is a cache to store ship position in one projection"""

    def __init__(self, xsf_file, projection_proj4_def: str):
        self.projection_proj4_def = projection_proj4_def
        self.file = xsf_file

    def getXY(self):
        if not hasattr(self, "_x"):
            self.project()
        return self._x, self._y

    def project(self):
        """ project the detection variable to the projection given in parameter and store result in file variables"""
        projSource = Proj("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
        projDest = Proj(self.projection_proj4_def)
        latitudes = self.file.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_LATITUDE)
        longitudes = self.file.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_LONGITUDE)
        self._x, self._y = transform(projSource, projDest, longitudes, latitudes)
        # self._x[self._x == np.inf] = np.nan
        # self._y[self._y == np.inf] = np.nan
        print(
            "Compute ship position latitude and longitude to UTM for file", self.file.file_name,
        )
