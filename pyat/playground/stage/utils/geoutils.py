import math

import numpy as np
from osgeo import osr


def fromWKT_to_epsg(wkt: str):
    """ convert a wkt string to epsg"""
    srs = osr.SpatialReference()  # makes an empty spatial ref object
    srs.ImportFromWkt(wkt)  # populates the spatial ref object with our WKT SRS
    return srs.ExportToProj4()  # Exports an SRS ref as a Proj4 str


def haversine_distance(origin, destination):
    """
    Calculate the Haversine distance.
    https://fr.wikipedia.org/wiki/Formule_de_haversine
    Parameters
    ----------
    origin : tuple of float
        (lat, long)
    destination : tuple of float
        (lat, long)

    Returns
    -------
    distance_in_km : float

    """
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371  # km

    dlat = np.radians(lat2 - lat1)
    dlon = np.radians(lon2 - lon1)
    a = np.sin(dlat / 2) * np.sin(dlat / 2) + np.cos(np.radians(lat1)) * np.cos(np.radians(lat2)) * np.sin(
        dlon / 2
    ) * np.sin(dlon / 2)
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    d = radius * c

    return d


def meters_to_decimal_degrees(meters, latitude):
    """
    convert a distance from meter to decimal degree at a given latitude and assuming that earth is round
    :param meters: the distance
    :param latitude: the considered latitude
    :return: the equivalent angle in degree
    """
    return meters / (111.32 * 1000 * math.cos(math.radians(latitude)))


def latlon_to_zone_number(latitude, longitude):
    """ Copied from utm python package"""
    if 56 <= latitude < 64 and 3 <= longitude < 12:
        return 32

    if 72 <= latitude <= 84 and longitude >= 0:
        if longitude < 9:
            return 31
        elif longitude < 21:
            return 33
        elif longitude < 33:
            return 35
        elif longitude < 42:
            return 37

    return int((longitude + 180) / 6) + 1


def lat_lon_to_utm_proj4(latitude, longitude):
    utm_band = str(latlon_to_zone_number(latitude, longitude))

    if latitude >= 0:
        return "+proj=utm +zone=" + utm_band + "+ellps=WGS84 +datum=WGS84 +units=m +no_defs"
    else:
        return "+proj=utm +zone=" + utm_band + "+south +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
