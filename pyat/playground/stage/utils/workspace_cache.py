import numpy as np

import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as XConstants


class FileCache:
    """
    Class utility to store data associated with one file. Mainly projected positions of ship or detections
    """

    def __init__(self, file_list):
        self.file_list = file_list
        self.projected_detection = dict()
        self.ship_position_projected = dict()


class WorkArea_Cache:
    def __init__(self, files, output_directory):
        self.xsf_files = files
        self.cache = FileCache(self.xsf_files)
        self.projection = None

    def computeBoundingBoxes(self):
        # first of all compute the bounding boxes
        minlat, maxlat = [], []
        minlon, maxlon = [], []
        for f in self.xsf_files:
            longitudes = f.get_as_detection_masked_array(
                XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_LONGITUDE
            )
            minlon.append(np.min(longitudes))
            maxlon.append(np.max(longitudes))
            latitudes = f.get_as_detection_masked_array(
                XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_LATITUDE
            )
            minlat.append(np.min(latitudes))
            maxlat.append(np.max(latitudes))
        return [min(minlon), min(minlat), max(maxlon), max(maxlat)]

    def define_projection(self):
        print("Computing bounding box")
        (x_min_longitude, y_min_latitude, x_max_longitude, y_max_latitude,) = self.computeBoundingBoxes()

        print("define metric projection")
        center_latitude, center_longitude = (
            (y_max_latitude + y_min_latitude) / 2,
            (x_max_longitude + x_min_longitude) / 2,
        )
        self.projection = geoutils.lat_lon_to_utm_proj4(center_latitude, center_longitude)
        print("Use projection :", self.projection)
