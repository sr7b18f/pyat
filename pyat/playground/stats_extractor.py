import pandas as pd

stats = pd.read_csv("d:/DownloadStats2019.csv", sep=";")
data_top = stats.head()
# Sonarscope stats
keys = ["ADELIE", "CARAIBES", "CASINO", "MOVIES", "SonarScope", "GLOBE", "DORIS"]
for k in keys:
    extract = stats[stats["Fichier "].str.contains(k)].groupby("Mail ").count()
    print(k, ":", extract.__len__())
