import subprocess
from os import sys, path

output_dir = "d://tmp//"

sonarscope_dtm = r"D:\tmp\SurveyReport\all\SurveyReport\stage\GEOTIF\trest.tif"
gdal_utils_path = path.join(sys.prefix, "Scripts")
fileA = output_dir + "2019_10_08_08_58_18.tif"
fileA = fileA.replace("//", "\\")
diff_mnt_cmd = [
    "python",
    path.join(gdal_utils_path, "gdal_calc.py"),
    "-A" + fileA,
    "-B" + sonarscope_dtm,
    "--outfile=" + path.join(output_dir, "dtm_comparaison_result.tif"),
    '--calc="(A-B)"',
    "--NoDataValue=0",
]
subprocess.call(diff_mnt_cmd)
