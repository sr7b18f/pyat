from osgeo import gdal
from osgeo import gdalconst
import numpy as np
import matplotlib.pyplot as plt

from pyat.core.utils.tiff_driver import read_tiff

def read_png(png):
        # display differences
        src = gdal.Open(png, gdalconst.GA_ReadOnly)
        band=[[],[],[],[]]
        values=[[],[],[],[]]
        for b in range(1, 4):
            band[b] = src.GetRasterBand(b)
            values[b-1] = band[b].ReadAsArray()

        return (values[0]*256*256. +values[1]*256+values[2])*255./(256.*256.*256.)

def print_gradient(tiff,reader=read_tiff):
    plt.figure()

    value = reader(tiff)
    grad = np.gradient(value)

    f=plt.imshow(np.maximum(grad[1],grad[0]))
    plt.clim(-10,10)
    plt.title(tiff)


#print_gradient(r'C:\ProgramData\WorldWindData\globe_data_v2\25m_UTM__Elevation_qgis_2_Elevation_rgb\0\0\warp2.tif',reader=read_png)
print_gradient(r'C:\ProgramData\WorldWindData\globe_data_v2\25m_UTM__Elevation_qgis_2_Elevation_rgb\0\0\warp_bilinear.tif',reader=read_png)
print_gradient(r'C:\Users\cponcele\AppData\Local\Temp\GLOBE_25m_UTM__Elevation_qgis_2_Elevation9411930214207424774.tif',reader=read_png)

print_gradient(r'C:\ProgramData\Application Data\WorldWindData\globe_data_v2\25m_UTM__Elevation_qgis_2_Elevation_rgb\0\0\0_0.png',reader=read_png)
# print_gradient(r'C:\ProgramData\WorldWindData\globe_data_v2\25m_UTM__Elevation_qgis_2_Elevation_rgb\0\0\0_0.png_asbyte.tiff',
#                reader=read_png)
#print_gradient(r"C:\Users\cponcele\AppData\Local\Temp\GLOBE_25m_UTM__Elevation_qgis_2_Elevation8889593322051260064.tif")
#print_gradient(r"C:\data\tmp\dano\TestOmbrage.tif")
plt.show(block=True)

