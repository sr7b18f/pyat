"""
This module is dedicated to do some analysis and to compare the two differents tide models FES2014 and Shom harmonic files
"""
# Get the feed
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pyat.core.tide.shom_harmonic.shom_harmonic_tide_predict as shom
import pyat.core.tide.tidegauges.shom_service as tg
import pyat.core.tide.fes2014.fes_tide_predict as fes
import pyat.core.dtm.services.depth_retriever as em
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from dateutil import rrule
import pyat.core.tide.tide_shom.hfs_tide as hfs
from pyat.core.surface_reference.sea_surface import SeaSurfaceProjector
from pyat.core.utils.monitor import DefaultMonitor


def compare_models_vs_tidegauge():

    date_start = datetime.datetime(year=2021, month=4, day=1, hour=4, minute=15, second=0, tzinfo=datetime.timezone.utc)
    date_end = datetime.datetime(year=2021, month=4, day=21, hour=6, minute=30, second=0, tzinfo=datetime.timezone.utc)

    # RETRIEVE TIDEGAUGE
    service = tg.TideGaugeGetter()
    selected = service.get_tidegauge("LE_CONQUET")
    if len(selected) == 0:
        raise Exception("Cannot find tide gaude")
    # retrieve lat/lon of point of interest
    longitude = float(selected[0]["longitude"])
    latitude = float(selected[0]["latitude"])
    latitudes = np.array([latitude])
    longitudes = np.array([longitude])

    # # 1 = Brutes haute fréquence, 2 = Brutes temps différé, 3 = Validées temps différé, 4 = Validées horaires, 5 = Brutes horaires
    t, v = service.get_values("LE_CONQUET", date_start=date_start, date_end=date_end, data_source=1)

    # generate dates for prediction
    time_elapsed = date_end - date_start
    duration_minute = int(time_elapsed.total_seconds() / 60)
    dates_ = list(
        rrule.rrule(freq=rrule.MINUTELY, interval=10, dtstart=date_start, count=int(duration_minute / 10) + 1)
    )
    dates_ = np.array(dates_)

    fig, ax = plt.subplots()

    ax.plot(t, v, label=f"TideGauge : {selected[0]['name']}Brutes haute fréquence")

    predictor = fes.FESTideComputer(model_dir_path="C:/data/datasets/Tide/fes2014")
    # Creating the time series
    _, _, dates, tides, *_ = predictor.compute_tides(latitudes=latitudes, longitudes=longitudes, dates=dates_)
    # FIXME : retrieve bathyElli model and compute the static offset
    ax.plot(dates, tides + 3.5, label="fes2014 +3.5m (constant offset due to missing BathyElli)")

    predictor_shom = shom.ShomTidePredictor(harmonic_file="C:/data/datasets/Tide/harmoniques/cstCELTIQUE_143.har")
    _, _, dates, tides, *_ = predictor_shom.compute_tides(latitudes=latitudes, longitudes=longitudes, dates=dates_)
    ax.plot(dates, tides, label="shom cstCELTIQUE_143.har")

    predictor_shom = shom.ShomTidePredictor(
        harmonic_file="C:/data/datasets/Tide/harmoniques/cstFRANCE_24062009_143.har"
    )
    _, _, dates, tides, *_ = predictor_shom.compute_tides(
        latitudes=np.array([latitude]), longitudes=np.array([longitude]), dates=dates_
    )
    ax.plot(dates, tides, label="shom cstFRANCE_24062009_143.har")

    depth_at_point = em.retrieve_depth(longitude=longitude, latitude=latitude)

    plt.legend()
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%d %H:%M:%S"))
    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    plt.grid()
    plt.show()


def compare_models():
    latitude = 47.5
    longitude = -5.5625

    depth_at_point = em.retrieve_depth(longitude=longitude, latitude=latitude)
    proj = SeaSurfaceProjector(model_dir="C:/data/datasets/Tide/tide_models")

    date = datetime.datetime(2019, 4, 21, 0, 0, 0, tzinfo=datetime.timezone.utc)
    # one day of tide every 10 min
    every_x_minutes = 10
    period_in_days = 30
    dates_ = np.array(
        [
            date + datetime.timedelta(seconds=item * 3600 / every_x_minutes)
            for item in range(24 * every_x_minutes * period_in_days)
        ]
    )

    predictor = fes.FESTideComputer(model_dir_path="C:/data/datasets/Tide/tide_models/fes2014")
    # Creating the time series

    # run prediction with fes2014
    _, _, dates, tides, tide_ori, long_period, load_, load_lp_ = predictor.compute_tides(
        latitudes=np.array([latitude]), longitudes=np.array([longitude]), dates=dates_, monitor=DefaultMonitor
    )
    tides = proj.project_msl_to_lat(longitudes=np.array([longitude]), latitudes=np.array([latitude]), values=tides)

    tide_ori = proj.project_msl_to_lat(
        longitudes=np.array([longitude]), latitudes=np.array([latitude]), values=tide_ori
    )

    predictor_shom = shom.ShomTidePredictor(harmonic_file="C:/data/datasets/Tide/harmoniques/cstCELTIQUE_143.har")
    _, _, dates, tides_shom_w, *_ = predictor_shom.compute_tides(
        latitudes=np.array([latitude]), longitudes=np.array([longitude]), dates=dates_, monitor=DefaultMonitor
    )
    #
    predictor_shom = shom.ShomTidePredictor(
        harmonic_file="C:/data/datasets/Tide/harmoniques/cstFRANCE_24062009_143.har"
    )
    _, _, dates, tides_shom, *_ = predictor_shom.compute_tides(
        latitudes=np.array([latitude]), longitudes=np.array([longitude]), dates=dates_, monitor=DefaultMonitor
    )

    fig, ax = plt.subplots(2, 1)
    ax1 = plt.subplot(2, 1, 1)
    ax1.plot(dates, tides, label="fes2014", marker="+")
    ax1.plot(dates, tides_shom_w, label="shom cstCELTIQUE_143.har")
    ax1.plot(dates, tides_shom, label="shom cstFRANCE_24062009_143.har")

    ax1.legend()
    # ax.xaxis.set_major_locator(mdates.DayLocator(interval=4))
    ax1.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%d %H:%M:%S"))
    plt.setp(ax1.get_xticklabels(), rotation=30, ha="right")
    ax1.grid()
    fig.autofmt_xdate()

    ax2 = plt.subplot(2, 1, 2, sharex=ax1)

    ax2.plot(dates, tides - tides_shom_w, label="fes - shom_celtique ")
    ax2.plot(dates, tides - tides_shom, label="fes - shom_france ")
    ax2.plot(dates, tides_shom_w - tides_shom, label="shom_celtique - shom_france ")
    tide_ori = proj.project_msl_to_lat(
        longitudes=np.array([longitude]), latitudes=np.array([latitude]), values=tide_ori
    )
    ax2.plot(dates, tide_ori - tides_shom, label="tide_ori - shom_france ")
    ax2.plot(dates, tide_ori - tides_shom_w, label="tide_ori - shom_celtique ")
    ax2.legend()
    ax2.grid()

    plt.show(block=True)


compare_models()
