"""
This module is dedicated to do some analysis and to compare the two differents tide : model FES2014 and Shom  bought prediction
"""
import datetime

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
from dateutil import rrule

import pyat.core.tide.fes2014.fes_tide_predict as fes
import pyat.core.tide.tide_shom.hfs_tide as hfs
from pyat.core.utils.monitor import DefaultMonitor


def compare_mayobs_area():
    """compare tide prediction bought from shom at a given point near mayotte and prediction from FES over 1 year"""
    point_lat = -12.5
    point_lon = 45.65
    predictor = fes.FESTideComputer(model_dir_path="C:/data/datasets/Tide/fes2014")

    # compute FES local Lowest Astronomical Tide over 20 years, with a 10 min resolution
    date_start = datetime.datetime(year=2001, month=1, day=1, hour=0, minute=0, second=0, tzinfo=datetime.timezone.utc)
    date_end = datetime.datetime(year=2021, month=1, day=1, hour=0, minute=0, second=0, tzinfo=datetime.timezone.utc)
    time_elapsed = date_end - date_start
    duration_minute = int(time_elapsed.total_seconds() / 60)
    dates_for_lowest_astro_tide = list(
        rrule.rrule(freq=rrule.MINUTELY, interval=10, dtstart=date_start, count=int(duration_minute / 10) + 1)
    )
    dates_for_lowest_astro_tide = np.array(dates_for_lowest_astro_tide)
    # now predic tide over this
    _, _, _, tides_lat, *_ = predictor.compute_tides(
        latitudes=np.array([point_lat]),
        longitudes=np.array([point_lon]),
        dates=dates_for_lowest_astro_tide,
        monitor=DefaultMonitor,
    )
    min_tide = np.min(tides_lat)
    print(f"min tide at point {min_tide}")
    # compute tides values
    time_hfs, tide_hfs = hfs.read("C:/data/datasets/Tide/prediction_mayotte/prediction_1230S4539E.hfs")
    _, _, dates, tides_fes, tide_raw, lp_raw, load_raw, load_lp = predictor.compute_tides(
        latitudes=np.array([point_lat]), longitudes=np.array([point_lon]), dates=time_hfs, monitor=DefaultMonitor
    )
    # get tides_fes to LAT ref
    tides_fes -= min_tide

    # Display results and analysis

    fig, ax = plt.subplots(3, 1)
    ax1 = plt.subplot(3, 1, 1)
    ax1.plot(dates, tides_fes, label="fes2014")
    # ax1.plot(dates, tide_raw, label="fes2014 raw")
    # ax1.plot(dates, tide_raw + lp_raw, label="fes2014 raw+lp")
    # ax1.plot(dates, tide_raw + load_raw, label="fes2014 raw+load_raw")

    ax1.plot(time_hfs, tide_hfs, label="shom prediction")
    ax1.legend()
    # reset to 0 mean values
    ax2 = plt.subplot(3, 1, 2, sharex=ax1)

    print(f"median values_hfs {np.median(tide_hfs)}")
    ax2.plot(time_hfs, tide_hfs - np.median(tide_hfs), label="shom prediction - median ")
    print(f"median fes2014 {np.median(tides_fes)}")

    ax2.plot(time_hfs, tides_fes - np.mean(tides_fes), label="fes2014 prediction - median")
    ax2.legend()

    # residu
    rel_fes = tides_fes - np.median(tides_fes)
    ref_hfs = tide_hfs - np.median(tide_hfs)
    rel_fes_lp = tide_raw + lp_raw
    rel_fes_lp = rel_fes_lp - np.median(rel_fes_lp)
    ax3 = plt.subplot(3, 1, 3, sharex=ax1)

    ax3.plot(time_hfs, rel_fes - ref_hfs, label="relatives fes2014 - shom prediction")
    ax3.plot(time_hfs, rel_fes_lp - ref_hfs, label="lp_raw fes2014 - shom prediction")

    ax3.legend()
    # ax.xaxis.set_major_locator(mdates.DayLocator(interval=4))

    ax3.grid()
    ax3.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%d %H:%M:%S"))
    plt.setp(ax3.get_xticklabels(), rotation=30, ha="right")
    fig.autofmt_xdate()

    # fft_hfs=np.fft.fft(tide_hfs)
    # fft_fes=np.fft.fft(tides_fes)
    # freq = np.fft.fftfreq(dates.shape[-1])
    # freq*= 24*6 #sampling is every 10min
    # plt.subplots(1,1)
    # ax=plt.subplot(1,1,1)
    # ax.plot(freq,np.absolute( fft_hfs),label="shom")
    # ax.plot(freq,np.absolute( fft_fes),label="fes")
    # ax.set_xlabel("Frequency (per day)")
    # ax.legend()
    # ax.grid()
    # ax.set_xlim([0,5])
    plt.show()


# compare_models_vs_tidegauge()


compare_mayobs_area()
