import tempfile as tmp
import os
import numpy as np
import netCDF4 as nc
import pandas as pd
from playground.GDFCreator import GDFConstants as cst


class CSVBinaryConverter:
    def __init__(self, input, output, coordinates_columns, elevation_factor=1.0):
        self.input = os.path.realpath(input)
        self.base = os.path.basename(input)
        self.input_directory = os.path.dirname(input)
        self.output = os.path.realpath(output)
        self.elevation_factor = elevation_factor
        self.coordinates_columns = coordinates_columns

    def convert(self):
        data = pd.read_csv(self.input, sep="\t", header=0)
        numerical_data = []
        for col in data:
            if (data[col].dtypes == np.float64) and not col in self.coordinates_columns.values():
                # we just keep numerical values
                numerical_data.append(col)

        with nc.Dataset(self.output, mode="w") as out:
            # declare type
            out.dataset_type = cst.RootGrp.TYPE_POINTCLOUD
            out.history = "Created via conversion from " + self.input
            datalayer_dim = out.createDimension(cst.RootGrp.DATALAYER_COUNT, len(numerical_data))
            v = out.createVariable(
                varname=cst.RootGrp.DATALAYER_VARIABLES_NAMES, dimensions=(cst.RootGrp.DATALAYER_COUNT), datatype=str
            )
            for i in range(0, len(numerical_data)):
                v[i] = numerical_data[i]

            slice_group = out.createGroup("000")
            slice_group.is_valid = 1
            slice_group.long_name = "Default"
            obs_count = slice_group.createDimension(cst.RootGrp.DataLayerGrp.OBS_DIM, data.shape[0])
            latitude = slice_group.createVariable(
                varname=cst.RootGrp.DataLayerGrp.LATITUDE_VARIABLE,
                dimensions=(cst.RootGrp.DataLayerGrp.OBS_DIM),
                datatype="f8",
            )
            longitude = slice_group.createVariable(
                varname=cst.RootGrp.DataLayerGrp.LONGITUDE_VARIABLE,
                dimensions=(cst.RootGrp.DataLayerGrp.OBS_DIM),
                datatype="f8",
            )
            elevation = slice_group.createVariable(
                varname=cst.RootGrp.DataLayerGrp.BlockGrp.ELEVATION,
                dimensions=(cst.RootGrp.DataLayerGrp.OBS_DIM),
                datatype="f4",
            )
            latitude[:] = data[coordinates["latitude"]][:]
            longitude[:] = data[coordinates["longitude"]][:]
            elevation_values = self.elevation_factor * data[coordinates["elevation"]][:]
            elevation[:] = elevation_values

            for name in numerical_data:
                v = slice_group.createVariable(
                    varname=name, dimensions=(cst.RootGrp.DataLayerGrp.OBS_DIM), datatype="f4",
                )
                v[:] = data[name][:]


if __name__ == "__main__":
    print("starting xml converter")
    out = tmp.mktemp(suffix=".nc", prefix="PointCloud_", dir="d://tmp")

    input = "D:/tmp/GA-HY03-BAT2-BAT5-up.txt"
    # dictonnary to retrieve coordinates, depends on the file
    coordinates = {"latitude": "Lat_N", "longitude": "Long_E", "elevation": "Depth_m"}
    converter = CSVBinaryConverter(input, out, coordinates, -1)
    converter.convert()
