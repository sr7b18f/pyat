from pathlib import Path

import gdf_constants as cst
import tempfile as tmp
import netCDF4 as nc
import numpy as np
from skimage.measure import block_reduce
from sonar_netcdf import sonar_groups as sonar_def


def pprint(msg: str):
    print(msg)


def apply_scale_offset(values: np.ndarray, scale_factor=None, add_offset=None, missing_value=None):
    replace_masked_values = False
    if missing_value is not None:
        if np.ma.is_masked(values):
            mask = np.ma.getmask(values)
        else:
            invalid_values = values == missing_value
            mask = invalid_values
    if scale_factor is not None:
        values = values * scale_factor
        replace_masked_values = True
    if add_offset is not None:
        values = values + add_offset
        replace_masked_values = False
    if replace_masked_values and missing_value is not None:
        values[mask] = np.nan
    return values


def _squeeze_shape(shape: tuple, dimensions: tuple):
    """
    Remove dimension equals to 1 in a tuple list, if everything is equal to 1 the initia
    :param shape:
    :return: a tuple with 1 value removed
    """
    final_shape = ()
    final_dims = ()

    for sh, dim in zip(shape, dimensions):
        if sh != 1:
            final_shape += (sh,)
            final_dims += (dim,)
    return final_shape, final_dims


# copy from notebooks code, shall be moved somewhere else
def get_vlen_variable(file: nc.Dataset, variable_path: str, slice_index: dict = dict()):
    """
        retrieve a matrix containing a slice (matrix) of all vlen data, filled with NaN values


    """
    vlen_variable = file[variable_path]
    shape = vlen_variable.shape
    dimensions = vlen_variable.dimensions
    reduced_shape, reduced_dimensions = _squeeze_shape(shape, dimensions)

    #disable autoscale to keep a code compliant no matter what netcdf version we are using
    vlen_variable.set_auto_scale(False)
    
    # with netcdf < 1.5.4 we need to take into account for scale factor and add offset by ourself
    scale_factor = None
    add_offset = None
    if hasattr(vlen_variable, "scale_factor"):
        scale_factor = vlen_variable.scale_factor
    if hasattr(vlen_variable, "add_offset"):
        add_offset = add_offset
    missing_value = None
    if hasattr(vlen_variable, "missing_value"):
        missing_value = vlen_variable.missing_value
    if hasattr(vlen_variable, "_FillValue"):
        missing_value = vlen_variable._FillValue

    if len(reduced_shape) == 0:
        # data are like a vector shape
        values = vlen_variable[:]
        values = np.squeeze(values)
        values = apply_scale_offset(scale_factor, add_offset, missing_value)
        return values[:]

    if len(reduced_shape) > 1:
        # data is still with a dimensionnality higher than 1 (in fact 2 with its vlen charactéristic)
        # we need to reduce this, to do so we'll use the slice index dictionnary

        values = np.squeeze(vlen_variable[:])
        # compute slice taking into account for slice_index
        selection_applied = ""
        reduced_shape_in_progress = ()
        reduced_dim_in_progress = ()
        slicing = ()
        for sh, dim in zip(reduced_shape, reduced_dimensions):
            if dim in slice_index:
                index = slice_index[dim]
                slicing += (slice(1),)  # we select the slice specified in index
            else:
                reduced_shape_in_progress += (sh,)
                reduced_dim_in_progress += (dim,)
                slicing += (slice(None),)  # we select all data
        values = values[slicing]
        values = np.squeeze(values)
        reduced_shape = reduced_shape_in_progress
        reduced_dimensions = reduced_dim_in_progress
    else:

        if "echoangle_major" in variable_path or "echoangle_minor" in variable_path:
            #BIG HACK TO REMOVE
            values = np.empty(shape=reduced_shape,dtype=object)
            for index in range(0,reduced_shape[0]):           
                values[index]=vlen_variable[index,0]
        else:
            values = vlen_variable[:]
    if len(reduced_shape) == 1:
        # dimension is 1 like a ping indexed variable, we can parse all vlen data and build a matrix from it
        values = np.squeeze(values)
        # retrieve max size
        max_samples = 0
        for (
            sub_array
        ) in values:  # in a performance point of view we'll read the data twice
            max_samples = max(max_samples, len(sub_array))
        # fill ping with data\n",
        matrix = np.full(
            (reduced_shape[0], max_samples),
            dtype="float32",
            fill_value=float(np.nan),
        )
        for x in range(reduced_shape[0]):
            # Warning, auto scale if set by default ping[bnr][:count] = sample_amplitude[start:stop]
            count = len(values[x])
            matrix[x][:count] = apply_scale_offset(values[x], scale_factor, add_offset, missing_value)
        matrix = matrix.transpose()
        return matrix[::-1, :]
    raise NotImplementedError(
        f"Not supported display of vlen variable {variable_path}, reduced dimensions are too high {reduced_dimensions} ({reduced_shape})"
    )


def nano_todatetime(value: np.array):
    """
    Convert nanosecond values to numpy datetime array
    :param value:
    :return: a new allocated array with values in datetime
    """
    return value.astype("datetime64[ns]")


def convert(input_file_name: str, output_file_name: str, max_vertical_samples=16380):
    print(f"Converting {input_file_name} to {output_file_name}")
    with nc.Dataset(input_file_name) as input:
        sonarGrp = input[sonar_def.SonarGrp.get_group_path()]
        selected_beam = 0
        for beam_group_name in sonarGrp.groups:
            # ping_count= input[sonar_def.BeamGroup1Grp.PING_TIME_DIM_NAME]
            beam_group = input[sonar_def.BeamGroup1Grp.get_group_path(beam_group_name)]
            ping_count_dim = beam_group.dimensions[sonar_def.BeamGroup1Grp.PING_TIME_DIM_NAME]
            ping_count = len(ping_count_dim)
            longitude_variable = input[sonar_def.BeamGroup1Grp.PLATFORM_LONGITUDE(beam_group_name)]
            longitude_values = longitude_variable[:]
            latitude_variable = input[sonar_def.BeamGroup1Grp.PLATFORM_LATITUDE(beam_group_name)]
            latitude_values = latitude_variable[:]
            time_variable = input[
                sonar_def.BeamGroup1Grp.PING_TIME(beam_group_name)]  # nanosec in netcdf but should be set to ms
            values = get_vlen_variable(file=input, variable_path=sonar_def.BeamGroup1Grp.BACKSCATTER_R(beam_group_name),
                                       slice_index={'beam': selected_beam})

            angle_major = get_vlen_variable(file=input,
                                            variable_path=sonar_def.BeamGroup1Grp.ECHOANGLE_MAJOR(beam_group_name),
                                            slice_index={'beam': selected_beam})
            angle_minor = get_vlen_variable(file=input,
                                            variable_path=sonar_def.BeamGroup1Grp.ECHOANGLE_MINOR(beam_group_name),
                                            slice_index={'beam': selected_beam})

            top = input[sonar_def.BeamGroup1Grp.TX_TRANSDUCER_DEPTH(
                beam_group_name)]  # nanosec in netcdf but should be set to ms
            sound_speed_at_transducer = input[sonar_def.BeamGroup1Grp.SOUND_SPEED_AT_TRANSDUCER(beam_group_name)][:]
            blanking_interval = input[sonar_def.BeamGroup1Grp.BLANKING_INTERVAL(beam_group_name)][:, selected_beam]
            sample_time_offset = input[sonar_def.BeamGroup1Grp.SAMPLE_TIME_OFFSET(beam_group_name)][:, selected_beam]

            sample_index = values.shape[0]
            sample_interval = input[sonar_def.BeamGroup1Grp.SAMPLE_INTERVAL(beam_group_name)][:]
            range = sound_speed_at_transducer * (
                blanking_interval + sample_index * sample_interval - sample_time_offset) / 2
            # try to guess field name
            name = input[sonar_def.BeamGroup1Grp.BEAM(beam_group_name)]
            beam_long_name = name.long_name
            with nc.Dataset(f"{output_file_name}_{beam_long_name}.g3d.nc", mode="w") as out:
                # declare type
                out.dataset_type = cst.RootGrp.TYPE_FLYTEXTURE
                out.history = f"Created via conversion from {input_file_name}"

                # Declare available variables
                datalayer_dim = out.createDimension(cst.RootGrp.DATALAYER_COUNT, 3)
                angle_major_name = "athwartship angle"
                angle_minor_name = "alongship_angle"
                time_name = "time"
                bs_name = "absolute_backscatter"
                v = out.createVariable(
                    varname=cst.RootGrp.DATALAYER_DISPLAY_NAMES, dimensions=(cst.RootGrp.DATALAYER_COUNT), datatype=str
                )
                v[0] = f"{beam_long_name}"
                v[1] = f"{angle_minor_name}"
                v[2] = f"{angle_major_name}"
                v = out.createVariable(
                    varname=cst.RootGrp.DATALAYER_VARIABLES_NAMES, dimensions=(cst.RootGrp.DATALAYER_COUNT),
                    datatype=str
                )
                v[0] = bs_name
                v[1] = angle_minor_name
                v[2] = angle_major_name

                slice_group = out.createGroup(beam_group_name)
                slice_group.long_name = beam_long_name

                slice_group.createDimension(cst.RootGrp.DataLayerGrp.LENGTH_DIM, values.shape[1])
                vertical_sampling = int(max(1, np.ceil(values.shape[0] / max_vertical_samples)))

                reduced_value = block_reduce(values[:], block_size=(vertical_sampling, 1), func=np.max)
                assert reduced_value.shape[0] < 16384

                slice_group.createDimension(cst.RootGrp.DataLayerGrp.HEIGHT_DIM, reduced_value.shape[0])
                v = slice_group.createVariable(
                    varname=bs_name,
                    dimensions=(cst.RootGrp.DataLayerGrp.HEIGHT_DIM, cst.RootGrp.DataLayerGrp.LENGTH_DIM),
                    datatype=float, zlib=True
                )
                v[:] = reduced_value
                v = slice_group.createVariable(
                    varname=angle_minor_name,
                    dimensions=(cst.RootGrp.DataLayerGrp.HEIGHT_DIM, cst.RootGrp.DataLayerGrp.LENGTH_DIM),
                    datatype=float, zlib=True
                )
                reduced_value = block_reduce(angle_minor[:], block_size=(vertical_sampling, 1), func=np.max)
                v[:] = reduced_value[:]

                v = slice_group.createVariable(
                    varname=angle_major_name,
                    dimensions=(cst.RootGrp.DataLayerGrp.HEIGHT_DIM, cst.RootGrp.DataLayerGrp.LENGTH_DIM),
                    datatype=float, zlib=True
                )
                reduced_value = block_reduce(angle_major[:], block_size=(vertical_sampling, 1), func=np.max)
                v[:] = reduced_value[:]

                v = slice_group.createVariable(
                    varname=time_name,
                    dimensions=(cst.RootGrp.DataLayerGrp.LENGTH_DIM),
                    datatype=np.uint64, zlib=True
                )
                tv = nano_todatetime(time_variable[:])
                tvms = tv.astype("datetime64[ms]")
                v[:] = tv.astype("datetime64[ms]")  # swith from ns to ms
                # create position vector
                slice_group.createDimension(cst.RootGrp.DataLayerGrp.VECTOR_DIM, 2)
                slice_group.createDimension(cst.RootGrp.DataLayerGrp.POSITION_DIM, ping_count)

                latitude = slice_group.createVariable(
                    varname=cst.RootGrp.DataLayerGrp.LATITUDE_VARIABLE,
                    dimensions=(
                        cst.RootGrp.DataLayerGrp.VECTOR_DIM, cst.RootGrp.DataLayerGrp.POSITION_DIM),
                    datatype="f8",
                )
                latitude[0, :] = latitude_values[:]
                latitude[1, :] = latitude_values[:]

                longitude = slice_group.createVariable(
                    varname=cst.RootGrp.DataLayerGrp.LONGITUDE_VARIABLE,
                    dimensions=(
                        cst.RootGrp.DataLayerGrp.VECTOR_DIM, cst.RootGrp.DataLayerGrp.POSITION_DIM),
                    datatype="f8",
                )
                longitude[0, :] = longitude_values[:]
                longitude[1, :] = longitude_values[:]

                elevation = slice_group.createVariable(
                    varname=cst.RootGrp.DataLayerGrp.ELEVATION,
                    dimensions=(
                        cst.RootGrp.DataLayerGrp.VECTOR_DIM, cst.RootGrp.DataLayerGrp.POSITION_DIM),
                    datatype="f4",
                )
                elevation[0, :] = -1 * top[:]
                elevation[1, :] = -1 * (top + range)[:]


if __name__ == "__main__":
    #    merger.merge_with_MFDataset(output_file_name=path.joinpath("out.nc"),input_files=files)
    input_directory = 'K:/Jeremie/TONGA/'
    path = Path(input_directory)
    files = list(path.glob("*.xsf.nc"))
    for input in files:
        out = tmp.mktemp(prefix=f"{input.stem}_",dir=input_directory)
        convert(input_file_name=input, output_file_name=out)
        