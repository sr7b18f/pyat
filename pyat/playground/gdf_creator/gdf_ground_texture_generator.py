import math
import tempfile as tmp
import os
import netCDF4 as nc

from playground.GDFCreator import GDFConstants as cst

import pyat.core.dtm.emodnet.dtm_standard_constants as dtm_cst


class MultiDtmConverter:
    def __init__(self, input, output):
        self.input = (input[0], input[1])
        self.output = os.path.realpath(output)

    def convert(self):
        # parse a XML binary file containing slices
        with nc.Dataset(self.output, mode="w") as out:
            # declare type
            out.dataset_type = cst.RootGrp.TYPE_GROUNDTEXTURE
            out.history = "Created via conversion from " + str(self.input)

            datalayer_dim = out.createDimension(cst.RootGrp.DATALAYER_COUNT, 3)  # we force layer count to 2
            # datalayer_dim = out.createDimension(cst.RootGrp.DATALAYER_COUNT, 1)  # we force layer count to 1
            v = out.createVariable(
                varname=cst.RootGrp.DATALAYER_DISPLAY_NAMES, dimensions=(cst.RootGrp.DATALAYER_COUNT), datatype=str
            )
            v[0] = "elevation"
            v[1] = "seafloor backscatter"
            v[2] = "stdev"

            v = out.createVariable(
                varname=cst.RootGrp.DATALAYER_VARIABLES_NAMES, dimensions=(cst.RootGrp.DATALAYER_COUNT), datatype=str
            )
            v[0] = dtm_cst.ELEVATION_NAME
            v[1] = dtm_cst.BACKSCATTER
            v[2] = dtm_cst.STDEV

            slice_dim = len(self.input)
            variables_to_copy = [
                dtm_cst.ELEVATION_NAME,
                dtm_cst.BACKSCATTER,
                dtm_cst.STDEV,
                dtm_cst.CRS_NAME,
                dtm_cst.LON_NAME,
                dtm_cst.LAT_NAME,
            ]
            for slice in range(1, slice_dim + 1):
                slice_index = slice - 1
                digit_count = int(math.log10(slice_dim)) + 1
                # create one group per slice
                format_str = "%0" + str(digit_count) + "d"
                groupName = format_str % slice
                slice_group = out.createGroup(groupName)
                # group metadata
                slice_group.is_valid = 1
                slice_group.long_name = "Slice number " + str(slice)

                input_file = self.input[slice_index]
                with nc.Dataset(input_file, "r") as file_in:
                    """Copy dimensions based on the input file."""
                    for name, dimension in file_in.dimensions.items():
                        slice_group.createDimension(name, len(dimension) if not dimension.isunlimited() else None)
                    """Copy variables based on the input file."""
                    for name, variable in file_in.variables.items():
                        if name in variables_to_copy:
                            v = slice_group.createVariable(
                                name,
                                variable.datatype,
                                variable.dimensions,
                                fill_value=variable._FillValue if hasattr(variable, "_FillValue") else None,
                            )
                            # copy attributes
                            v.setncatts(variable.__dict__)
                            # copy variable
                            v[:] = variable[:]


if __name__ == "__main__":
    print("starting dtm to multidtm converter")
    out = tmp.mktemp(suffix=".nc", prefix="GroundTexture_", dir="d://tmp")

    input = ("D:/tmp/EPAVES_2012.dtm.nc", "d:/tmp/EPAVES_2012_N.dtm.nc")
    converter = MultiDtmConverter(input, out)
    converter.convert()
