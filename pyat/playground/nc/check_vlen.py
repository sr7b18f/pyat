import netCDF4 as nc
import tempfile as tmp
import numpy as np


def create_file_with_float_valid_range(file: str):
    """
    Create a netcdf file with one vlen variable and a valid_range attribute
    """
    with nc.Dataset(file, "w") as f:
        # create vlen type and name it
        x = f.createDimension("x", None)

        vlen_t_float = f.createVLType(np.float32, "my_type")
        vlvar_float = f.createVariable("values_float", vlen_t_float, ("x"))
        vlvar_float.valid_range = [np.float32(0), np.float32(5)]

        count = 292
        # fill with arbitrary values
        data = np.empty(len(x), object)
        for n in range(count):
            # create array of variable len
            values = np.pi + np.arange(4)
            values[1] = np.nan
            vlvar_float[n] = values  # create fake data with pi values


def print_content_sample(file: str, var_name="var"):
    """
    read first and last values of vlen variable and print them
    """
    with nc.Dataset(file) as input:
        values = input[var_name]
        values.set_auto_mask(True)
        sample = values[:]
        last_index = len(sample) - 1
        print(f"file {file} : content_vlen = [{sample[0]} ...{sample[last_index]} ]")


if __name__ == "__main__":
    file = tmp.mktemp(suffix=".nc", prefix="temp_")
    create_file_with_float_valid_range(file)
    print_content_sample(file, var_name="values_float")
