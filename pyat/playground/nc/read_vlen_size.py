import os
import tempfile as tmp

import netCDF4 as nc
import numpy as np
import h5py

import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as xsf

variable_name = "/Sonar/Beam_group1/backscatter_r"

def read_all_values(file_name: str):
    with nc.Dataset(file_name) as dataset:
        v = dataset[variable_name]
        #read all values
        values = v[:]
        # get all values length
        f=np.vectorize(len)
        array_of_len=f(values)
        print(array_of_len)

def read_only_size(file_name: str):
    file = h5py.File(file_name, 'r')
    dataset = file[variable_name]
    dataset_id= h5py.h5d.open(file.id,str.encode(variable_name))

    h5py.h5d.vlen_get_buf_size


# in case we are started in standalone app, for debug only
if __name__ == "__main__":
    file_path = "D:/data/file/XSF/Movies/Sardine_schools_1.xsf.nc"
    file_path = "D:/XSF/0006_20200504_111056_FG_EM122.xsf.nc"
    # file_path = "D:/data/file/XSF/ExampleSonarData/test90-D20171107-T195133.nc"
    read_only_size(file_path)
