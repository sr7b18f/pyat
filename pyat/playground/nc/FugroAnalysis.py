
import matplotlib.pyplot as plt
import numpy as np

import pyat.core.xsf.struct.sonar_netcdf.sonar_groups as cst
from pyat.playground.stage.utils.time_utils import secondToNanoF, nano_todatetime, floatsecond_tonano_array
from pyat.core.xsf.xsf_reader import XSFReader
import timeit

def process(file_name,slice_index):
    reader = XSFReader(file_name)
    wc=reader.get_vlen_variable(cst.RootGrp.SonarGrp.BeamGroup1Grp.BACKSCATTER_R, slice_index)
    angles=reader.get_variable(cst.RootGrp.SonarGrp.BeamGroup1Grp.RX_BEAM_ROTATION_PHI)[slice_index][:]
    ping_time=reader.get_variable(cst.RootGrp.SonarGrp.BeamGroup1Grp.PING_TIME)[slice_index]
    roll=reader.get_variable(cst.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_ROLL)[slice_index]
    sample_time_offset= reader.get_variable(cst.RootGrp.SonarGrp.BeamGroup1Grp.BeamGroup1VendorSpecificGrp.TRANSMIT_TIME_DELAY)[
                        slice_index, :]
    tx_sectors = reader.get_variable(cst.RootGrp.SonarGrp.BeamGroup1Grp.BEAM_SECTOR)[slice_index, :]
    sample_interval=reader.get_variable(cst.RootGrp.SonarGrp.BeamGroup1Grp.SAMPLE_INTERVAL)[slice_index]


    r=np.arange(0,wc.shape[0],1)
    r=r.reshape((-1,1))

    rmax=np.max(r)
    y=rmax-r*np.cos(angles.reshape((1,-1)))
    x=r*np.sin(angles.reshape((1,-1)))
    plt.figure()
    plt.scatter(x,y,c=wc,s=1)
    plt.title("Source data with angles as they are")
    plt.savefig(file_name+"_raw_angles_"+str(slice_index)+".png")


    sample_time_offset_per_beam=sample_time_offset[tx_sectors-1]
    min_sample_time=np.min(sample_time_offset_per_beam)
    echo_time = ping_time+floatsecond_tonano_array(sample_time_offset_per_beam+ r*sample_interval)

    min_echo_time = ping_time + floatsecond_tonano_array(min_sample_time)
    max_echo_time = np.max(echo_time)
    min_hf_index = np.argmin(np.abs(hf_time-min_echo_time))
    max_hf_index = np.argmin(np.abs(max_echo_time-hf_time))

    #resize high frequency variables to save some space
    hf_time=hf_time[min_hf_index:max_hf_index:]
    hf_roll= hf_roll[min_hf_index:max_hf_index:]
    #now we have echo times in nano sec,
    t=plt.figure()
    plt.scatter(nano_todatetime(hf_time),hf_roll, label='high frequency roll')
    plt.scatter(nano_todatetime(echo_time[:,144]),np.ones((echo_time.shape[0]))* roll, label="echo roll per range, for central beam")
    plt.title("Roll high frequency vs roll at ping time")
    plt.legend()
    plt.xlim(np.min(nano_todatetime(echo_time[:,144])),np.max(nano_todatetime(echo_time[:,144])))
    plt.savefig(file_name+"Roll high frequency vs roll at ping time"+str(slice_index)+".png")

    # compute roll at each sample file
    v = echo_time
    v = v.reshape((1, v.shape[0], v.shape[1]))
    roll_index = np.argmin(np.abs(v - hf_time.reshape(-1, 1, 1)), axis=0)
    v_roll = hf_roll[roll_index]
    roll_per_sample = v_roll

    # t=plt.figure()
    # plt.scatter(hf_time,hf_roll)
    # plt.scatter(nano_todatetime(echo_time[0,:]),roll_per_sample[0,:])
    # plt.title("Roll high frequency vs roll at tx time")
    # plt.xlim(np.min(nano_todatetime(echo_time[0,:])),np.max(nano_todatetime(echo_time[0,:])))


    final_angles=angles.reshape((1,-1))+np.radians(roll_per_sample-roll)

    fig=plt.figure()
    yf=rmax-r*np.cos(final_angles)
    xf=r*np.sin(final_angles)
    plt.scatter(xf,yf,c=wc,s=1)
    plt.title("Source data with roll per sample")

    plt.savefig(file_name+"_roll_per_sample_"+str(slice_index)+".png")
    plt.close(fig)
    #profil de célérité
    # z=reader.get_variable(cst.RootGrp.EnvironmentGrp.SoundSpeedProfileGrp.DEPTH)[0,:]
    # speed=reader.get_variable(cst.RootGrp.EnvironmentGrp.SoundSpeedProfileGrp.SOUND_SPEED)[0,:]
    # plt.figure()
    # plt.scatter(speed,-z)
    # plt.plot(speed,-z)
    # plt.title("Sound speed")
    # plt.grid()


file_name= "D:/data/fugro/GAUSS_ATALANTE_comparaison/FG_20200422_181329_EM122.xsf.nc"
slice_index = 30
for slice_index in np.arange(28,32):
    print("Processing ping "+str(slice_index))
    process(file_name,slice_index)
    t=timeit.timeit("process(file_name, slice_index)", number=1, globals=globals())
    print(f"Processed in {t}s")

# file_name= "D:\data\fugro\GAUSS_ATALANTE_comparaison/0297_20160208_053317_ATL.xsf.nc"
# slice_index = 30
# for slice_index in np.arange(79,81,1):
#     print("Processing ping "+str(slice_index))
#     print(timeit.timeit("process(file_name, slice_index)", number=1,globals=globals()))

print('Finished')
