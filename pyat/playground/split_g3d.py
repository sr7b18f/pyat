import os

import netCDF4 as nc
import numpy as np

import pyat.core.dtm.emodnet.cdi_layer_util as cdi_util
from pyat.core.utils.netcdf_utils import duplicate_dimensions, duplicate_variable


def copy_att(input_grp,output_grp):
    output_grp.setncatts(input_grp.__dict__)

def duplicate_var(i_var, o_dataset,subpart_index:int):
    var_name = i_var.name
    print(f"Processing {var_name}")
    if var_name == "elevation":
        duplicate_variable(i_var=i_var, o_dataset=o_dataset)
        variable =o_dataset.variables["elevation"]
        values = variable[:]
        if subpart_index==0:
            values[1,:]=values[1,:]/2
        else:
            values[0,:]=values[1,:]/2
        variable[:] =values

    elif var_name == "sv":
        o_var = o_dataset.createVariable(i_var.name, i_var.datatype, i_var.dimensions)
        o_var.setncatts(i_var.__dict__)
        new_heigh = int(i_var.shape[0]/2)
        if subpart_index==0:
            new_data = i_var[new_heigh::, :]
        else:
            new_data = i_var[0:new_heigh,:]
        o_var[:] = new_data.data
    else:
        duplicate_variable(i_var=i_var, o_dataset=o_dataset)


def duplicate_dim(i_dataset, o_dataset,subpart_index):
    for name, dimension in i_dataset.dimensions.items():
        if name == "height":
            new_len = dimension.size/2 #value is pair in ou sample case
            o_dataset.createDimension(name,new_len)
        else:
            o_dataset.createDimension(name, len(dimension) if not dimension.isunlimited() else None)


def copy_grp(input_grp:nc.Dataset,output_grp:nc.Dataset,subpart_index:int):
    copy_att(input_grp,output_grp)
    duplicate_dim(i_dataset=input_grp, o_dataset=output_grp,subpart_index=subpart_index)
    for var in input_grp.variables:
        variable = input_grp.variables[var]
        duplicate_var(i_var=variable,o_dataset=output_grp,subpart_index=subpart_index)

    for subgrp in input_grp.groups:
        i_dataset = input_grp.groups[subgrp]
        grp=output_grp.createGroup(subgrp)
        copy_att(input_grp=i_dataset,output_grp=grp)
        duplicate_dim(i_dataset=i_dataset,o_dataset=grp,subpart_index=subpart_index)
        for var in i_dataset.variables:
            variable = i_dataset.variables[var]
            duplicate_var(i_var=variable,o_dataset=grp,subpart_index=subpart_index)

def run(input_file:str):
    input_dir = os.path.dirname(input_file)
    base_name = os.path.basename(input_file)
    file_partA = base_name.replace(".g3d.nc","_part1.g3d.nc")
    file_partB = base_name.replace(".g3d.nc","_part2.g3d.nc")
    file_partA = os.path.join(input_dir,file_partA)
    file_partB = os.path.join(input_dir,file_partB)
    with nc.Dataset(input_file) as src, nc.Dataset(file_partA,mode="w") as targetA,nc.Dataset(file_partB,mode="w") as targetB:
        copy_grp(input_grp=src,output_grp=targetA,subpart_index=0)
        copy_grp(input_grp=src,output_grp=targetB,subpart_index=1)


if __name__ == "__main__":


    run(r"C:\data\tmp\wc\MD228_MAYOBS15-D20201012-T192619_v20220121.g3d.nc")
