from typing import Union
import pprint
import json

import locale

import os
from enum import Enum
from pathlib import Path
from pyat.core.utils import pyat_logger
from pyat.core.utils.path_utils import scan_dir
from pyat.robot.pipeline import Pipeline_converter, Pipeline, Empty_pipeline
from pyat.robot.pipeline_units import PipeLineContext


class Mode(Enum):
    WATCH_SINGLE_DIR = 0
    SCAN_AND_RECURSE = 1


class PipelineSingleFileStatus(Enum):
    """Status of a pipeline for one single input"""

    UNKOWN = -1
    ERROR = 0
    SUCCESS = 1


status_key = "status"
input_pathkey = "input_path"


class PipelineState:
    """Class storing all individual status and values for every file added to the pipeline"""

    def __init__(self, working_directory_path: Path, pipeline_class: Pipeline):
        self.logger = pyat_logger.logging.getLogger(self.__class__.__name__)
        self.state = {}  # empty dictionnary with key = path and value = a dictionary of data linked to the file
        self.working_directory_path = working_directory_path
        self.pipeline_class = pipeline_class
        self.file_path = self.compute_path(self.working_directory_path, self.pipeline_class)
        self._deserialize(self.file_path)

    @staticmethod
    def compute_path(working_directory_path: Union[Path, str], pipeline_class: Pipeline):
        """Compute the file path of the json file where data are serialized"""
        if isinstance(working_directory_path, str):
            working_directory_path = Path(working_directory_path)
        return working_directory_path.joinpath(pipeline_class.__name__ + ".json")

    def _deserialize(self, fpath: Path):
        """Read the json file in the working_dir to initialize file-watching attributes"""
        state = {}  # in case of error, use empty state read
        try:
            if fpath.exists():
                with open(fpath, encoding=locale.getpreferredencoding()) as fd:
                    state = json.load(fd)
        except json.decoder.JSONDecodeError:
            self.logger.error(f"file {fpath} doesn't contain valid json. Default values will be used.")
        self.state = state

    def _serialize(self, state: dict, fname: Path):
        """save the dictonnary"""
        try:
            with open(fname, "w", encoding=locale.getpreferredencoding()) as fd:
                json.dump(state, fd, indent=4, ensure_ascii=False)
        except PermissionError:
            self.logger.error(
                f"Can't save the state because {fname} cannot be written. State was:\n"
                + json.dumps(state, indent=4, ensure_ascii=False)
            )

    def save(self):
        """save to file"""
        self._serialize(self.state, self.file_path)

    def push_file(self, path_as_string: str):
        """Add default states for the given file if not already in dictionnary"""
        state_for_file = {status_key: PipelineSingleFileStatus.UNKOWN.name}
        # retrieve previous execution status
        if path_as_string not in self.state:
            self.state[path_as_string] = state_for_file

    def get_operation_status(self, path_as_string: str):
        """retrieve the last operation (pipeline execution) status"""
        return self.state[path_as_string][status_key]

    def set_operation_status(self, path_as_string: str, status: PipelineSingleFileStatus):
        """set the last operation (pipeline execution) status"""
        self.state[path_as_string][status_key] = status

    def print(self):
        """Retrieve if exist robot state file and print its content"""

        pprint.pprint(self.state)


class Robot:
    """
    Scan a input directory and react when a file is read or added to this directory
    Maintains a list of already processed files and is robust to restart
    """

    def _get_mode(self):
        """return mode for this robot"""
        return Mode.SCAN_AND_RECURSE

    def __init__(self, input_dir: os.PathLike, work_dir: os.PathLike):
        self.logger = pyat_logger.logging.getLogger(self.__class__.__name__)
        input_dir = os.path.expanduser(input_dir)
        input_dir = os.path.expandvars(input_dir)
        self.input_dir_path = Path(input_dir)
        self.input_file_patterns = ("*.s7k", "*.all", "*.kmall")  # File extension that are monitored by robot

        self.working_directory_path = os.path.expandvars(os.path.expanduser(work_dir))
        self.working_directory_path = Path(self.working_directory_path)
        self.working_directory_path.mkdir(parents=True, exist_ok=True)

        if not (self.input_dir_path.exists() and self.input_dir_path.is_dir()):
            self.logger.error(f"input directory {self.input_dir_path} does not exist or is not a directory")

    def scan_watch_dir(self, pipeline_class: Pipeline):
        """Scan directory for and retrieve all files matching pattern"""
        self.logger.info(f"scanning input directory for file pattern {self.input_file_patterns}")

        # try to load the previous state for this pipeline, state is store in the working directory
        state = PipelineState(self.working_directory_path, pipeline_class)

        # scan dir return an iterable but we will return a list of file
        for path in scan_dir(self.input_dir_path, self.input_file_patterns, recurse=True):

            # load previous state if it exists
            # robot file name
            path_as_string = str(path)
            context = PipeLineContext(
                input_file_name=path, input_directory=self.input_dir_path, output_directory=self.working_directory_path
            )

            state.push_file(path_as_string)
            state.save()

            returned_code = state.get_operation_status(path_as_string)

            if returned_code == PipelineSingleFileStatus.SUCCESS.name:
                self.logger.info(f"{pipeline_class.__name__ } skipping {path} (already processed)")
            else:
                # create a pipeline instance and call run method
                return_value = pipeline_class(context).run()
                if return_value:
                    state.set_operation_status(path_as_string, PipelineSingleFileStatus.SUCCESS.name)
                else:
                    state.set_operation_status(path_as_string, PipelineSingleFileStatus.ERROR.name)
                state.save()

    def run(self, pipeline_class: Pipeline):
        if self._get_mode() is Mode.SCAN_AND_RECURSE:
            # try:
            self.scan_watch_dir(pipeline_class)
            # except Exception as err:
            # self.manager.save_state()
        else:
            raise NotImplementedError("WATCHDOG ROBOT NOT IMPLEMENTED")
            # we should probably use the watchdog api to be notified in change on file systems
            # and hope that it can work over network file system
            # see https://github.com/gorakhargosh/watchdog
            # ps: this works, but it seems that we are only notified of "FileModified" events, never of File close


if __name__ == "__main__":

    robot = Robot(
        input_dir=r"C:\data\datasets\Backscatter\Compensation\THALIA_ESSDEC2019\EM2040",
        work_dir=r"C:\data\datasets\Backscatter\Compensation\THALIA_ESSDEC2019\EM2040\XSF",
    )

    robot.run(Pipeline_converter)
    # robot.run(Empty_pipeline)

    state = PipelineState(
        working_directory_path=r"C:\data\datasets\Backscatter\Compensation\THALIA_ESSDEC2019\EM2040\XSF",
        pipeline_class=Pipeline_converter,
    )
    state.print()
