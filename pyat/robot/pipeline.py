"""Definition of a PipelineUnit, the small building blocks of a full pipeline
used by Robot to implement complex behaviors on watched directories.

"""
from asyncio import subprocess
from enum import Enum

from pathlib import Path

from abc import abstractmethod, ABC

import os
import time
import fnmatch
import itertools
from typing import Callable, List, Optional, Iterable, Tuple
from multiprocessing import Process
from pyat.core.utils.path_utils import basename_of_fname
from pyat.core.utils import pyat_logger
from pyat.robot.installation_parameters import path_to_converter
from pyat.robot.pipeline_units import PipelineUnit, ConvertToXSFUnit, PipeLineContext, EmptyUnit


class Pipeline:
    """A pipeline has a set of Pipeline units that are chained together"""

    def __init__(self, context: PipeLineContext):
        self.logger = pyat_logger.logging.getLogger(self.__class__.__name__)
        self.context = context
        # the list of pipeline units
        self.pipeline_units = self.get_units()



    @abstractmethod
    def get_units(self) -> Iterable[PipelineUnit]:
        """Build and return the list of pipeline units for this pipeline"""
        return []

    @abstractmethod
    def _get_name(self):
        """Return the pipeline name"""

    def run(self) -> bool:
        """Run the Pipeline and return True if success or False if an error occured"""
        for unit in self.pipeline_units:
            result, output_filename = unit.run(self.context)
            if result != 0:
                self.logger.error(
                    f"Pipeline {self._get_name()} failed on operation unit {unit._get_name()} last_generated file = {self.context.last_output}, output file= {output_filename}, returned value {result}"
                )
                return False
        self.logger.info(
            f"Success of Pipeline {self._get_name()}"
        )

        return True

class Pipeline_converter(Pipeline):
    def _get_name(self):
        return "XSF Converter pipeline"

    def get_units(self) -> Iterable[PipelineUnit]:
        return [ConvertToXSFUnit(path_to_converter)]

class Empty_pipeline(Pipeline):
    def _get_name(self):
        return "Empty pipeline"

    def get_units(self) -> Iterable[PipelineUnit]:
        return [EmptyUnit(), EmptyUnit()]
