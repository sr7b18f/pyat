#! /usr/bin/env python3
# coding: utf-8
import locale

import os
import sys
import time
import json
from enum import Enum
from typing import Any, Callable, Dict, FrozenSet, Iterable, List, Optional, Set, Tuple, Type, Union
from multiprocessing import Process, Manager

from pyat.robot.deprecated.pipeline import Pipeline
from pyat.core.utils import pyat_logger
from pyat.core.utils.path_utils import (
    splitext_of_fname,
    basename_of_fname,
    ext_of_fname,
    repr_file_tree,
    replace_path_prefix,
)
from pyat.robot.deprecated.pipeline_xsf_report import Extractor


class Mode(Enum):
    TERRE = 0
    MER = 1


class Robot:
    def __init__(
        self,
        watched_dir: str,
        working_dir: Optional[str] = None,
        soundings_file_converter_path: Optional[str] = None,
        sleep_time: float = 1,
        mode: Mode = Mode.TERRE,
        multiproc: bool = True,
    ):
        self.logger = pyat_logger.logging.getLogger(self.__class__.__name__)
        self.mode = mode
        self.sleep_time = sleep_time
        self.watched_dir = os.path.expanduser(watched_dir)
        self.working_dir = os.path.expanduser(working_dir) or self.watched_dir
        self.multiproc = multiproc
        pipelines: List[Tuple[Type[Pipeline], Dict[str, Any]]] = [
            (
                Extractor,
                {
                    "delete_xsf": True,
                    "soundings_file_converter_path": soundings_file_converter_path,
                },
            ),
        ]
        self.manager = OrderedTaskManager(
            pipelines,
            self.watched_dir,
            self.working_dir,
            recursive=mode is Mode.TERRE,
            logger=self.logger,
            sleep_time=self.sleep_time,
            multiproc=self.multiproc,
        )

    def __call__(self):
        w = (
            f"directories ({self.watched_dir} and {self.working_dir})"
            if self.working_dir != self.watched_dir
            else f"given directory ({self.working_dir})"
        )
        multiproc = " with multiprocesses" if self.multiproc else " and sequential"
        if self.mode is Mode.TERRE:
            self.logger.info(f"Robot in mode TERRE{multiproc}: will scan once and recursively {w}")
            # try:
            self.manager.scan_watch_dir()
            # except Exception as err:
            # self.manager.save_state()
        else:
            self.logger.info(
                f"Robot in mode MER{multiproc}: will scan and treat every {self.sleep_time} seconds given {w}"
            )
            # try:
            self.manager.wait_for_files()
            # except:
            # self.manager.save_state()
        self.logger.info(f"Robot finished.")


class RobotTerre(Robot):
    def __init__(
        self,
        watched_dir: str,
        working_dir: Optional[str] = None,
        soundings_file_converter_path: Optional[str] = None,
        multiproc: bool = False,
    ):
        super().__init__(watched_dir, working_dir, soundings_file_converter_path, mode=Mode.TERRE, multiproc=multiproc)


class RobotMer(Robot):
    def __init__(
        self,
        watched_dir: str,
        working_dir: Optional[str] = None,
        soundings_file_converter_path: Optional[str] = None,
        multiproc: bool = False,
    ):
        super().__init__(watched_dir, working_dir, soundings_file_converter_path, mode=Mode.MER, multiproc=multiproc)


class OrderedTaskManager:
    def __init__(
        self,
        pipelines: List[Tuple[Type[Pipeline], Dict[str, Any]]],
        watched_dir: str,
        working_dir: str,
        recursive: bool,
        logger=None,
        sleep_time: float = 1,
        multiproc: bool = True,
    ):
        self.logger = logger or pyat_logger.logging.getLogger(self.__class__.__name__)
        self.pipelines = pipelines
        self.watched_dir, self.working_dir = watched_dir, working_dir
        self.recursive = recursive
        self.sleep_time = sleep_time
        self.units: Dict[str, Pipeline] = {}  # fname -> pipeline
        self.multiproc = multiproc
        self.proc_manager = Manager() if self.multiproc else None  # will manage the units processes
        self.load_state()

    def load_state(self):
        """Read the .robot file in the working_dir to initialize file-watching attributes"""
        fname = os.path.join(self.working_dir, ".robot")
        state = {}  # in case of error, nothing was read
        try:
            with open(fname,encoding=locale.getpreferredencoding()) as fd:
                state = json.load(fd)
            self.logger.info(f"State loaded from {fname}")
        except FileNotFoundError:
            self.logger.warning(f"file {fname} doesn't exists. Default values will be used.")
        except json.decoder.JSONDecodeError:
            self.logger.error(f"file {fname} doesn't contain valid json. Default values will be used.")
        for fname, unit_desc in dict(state.get("units", {})).items():
            self.run_unit_from_json(fname, unit_desc)

    def save_state(self):
        """Write the .robot file in the working_dir to be able to remember the state of work if restarted"""
        state = {
            "units": {f: u.as_json() for f, u in self.units.items()},
        }
        fname = os.path.join(self.working_dir, ".robot")
        try:
            with open(fname, "w",encoding=locale.getpreferredencoding()) as fd:
                json.dump(state, fd, indent=4, ensure_ascii=False)
        except PermissionError:
            self.logger.error(
                f"Can't save the state because {fname} cannot be written. State was:\n"
                + json.dumps(state, indent=4, ensure_ascii=False)
            )

    def repr_state(self):
        """Trie to represent the state of the Robot"""
        all_units = set(u for u in self.units.values())
        working_units = set(u for u in self.units.values() if u.has_works_remaining)
        finished_units = set(u for u in self.units.values() if not u.has_works_remaining)
        error_units = set(u for u in self.units.values() if u.err)

        self.logger.info(
            f"On a total of {len(all_units)} units, {len(working_units)} are still working, {len(finished_units)} have finished, and {len(error_units)} presents some error."
        )
        if error_units and error_units <= finished_units:
            self.logger.info(f"All units encountering errors have finished.")

        self.logger.info("Units description:")
        for unit in all_units:
            self.logger.info(f"   * {unit}")
            for level, log in unit.logs:
                self.logger.info(f"       - {level.rjust(6).upper()}: {repr(log)}")

    def run_unit_for_file(self, pipeclass, fname: str, kwargs: dict):
        shared_state: Dict[str, Any] = (
            self.proc_manager.dict() if self.multiproc else {}
        )  # dict that will be shared between two processes
        unit = self.units[fname] = pipeclass.run_for_file(shared_state, self.data_dir_of, fname, **kwargs)
        if self.multiproc:
            unit.proc = proc = Process(target=unit)
            if proc:
                proc.start()
            else:
                self.logger.error(f"Couldn't start process of unit {unit}. It won't be treated.")
        else:  # just run it in that thread, and wait for it to finishes
            self.logger.info(f"Running {unit}…")
            for step in unit.exec_step_by_step():
                self.logger.info(f"    {unit}" )
                self.save_state()  # save state at each moment
            self.logger.info(f"Unit have finished.")

    def run_unit_from_json(self, fname: str, unit_desc: dict):
        shared_state: Dict[str, Any] = (
            self.proc_manager.dict() if self.multiproc else {}
        )  # dict that will be shared between two processes
        self.units[fname] = Pipeline.from_json(
            unit_desc,
            shared_state,
            logger=self.logger,
            data_dir_of=self.data_dir_of,
            available_pipelines={p for p, _ in self.pipelines},
        )
        self.units[fname].proc = proc = Process(target=self.units[fname])
        if proc:
            proc.start()
        else:
            self.logger.error(f"Couldn't start process of unit {self.units[fname]}. It won't be treated.")

    def scan_watch_dir(self):
        self.search_for_files_to_handle()
        if self.multiproc:
            self.wait_for_units_to_finishes()
        self.repr_state()
        self.save_state()

    def wait_for_units_to_finishes(self):
        # show progression
        while any(unit.has_works_remaining for unit in self.units.values()):
            self.repr_state()
            self.save_state()
            time.sleep(self.sleep_time)
        # ensure we wait for each to finishes
        self.logger.info("Joining all units…")
        for unit in self.units.values():
            unit.proc.join()

    def wait_for_files(self):
        repr_needed = True
        try:
            while True:
                if self.search_for_files_to_handle():  # one or more files were found
                    repr_needed = True
                elif repr_needed:
                    self.repr_state()
                    repr_needed = False
                self.save_state()
                time.sleep(self.sleep_time)
        except KeyboardInterrupt:
            self.logger.info(f"Robot killed by user. Saving state…")
            self.save_state()


    def search_for_files_to_handle(self) -> bool:
        novelty = False
        for fname in set(
            self.__all_files_that_are_present_but_not_tracked(self.watched_dir)
        ):  # encounter each file once at most
            for pipeclass, kwargs in self.pipelines:
                if pipeclass.accepts_filename(fname):
                    assert fname not in self.units, self.units
                    self.run_unit_for_file(pipeclass, fname, kwargs)
                    novelty = True
                    break
        return novelty

    def __all_files_that_are_present_but_not_tracked(self, indir: str) -> Iterable[str]:
        for item in os.scandir(indir):
            if item.is_file() and item.path not in self.units:
                yield item.path
            elif self.recursive and item.is_dir():  # recursive search
                yield from self.__all_files_that_are_present_but_not_tracked(indir=item.path)

    def data_dir_of(
        self, datafile: str, *, files_exists: bool = True, remove_ext: bool = True, create_if_needed: bool = True
    ) -> str:
        """Return the data dir, i.e. the datafile directory in which files generated
        from it should be placed."""
        samefile = os.path.samefile if files_exists else lambda x, y: str(x).rstrip(r"\/") == str(y).rstrip(r"\/")
        if not samefile(self.watched_dir, self.working_dir):
            file_is_in_watched_area = samefile(os.path.commonpath((datafile, self.watched_dir)), self.watched_dir)
            if file_is_in_watched_area:
                dirpath = replace_path_prefix(datafile, self.watched_dir, self.working_dir)
            else:  # file is already in the working directory
                dirpath = datafile
        else:  # watching and working are the very same directory
            dirpath = datafile
        if remove_ext:
            dirpath = splitext_of_fname(dirpath)[0]  # remove the extension
        if create_if_needed and not os.path.exists(dirpath):
            os.makedirs(dirpath)
        return dirpath


if __name__ == "__main__":
    r = RobotTerre(
        sys.argv[1],
        sys.argv[2],
        "~/Téléchargements/SoundingFileConverter-windows/SoundingsFileConverter.exe",
        multiproc=False,
    )
    r()
