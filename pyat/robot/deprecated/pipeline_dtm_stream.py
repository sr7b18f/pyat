"""Definition of the Robot pipeline that produces an Orpheus report from input sounder files.

Tasks:
- convert any .all into a corresponding .xsf
- convert .xsf to .dtm
- export geotiff from .dtm
- notify Globe with the new geotiff, open GEarth

"""

import os
from typing import Optional
from pyat.robot.deprecated.pipeline import Pipeline
from pyat.robot.deprecated.pipeline_xsf_report import convert_all_to_xsf, sounder_file_is_ready


class DTMStream(Pipeline):
    watch = ["all"]

    #pylint: disable=arguments-differ
    def init(self, soundings_file_converter_path: Optional[str]):
        self.path_to_converter = soundings_file_converter_path
        if not self.path_to_converter or not os.path.exists(self.path_to_converter):
            self.mark_as_finished(because=f"{self.path_to_converter} is not a valid path to the .all->.xsf converter")

    @classmethod
    def accepts(cls, fname: str) -> bool:
        return sounder_file_is_ready(fname)

    def step_1(self, fname: str):
        """Convert to xsf"""
        assert self.path_to_converter, self.path_to_converter
        xsfpath = self.outfile_of(fname, ext="xsf.nc")
        success = convert_all_to_xsf(fname, xsfpath, self.path_to_converter, self.log_info)
        if success:  # something went wrong
            self.produced(xsfpath)  # notify we created that file
            return xsfpath
        else:  # everything is ok
            err = f"{self.path_to_converter} did not create {xsfpath}"
            self.mark_as_finished(because=err)
            self.log_error(err)
            return None

    def step_2(self, xsfpath: str):
        self.log_info(f"TODO: implement xsf to dtm conversion")
        dtmpath = self.outfile_of(xsfpath, ext="dtm")
        return dtmpath

    def step_3(self, dtmpath: str):
        self.log_info(f"TODO: implement dtm to geotiff conversion")
        gtiffpath = self.outfile_of(dtmpath, ext="gtiff")
        return gtiffpath

    # pylint:disable = unused-argument
    def step_4(self, gtiffpath: str):
        self.log_info(f"TODO: implement globe notification and GEarth opening")
