
from asyncio import subprocess
from pathlib import Path

from abc import abstractmethod, ABC

import os
import time
import fnmatch
import itertools
from typing import Callable, List, Optional, Iterable, Tuple
from multiprocessing import Process
from pyat.core.utils.path_utils import basename_of_fname



class Pipeline:
    watch: List[str] = []

    def __init__(self, state_container: dict, data_dir_of: Callable, input_fname: str, **kwargs):
        self.state = state_container
        self.state["logs"] = []
        self.state["start_time"] = 0
        self.state["already_ran_time"] = 0
        self.state["productions"] = []
        self.state["err"] = ""
        self.state["last_finished_step"] = -1
        self.state["finished"] = False

        self.proc: Optional[Process] = None  # will store the process running that pipeline ; set by Robot
        self.data_dir_of = data_dir_of
        self.__input_fname = input_fname
        self.__spec_args = kwargs
        # define the pipeline steps
        steps = []
        for idx in itertools.count(0):
            if hasattr(self, f"run_{idx}") or hasattr(self, f"step_{idx}"):
                steps.append(getattr(self, f"run_{idx}", getattr(self, f"step_{idx}")))
            elif idx > 1:
                break
        self.steps = tuple(steps)
        self.last_finished_step_value = self.__input_fname
        self.init(**self.__spec_args)

    # Access to (and modification thereof) shared state values
    @property
    def logs(self):
        return self.state["logs"]

    @logs.setter
    def logs(self, new):
        self.state["logs"] = new

    @property
    def err(self):
        return self.state["err"]

    @err.setter
    def err(self, new: str):
        self.state["err"] = new

    @property
    def last_finished_step(self):
        return self.state["last_finished_step"]

    @last_finished_step.setter
    def last_finished_step(self, new: int):
        self.state["last_finished_step"] = new

    @property
    def finished(self):
        return self.state["finished"]

    @finished.setter
    def finished(self, new: bool):
        self.state["finished"] = new

    @property
    def start_time(self):
        return self.state["start_time"]

    @start_time.setter
    def start_time(self, new):
        self.state["start_time"] = new

    @property
    def already_ran_time(self):
        return self.state["already_ran_time"]

    @already_ran_time.setter
    def already_ran_time(self, new):
        self.state["already_ran_time"] = new

    @property
    def productions(self):
        return self.state["productions"]

    # logging system
    def log(self, level: str, *msgs: str, sep=" "):
        # Why not append ? Because: https://bugs.python.org/issue36119
        self.logs += [(level, sep.join(map(str, msgs)))]

    def log_info(self, *msgs: str):
        self.log("info", *msgs)

    def log_warning(self, *msgs: str):
        self.log("warning", *msgs)

    def log_error(self, *msgs: str):
        self.log("error", *msgs)

    def init(self, *args, **kwargs):
        if args or kwargs:
            raise NotImplementedError(f"Derived class must overwrite init() method to get their own parameters.")

    def mark_as_finished(self, because: Optional[str] = None):
        "Can be used in derived classes to force the end of the pipeline"
        self.state["finished"] = True
        if because:
            if self.err is None:
                self.err = because
            else:
                self.err += " " + because

    def outfile_of(self, fname: str, ext: str = None) -> str:
        """If given fname (for instance, a .all file) is in the watched directory,
        returns a path to a file with given extension that is in the working directory,
        and that is expected to be a file produced from given fname
        """
        datadir = self.data_dir_of(fname)
        path = os.path.join(datadir, basename_of_fname(fname))
        return (path + "." + ext.lstrip(".")) if ext else path

    @classmethod
    def accepts_filename(cls, fname: str) -> bool:
        return any(fnmatch.fnmatch(fname, "*." + pat.lstrip("*.")) for pat in cls.watch) and cls.accepts(fname)

    @classmethod
    # pylint: disable=unused-argument
    def accepts(cls, fname: str) -> bool:
        "To be overwritten by derived classes if they want to narrow the acceptation system"
        return True  # by default, we take any file with correct extension

    def produced(self, *fnames: str):
        "A method for derived classes who want to notify the creation of files"
        for fname in fnames:
            self.productions.append(fname)

    @property
    def has_works_remaining(self) -> bool:
        return not self.finished

    def __call__(self, step_by_step: bool = False):
        for step in self.exec_step_by_step():
            pass

    def exec_step_by_step(self):
        self.start_time = time.time()
        while self.has_works_remaining:
            next_step = self.last_finished_step + 1
            if next_step < len(self.steps):
                self.run_step(next_step)
                yield self.last_finished_step
            else:
                self.mark_as_finished()

    def run_step(self, step_index: int):
        assert isinstance(step_index, int), repr(step_index)
        assert 0 <= step_index < len(self.steps), (step_index, len(self.steps))
        runner = self.steps[step_index]
        try:
            retval = runner(self.last_finished_step_value)
        except Exception as err:
            self.err = repr(err)
            self.finished = True
        else:
            self.last_finished_step_value = retval
            self.last_finished_step = step_index

    def as_json(self) -> dict:
        return {
            "err": self.state["err"],
            "logs": self.logs,
            "class": self.__class__.__name__,
            "finished": self.finished,
            "spec_args": self.__spec_args,
            "start_time": self.start_time,
            "input fname": self.__input_fname,
            "productions": self.productions,
            "already_ran_time": self.already_ran_time + self.active_runtime_in_seconds,
            "last_finished_step": self.last_finished_step,
        }

    @staticmethod
    def from_json(data: dict, state_container: dict, data_dir_of: Callable, logger, available_pipelines: set):
        """Create a Pipeline instance from given json dict"""
        cls = next((p for p in available_pipelines | {DummyUnit} if p.__name__ == data["class"]), None)
        if cls is None:
            err = f"Pipeline class {data['class']} doesn't exists. A non-running Unit be created."
            (logger.error if logger else print)(err)
            return DummyUnit.get_finished_unit(err, data["input fname"], state_container)
        return cls.run_for_file(
            state_container,
            data_dir_of,
            data["input fname"],
            data.get("already_ran_time"),
            data.get("last_finished_step"),
            data.get("finished"),
            data.get("err"),
            data.get("logs"),
            **data.get("spec_args", {}),
        )

    @classmethod
    def run_for_file(
        cls,
        state_container: dict,
        data_dir_of,
        fname: str,
        already_ran_time=None,
        last_finished_step=None,
        finished=None,
        err=None,
        logs=None,
        **kwargs,
    ) -> object:
        """Create a Pipeline instance for given filename, with, if needed, given value for internal properties (used by the from_json method)"""
        instance = cls(state_container, data_dir_of, fname, **kwargs)
        if already_ran_time is not None:
            instance.already_ran_time = already_ran_time
        if last_finished_step is not None:
            instance.last_finished_step = last_finished_step
        if finished is not None:
            instance.finished = finished
        if err is not None:
            instance.err = err
        if logs is not None:
            instance.logs.extend(logs)
        return instance

    @property
    def active_runtime_in_seconds(self) -> float:
        if self.start_time:
            return time.time() - self.start_time
        else:
            return 0

    @property
    def runtime_in_seconds(self) -> float:
        return self.active_runtime_in_seconds + self.already_ran_time

    @property
    def runtime(self) -> str:
        if self.start_time + self.already_ran_time:
            return f"{round(self.runtime_in_seconds, 2)}s"
        else:
            return f"[NOT YET STARTED]"

    def __str__(self) -> str:
        letter = "E" if self.err else ("F" if self.finished else "R")
        intro = f"[{letter}] Unit {self.__class__.__name__} working on {self.__input_fname}"
        err = f" Errors were encountered: {self.err}" if self.err else ""
        prod = f"produced {len(self.productions)} files ({', '.join(self.productions)}).{err}"
        if self.has_works_remaining:
            return f"{intro} for {self.runtime}, at step {self.last_finished_step + 1}/{len(self.steps)}, {prod}"
        else:
            return f"{intro}, finished in {self.runtime}, {prod}"


class DummyUnit(Pipeline):
    """Dummy Pipeline that encode a failed-to-be-created Pipeline instance"""

    # pylint: disable=arguments-differ
    def init(self, soundings_file_converter_path: Optional[str]):
        pass

    @staticmethod
    def get_finished_unit(because: str, fname: str, state_container: Optional[dict] = None) -> object:
        return DummyUnit.run_for_file(
            state_container or {},
            lambda x: x,
            fname,
            last_finished_step=-1,
            finished=True,
            err=because,
            logs=[("error", because)],
        )
