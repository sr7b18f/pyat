"""Definition of the Robot pipeline that produces an excel report containing metadata of .all files.

Tasks:
- convert any .all into a corresponding .xsf
- extract metadata, write them into a .csv file
- delete the .xsf

"""
import locale

import os
import csv
import platform
import subprocess
from typing import Any, Callable, Iterable, Optional, Tuple
from pyat.core.sounder import sounder_driver_factory

from pyat.robot.deprecated.pipeline import Pipeline
from pyat.core.utils.path_utils import splitext_of_fname, basename_of_fname


def convert_all_to_xsf(infile: str, outfile: str, path_to_converter: str, log_info: Callable):
    log_info(f"Converting {infile} to {outfile}…")
    command = [
        path_to_converter,
        "-in",
        infile,
        "-o",
        "-fmt",
        "xsf",
        "-out",
        os.path.split(outfile)[0],
    ]
    if platform.system() == "Linux" and path_to_converter.endswith(".exe"):
        command = ["wine"] + command  # use wine to run the program
    elif platform.system() in {"Windows", "Linux", "Darwin"}:
        pass  # nothing to do, the command is already complete
    else:  # bsd, solaris,…
        raise NotImplementedError(f"Calling {path_to_converter} from {platform} is not yet implemented")
    log_info(f"Calling {path_to_converter} with command `{' '.join(command)}`")
    with subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
        proc.communicate()
    return os.path.exists(outfile)


def sounder_file_is_ready(fname: str) -> bool:
    #pylint: disable=bare-except
    try:
        # with open(fname, 'a') as fd:
        in_copy = False
    except:
        in_copy = True
    is_999 = basename_of_fname(fname).startswith("999")
    has_wcd = os.path.exists(splitext_of_fname(fname)[0] + ".wcd")
    return not is_999 and not in_copy and has_wcd


def metadata_of_xsf(fname: str, logger_error) -> Iterable[Tuple[str, Any]]:
    metadata = {}
    try:
        with sounder_driver_factory.open_sounder(fname) as driver:
            yield "swath_count", driver.sounder_file.swath_count
            yield "beam_count", driver.sounder_file.beam_count
            # for longitudes, latitudes in driver.iter_beam_positions(1000):
                # print("Values for beam[swath_index=0, beam_index=0] ")
                # print("\tLongitude :", longitudes[0, 0])
                # print("\tLatitude :", latitudes[0, 0])
                # print("\tValidity :", driver.read_validity_flags(0, 1)[0, 0])
                # print("\tReflectivity :", driver.read_reflectivities(0, 1)[0, 0])
                # print("\tAcross distance :", driver.read_across_distances(0, 1)[0, 0])
                # print("\tDepth in FCS Coordinate reference :", driver.read_fcs_depths(0, 1)[0, 0])
                # print("\tDepth in SCS Coordinate reference :", driver.read_scs_depths(0, 1)[0, 0])
            yield "Nav longitudes", driver.read_platform_longitudes()
            yield "Nav latitudes", driver.read_platform_latitudes()
    except OSError as err:
        logger_error(f"File {fname} content wasn't recognized by NetCDF: {repr(err)}.")
        return


class Extractor(Pipeline):
    watch = ["all"]

    #pylint: disable=arguments-differ
    def init(self, delete_xsf: bool, soundings_file_converter_path: Optional[str]):
        self.delete_xsf = delete_xsf
        self.path_to_converter = (
            os.path.expanduser(soundings_file_converter_path) if soundings_file_converter_path else None
        )
        if not self.path_to_converter or not os.path.exists(self.path_to_converter):
            self.mark_as_finished(because=f"{self.path_to_converter} is not a valid path to the .all->.xsf converter")

    @classmethod
    def accepts(cls, fname: str) -> bool:
        return sounder_file_is_ready(fname)

    def step_1(self, fname: str):
        """Convert to xsf"""
        assert self.path_to_converter, self.path_to_converter
        xsfpath = self.outfile_of(fname, ext="xsf.nc")
        success = convert_all_to_xsf(fname, xsfpath, self.path_to_converter, self.log_info)
        if success:  # something went wrong
            self.produced(xsfpath)  # notify we created that file
            return xsfpath
        else:  # everything is ok
            err = f"{self.path_to_converter} did not create {xsfpath}"
            self.mark_as_finished(because=err)
            self.log_error(err)
            return None

    def step_2(self, xsfpath: str):
        """Get metadata, write them in CSV file"""
        outpath = self.outfile_of(xsfpath, ext="csv")  # or xls?
        with open(outpath, "w",encoding=locale.getpreferredencoding()) as fd:
            writer = csv.writer(fd)
            self.log_info("TODO implement writing of the csv/xls file")
            for name, value in metadata_of_xsf(xsfpath, self.log_error):
                writer.writerow([name, value])
        self.produced(outpath)  # notify we created that file
        return xsfpath

    def step_3(self, xsfpath: str):
        """Delete the xsf file if asked so"""
        if self.delete_xsf:
            os.remove(xsfpath)
