# -*- coding: utf-8 -*-

"""Top-level package for pyat."""

__author__ = """Cyrille Poncelet"""
__email__ = "cyrille.poncelet@ifremer.fr"
__version__ = "0.1.0"
