# -*- coding: utf-8 -*-
"""
This module create all environments defined for the pyat project
return 0 if everything went OK or the error code value otherwise
"""

from conda_env import conda_env

# environment names as defined in the yml files
environment_runtime_name = "pyat_runtime"
environment_test_name = "pyat_test"
environment_dev_name = "pyat_dev"


env = conda_env.DevEnv(
    environment_runtime_name="pyat_runtime", environment_test_name="pyat_test", environment_dev_name="pyat_dev"
)

env()
