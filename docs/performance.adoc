# How  Run a profiling

# Tools
install line_profiler `conda install line_profiler`

# Run a profiling

Use kernprof to start a recording of a profiling.
The function to profile shall be annotated with `@profile`

For example
`kernprof -l C:\Globe\workspace\pyat\pyat\app\emodnet\xyz_with_cdi_importer_app.py D:\WorkspaceUserGlobe\diffmnt\.cache\PyToolboxExplorer\xyz_with_cdi_importer_app_arguments.json`

# Display results

Use line_profiler module to show the .prof file created before

`$ python -m line_profiler my_script.py.*lprof*`
